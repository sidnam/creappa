import React, {Component} from 'react';

class AppPreview extends Component{
    render(){
        return(
            <div>
                <div className="spaziatura-top-navbar"></div>
                <div className="contenitore-immagine" style={{ backgroundImage: "url(" + this.props.background + ")" }}></div>
                <Spring from={{ opacity: 0, marginLeft: -500 }} to={{ opacity: 1, marginLeft: 0 }}>
                    {props => (
                        <div style={props}>
                            <div className="container-logo">
                                <img src={this.props.logo} className="logo-homepage" style={{ marginTop: "20px" }}></img>
                            </div>
                            <div className="container-bottoni">
                                {this.props.components.map(function (componente, i) {
                                    if (i == this.props.components.length - 1 && i % 2 == 0) {

                                        var styleIcone = {
                                            backgroundColor: "rgba(255,255,255, 0.4)",
                                            display: "block",
                                            flex: "1 0 40%",
                                            textAlign: "center",
                                            minWidth: "150px",
                                            marginTop: "50px",
                                            borderRadius: "25px",
                                            marginLeft: "20px",
                                            marginRight: "20px"
                                        }
                                    } else if (i % 2 != 0) {
                                        var styleIcone = {
                                            backgroundColor: "rgba(255,255,255, 0.4)",
                                            display: "block",
                                            flex: "1 0 40%",
                                            textAlign: "center",
                                            minWidth: "150px",
                                            marginTop: "50px",
                                            borderTopRightRadius: "25px",
                                            borderBottomRightRadius: "25px",
                                            marginRight: "20px"
                                        }
                                    } else {
                                        var styleIcone = {
                                            backgroundColor: "rgba(255,255,255, 0.4)",
                                            display: "block",
                                            flex: "1 0 40%",
                                            textAlign: "center",
                                            minWidth: "150px",
                                            marginTop: "50px",
                                            borderTopLeftRadius: "25px",
                                            borderBottomLeftRadius: "25px",
                                            marginLeft: "20px"
                                        }
                                    }

                                    if (componente.content.type === "About") {
                                        return (
                                            <div style={styleIcone}>
                                                <Link to={"/About" + this.controllo(this.props.components, componente, i)}>
                                                    <StorefrontRoundedIcon style={{ fontSize: 55, color: colors.primary }}></StorefrontRoundedIcon>
                                                </Link>
                                                <p className="descrizione" style={{ color: colors.accent }}>Chi siamo</p>
                                            </div>
                                        )
                                    } else if (componente.content.type === "Where") {
                                        return (
                                            <div style={styleIcone}>
                                                <Link to={"/Where" + this.controllo(this.props.components, componente, i)}>
                                                    <RoomIcon style={{ fontSize: 55, color: colors.primary }}></RoomIcon>
                                                </Link>
                                                <p className="descrizione" style={{ color: colors.accent }}>Dove</p>
                                            </div>
                                        )
                                    } else if (componente.content.type === "Shop") {
                                        return (
                                            <div style={styleIcone}>
                                                <Link to={"/Shop" + this.controllo(this.props.components, componente, i)}>
                                                    <ShoppingCartRoundedIcon style={{ fontSize: 55, color: colors.primary }}></ShoppingCartRoundedIcon>
                                                </Link>
                                                <p className="descrizione" style={{ color: colors.accent }}>Shop</p>
                                            </div>
                                        )
                                    } else if (componente.content.type === "Schedule") {
                                        return (
                                            <div style={styleIcone}>
                                                <Link to={"/Schedule" + this.controllo(this.props.components, componente, i)}>
                                                    <ScheduleRoundedIcon style={{ fontSize: 55, color: colors.primary }}></ScheduleRoundedIcon>
                                                </Link>
                                                <p className="descrizione" style={{ color: colors.accent }}>Orario</p>
                                            </div>
                                        )
                                    } else if (componente.content.type === "Date") {
                                        return (
                                            <div style={styleIcone}>
                                                <Link to={"/Date" + this.controllo(this.props.components, componente, i)}>
                                                    <CalendarTodayRoundedIcon style={{ fontSize: 55, color: colors.primary }}></CalendarTodayRoundedIcon>
                                                </Link>
                                                <p className="descrizione" style={{ color: colors.accent }}>Appunt.</p>
                                            </div>
                                        )
                                    } else if (componente.content.type === "Contacts") {
                                        return (
                                            <div style={styleIcone}>
                                                <Link to={"/Contacts" + this.controllo(this.props.components, componente, i)}>
                                                    <ContactPhoneRoundedIcon style={{ fontSize: 55, color: colors.primary }}></ContactPhoneRoundedIcon>
                                                </Link>
                                                <p className="descrizione" style={{ color: colors.accent }}>Contatti</p>
                                            </div>
                                        )
                                    } else if (componente.content.type === "Report") {
                                        return (
                                            <div style={styleIcone}>
                                                <Link to={"/Report" + this.controllo(this.props.components, componente, i)}>
                                                    <MailRoundedIcon style={{ fontSize: 55, color: colors.primary }}></MailRoundedIcon>
                                                </Link>
                                                <p className="descrizione" style={{ color: colors.accent }}>Report</p>
                                            </div>
                                        )
                                    } else if (componente.content.type === "WebPage") {
                                        return (
                                            <div style={styleIcone}>
                                                <Link to={"/WebPage" + this.controllo(this.props.components, componente, i)}>
                                                    <LanguageRoundedIcon style={{ fontSize: 55, color: colors.primary }}></LanguageRoundedIcon>
                                                </Link>
                                                <p className="descrizione" style={{ color: colors.accent }}>Sito Web</p>
                                            </div>
                                        )
                                    } else if (componente.content.type === "DataViewer") {
                                        return (
                                            <div style={styleIcone}>
                                                <Link to={"/DataViewer" + this.controllo(this.props.components, componente, i)}>
                                                    <AssessmentRoundedIcon style={{ fontSize: 55, color: colors.primary }}></AssessmentRoundedIcon>
                                                </Link>
                                                <p className="descrizione" style={{ color: colors.accent }}>DataViewer</p>
                                            </div>
                                        )
                                    } else if(componente.content.type === "Galleria"){
                                        return (
                                            <div style={styleIcone}>
                                                <Link to={"/galleria" + this.controllo(this.props.components, componente, i)}>
                                                    <PhotoLibraryIcon style={{ fontSize: 55, color: colors.primary }}></PhotoLibraryIcon>
                                                </Link>
                                                <p className="descrizione" style={{ color: colors.accent }}>{componente.content.titolo}</p>
                                            </div>
                                        )
                                    }
                                }, this)}
                            </div>
                        </div>
                    )}
                </Spring>
            </div>
        )
    }
}