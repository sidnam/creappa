import React, { Component } from "react"
import NavbarAdmin from "./NavbarAdmin"

class Visualizza extends Component{
    render(){
        return(
            <div>
                <NavbarAdmin></NavbarAdmin>
                <div style={{ marginLeft: "5%", height: "calc(100vh - 60px)", marginTop: "10px", width: "100vw - 220px", textAlign: "center", overflow: "scroll" }}>
                    <h1><strong>Visualizza</strong></h1>
                </div>
            </div>
        )
    }
}

export default Visualizza;