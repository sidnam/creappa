import  React, { Component }  from "react";
import { Link } from "react-router-dom"
import "./app.css";
import rectangle from "../images/rectangle-2@2x.svg";
import { default as logo } from "../images/Logo/Creappa logo.svg";
import "bootstrap/dist/css/bootstrap.min.css"

class Newnavbar extends Component{

  render(){
    return(
    <div className="">
      <nav className="navbar navbarTopC navbar-expand-lg navbar-light fixed sticky-top" data-toggle="sticky-onscroll" >

      <div className="container">
        <a className="navbar-brand" href="#">
              <img src={logo} className="logo" alt=""/>
        </a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarResponsive">
          <ul className="navbar-nav ml-auto">
            <li className="nav-item sofiapro-regular-normal-black-15px">
              <Link className="nav-link" to="/mission" style={{marginRight:"20px"}}>Mission</Link>
            </li>
            <li className="nav-item sofiapro-regular-normal-black-15px">
              <Link className="nav-link" to="/howto"style={{marginRight:"20px"}}>Caratteristiche</Link>
            </li>
            <li className="nav-item sofiapro-regular-normal-black-15px" style={{marginRight:"20px"}}>
              <Link className="nav-link" to="/pricing">Prezzi</Link>
            </li>
            <li className="nav-item sofiapro-regular-normal-black-15px" style={{marginRight:"20px"}}>
              <Link className="nav-link" to="/contactus">Supporto</Link>
            </li>
            <li className="nav-item accedi-89515">
              <Link to="/login" href="#" class="loginContainer nav-link btn-mobile">
                  ACCEDI
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </nav> 
    </div>
    );
  }
}

export default Newnavbar;