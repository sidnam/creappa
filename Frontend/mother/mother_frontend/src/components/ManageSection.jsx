import React, {Component} from "react"
import {Row, Col} from "react-bootstrap"

class ManageSection extends Component{

    constructor(props){
        super(props)
    }

    render(){
        return(
            <Row fluid className="manage-sec" style={{width:"90px", height:"90px"}}>
                <Col fluid className="justify-content-center">
                <img src={this.props.image} style={{ height:"30px", width:"30px", marginBottom:"3px"}}/>
                <Row fluid className="justify-content-center">
                    <span className="manage-section-title">{this.props.title}</span>
                </Row>
                </Col>
            </Row>
        )
    }
}

export default ManageSection