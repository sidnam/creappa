import React, { Component } from "react"
import '../styles/sya.css'
import {Card, Row, Button} from 'react-bootstrap'

var QRCode = require('qrcode.react');

class ShareyaApp extends Component{

    constructor(props){
        super(props)

    }

    copyLink(){
        var linkCopier=document.createElement("input");
        var mainDiv=document.getElementById("main-div");
        mainDiv.append(linkCopier);
        linkCopier.value=this.props.url;
        linkCopier.select();
        linkCopier.setSelectionRange(0, 99999);
        document.execCommand("copy");
        mainDiv.remove(linkCopier);
        alert("Link copiato: " + linkCopier.value);
    }

    render(){
        return(
            <div id="main-div">
                <Card className="syacard">
                    <center>
                        <Row className="justify-content-sm-center">
                            <div className="sya-title">
                                Condividi la tua applicazione
                            </div>
                        </Row>
                        <Row className="justify-content-sm-center">
                        <QRCode className="qrcode" value={this.props.url}/>
                        </Row>
                        <Row className="justify-content-sm-center">
                            <div className="sya-title2">
                                Scansiona
                            </div>
                        </Row>
                        <Row className="justify-content-sm-center">
                            <div className="sya-par">
                                Oppure, se la tua fotocamera non dovesse avere un QR-Code scanner:
                            </div>
                        </Row>
                        <Row>
                            <Button className="gradient-btn" variant="light" block onClick={this.copyLink.bind(this)}><div className="btnTitle">Premi qui</div></Button>
                        </Row>
                    </center>
                </Card>
            </div>
        )
    }
}
export default ShareyaApp