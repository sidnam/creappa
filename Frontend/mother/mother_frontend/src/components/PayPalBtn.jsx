import { PayPalButton } from "react-paypal-button-v2";
import React, { Component } from "react";
import Cookies from 'universal-cookie';
import qs from 'qs';
import { TransferWithinAStationSharp } from "@material-ui/icons";
 
class PayPalBtn extends Component {

    async componentDidMount(){
        this.state={
            payment: await this.checkPayment(),
            plan: this.props.type
        }
    }

    async checkPayment(){
        const axios = require('axios').default;
        var account=new Cookies();
        var bodyjson={
            email:account.get("account").email,
            password:sessionStorage.getItem("pwd")
        }
        const resp = await axios.post('https://api.creappa.com/getAccountInfo',bodyjson);
        if(resp.data.status==="successful"){
            var payment=resp.data.info.payment;
            return payment;
        } else {
            alert("Il recupero delle informazioni ha causato qualche problema. Controlla la tua connessione a internet, se il problema persiste contatta l'assistenza.")
            window.location.pathname="/appsmanagement"
        }
    }

    async setPaymentDone(id){
        console.log(id)
        var axios=require('axios').default;
        var account=new Cookies();
        var bodyjson={
            email:account.get("account").email,
            password:sessionStorage.getItem("pwd"),
            fieldToEdit:"payment",
            newValue:id
        }
        const resp = await axios.post('https://api.creappa.com/editAccountField',bodyjson);
        if(resp.data.status==="successful"){
            var bodyjson={
                email:account.get("account").email,
                password:sessionStorage.getItem("pwd"),
                fieldToEdit:"plan",
                newValue:this.state.plan
            }
            const resp2 = await axios.post('https://api.creappa.com/editAccountField',bodyjson);
            if(resp2.data.status==="successful"){
                return;
            } else {
                alert("Il pagamento è andato a buon fine, ma sembra esserci qualche problema con i nostri server. Contatta l'assistenza.")
                window.location.pathname="/badpay"
            }
        } else {
            alert("Il pagamento è andato a buon fine, ma sembra esserci qualche problema con i nostri server. Contatta l'assistenza.")
            window.location.pathname="/badpay"
        }
    }

    render() {
        return (
            <PayPalButton
                options={{
                    vault: true,
                    clientId:"ARsQyOh4CzmSbX1dPH61I4fJ1FePhjoKrxlr890-qKletuKWRJS1HLEao8MdQeaLgebDcDITK5kSxC92"
                }}
                createSubscription={(data, actions) => {
                    if(this.state.plan==="0"){
                        return actions.subscription.create({
                            plan_id: 'P-6U279109JP2799001L6JMN3A'
                        })
                    } else {
                        return actions.subscription.create({
                            plan_id: 'P-5AG06789X54687942L6JMO6I'
                        });
                    }
                }}
                onApprove={(data, actions) => {
                    this.setPaymentDone(data.subscriptionID);
                    window.location.pathname="/goodpay";
                    return actions.subscription.get().then(function(details) {
                    });
                }}
                catchError={(data, actions)=>{
                    window.location.pathname="/badpay";
                    return actions.subscription.get().then(function(details) {
                    });
                }}
            />
        );
    }
}
export default PayPalBtn
