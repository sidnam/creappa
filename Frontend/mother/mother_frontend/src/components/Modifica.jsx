import React, { Component } from "react"
import NavbarAdmin from "./NavbarAdmin"
import { Link } from "react-router-dom"
import Cookies from 'universal-cookie'
import { Col, Row } from "react-bootstrap"

class Modifica extends Component {
    constructor(props) {
        super(props)
        this.state = { componente: "Schedule" }
        this.aggiungiComponente = this.aggiungiComponente.bind(this)
        this.refreshComponents = this.refreshComponents.bind(this)
    }

    async aggiungiComponente() {
        var componente;
        switch (this.state.componente) {
            case "About":
                componente = [
                    {
                        "type": "About",
                        "testo": "Inserisci il testo da mostrare",
                        "titolo": "Chi siamo",
                        "immagine": "https://www.ccia-net.com/wp-content/uploads/2016/10/400x400-image.jpg"
                    }
                ]
                break
            case "Shop":
                componente = [
                    {
                        "type": "Shop",
                        "titolo": "Negozio",
                        "testo": "inserisci un testo",
                        "prodotti": []
                    }
                ]
                break
            case "Schedule":
                componente = [
                    {
                        "type": "Schedule",
                        "titolo": "Orario",
                        "testo": "inserisci un testo",
                        "orari": [
                            {
                                "giorno": "lunedì",
                                "orario": "8:00-12:00/14:00-18:00",
                            },
                            {
                                "giorno": "martedì",
                                "orario": "8:00-12:00/14:00-18:00",
                            },
                            {
                                "giorno": "mercoledì",
                                "orario": "8:00-12:00/14:00-18:00",
                            }
                        ]
                    }
                ]
                break
            case "Date":
                componente = [
                    {
                        "type": "Date",
                        "titolo": "Negozio",
                        "testo": "Prendi un appuntamento",
                    }
                ]
                break
            case "Report":
                componente = [
                    {
                        "type": "Report"
                    }
                ]
                break
            case "Webpage":
                componente = [
                    {
                        "type": "WebPage",
                        "titolo": "Il nostro sito",
                        "testo": "Visita il nostro sito",
                        "sito": "www.google.com"
                    }
                ]
                break
            case "Contacts":
                componente = [
                    {
                        "type": "Contacts",
                        "titolo": "Contatti",
                        "contatti": [
                            {
                                "nome": "Indirizzo",
                                "testo": "Via Garibaldi"
                            },
                            {
                                "nome": "numero",
                                "testo": "888 8889944"
                            },
                            {
                                "nome": "Email",
                                "testo": "nome@email.it" 
                            }
                        ]
                    }
                ]
                break
            case "Where":
                componente = [
                    {
                        "type": "Where",
                        "titolo": "Dove ci troviamo",
                        "posizione": {
                            "lat": "45.44698568174512",
                            "lng": "9.19225804848081"
                        }
                    }
                ]
                break
            case "Galleria":
                componente = [
                    {
                        "type": "Galleria",
                        "titolo": "Galleria",
                        "galleria": [
                            {
                                "immagine": "https://www.auroraseriate.it/wp-content/themes/auroraseriate/assets/images/common/400x300.gif",
                                "descrizione": "testo di prova della prima slide"
                            },
                            {
                                "immagine": "https://www.auroraseriate.it/wp-content/themes/auroraseriate/assets/images/common/400x300.gif",
                                "descrizione": "testo di prova della seconda slide"
                            },
                            {
                                "immagine": "https://www.auroraseriate.it/wp-content/themes/auroraseriate/assets/images/common/400x300.gif",
                                "descrizione": "testo di prova della terza slide"
                            }
                        ]
                    }
                ]
                break
            case "DataViewer":
                break
        }

        var cookie = new Cookies()
        const axios = require('axios').default
        const resp = await axios.post(
            'https://api.creappa.com/addComponents',
            {
                "email": cookie.get("account").email,
                "password": sessionStorage.getItem("pwd"),
                "appIndex": cookie.get("app").index,
                "subdomain": cookie.get("app").subdomain,
                "components": componente
            },
            { headers: { 'Content-Type': 'application/json' } }
        )
        console.log("risposta componente:")
        console.log(resp)
        this.refreshComponents()
        window.location.reload(false);
    }

    refreshComponents() {

    }
    render() {
        return (
            <div>
                <NavbarAdmin></NavbarAdmin>
                <div style={{ marginLeft: "5%", height: "calc(100vh - 60px)", marginTop: "10px", width: "100vw - 220px", textAlign: "center", overflow: "scroll" }}>
                    <h1 style={{ textAlign: "center" }}><strong>Modifica</strong></h1>
                    <br></br>
                    <Col>
                        <Row className="justify-content-center">
                            <h3>Aggiungi componenti</h3>
                        </Row>
                        <br></br>
                        <Row md={5} className="justify-content-center" >
                            <select name="components" value={this.state.componente} onChange={e => (this.setState({ componente: e.target.value }))} style={{ width: "100%" }}>
                                <option value="Shop">Shop</option>
                                <option value="Schedule">Schedule</option>
                                <option value="Date">Date</option>
                                <option value="Where">Where</option>
                                <option value="Report">Report</option>
                                <option value="Webpage">Webpage</option>
                                <option value="Contacts">Contacts</option>
                                <option value="About">About</option>
                                <option value="DataViewer">DataViewer</option>
                                <option value="Galleria">Galleria</option>
                            </select>
                        </Row>
                        <br />
                        <Row md={5} className="justify-content-center" >
                            <Link to="/admin"><button className="btn-primary btn" style={{ width: "100%" }} onClick={this.aggiungiComponente}>Aggiungi</button></Link>
                        </Row>
                    </Col>

                </div>
            </div>
        )
    }
}

export default Modifica;