import  React, { Component }  from "react";
import { Link } from "react-router-dom"
import "../styles/foot.css";
import {Row, Col} from 'react-bootstrap'
import FBIcon from "../images/Svg_/Social svg/facebook2.svg"
import TwitterIcon from "../images/Svg_/Social svg/twitter2.svg"
import InstaIcon from "../images/Svg_/Social svg/instagram.svg"
import YoutubeIcon from "../images/Svg_/Social svg/youtube.svg"

class Footbar extends Component{

  render(){
    return(
        <div className="footer-center">
            <Row className="justify-content-md-center cols-1 row menu-footer">
               
                <Col xs={1}>
                    <Link className="links" to="/mission">Mission</Link>
                </Col>
                <Col xs={1}>
                    <Link className="links" to="/howto">Caratteristiche</Link>
                </Col>
                <Col xs={1}>
                    <Link className="links" to="/pricing">Prezzi</Link>
                </Col>
                <Col xs={1}>
                    <Link className="links" to="/contactus">Supporto</Link>
                </Col>
  
            </Row>

            
            <Row className="justify-content-sm-center social">
                <Col xs={1} className="icons">
                    <a href="https://www.facebook.com/paltolab/" target="blank"><img src={FBIcon} alt="Facebook Icon"/></a>
                </Col>
                <Col xs={1} className="icons">
                    <a href="https://www.instagram.com/paltolab/?hl=it" target="blank"><img src={InstaIcon} alt="Instagram Icon"/></a>
                </Col>
            </Row>

            
            <Row className="legal  justify-content-sm-center">

                <Row className="justify-content-sm-center footer">
                <Col>
                  <div className="lastwrite-sx">© 2020 Creappa. Tutti i Diritti Riservati.</div>
                </Col>

                <Col>
                    <Link className="links" to="/privacypolicy">Privacy Policy</Link>
                </Col>
                <Col>
                    <Link className="links" to="/cookiepolicy">Cookie Policy</Link>
                </Col>
                <Col>
                    <Link className="links" to="/terminiecondizioni">Termini e Condizioni</Link>
                </Col>
                <Col>
                   <div className="lastwrite-dx">
                       <a className="paltolab" href="https://www.paltolab.com/" target="blank" >
                           Designed by Paltolab S.r.l.
                      </a>
                  </div>
                </Col>
                </Row>
            
            </Row>
        </div>
    );
  }
}

export default Footbar;