import React, { Component } from "react"
import caricaFile1 from "../../images/Svg_/caricafile1.svg"
import caricaFile2 from "../../images/Svg_/caricafile2.svg"
import ImpostazioniIcon from "../../images/Images/manage-sections-svgs/Impostazioni.svg"
import FileUploader from "../../utilities/FileUploader"
import "../../styles/appsmanagement.css"
import Cookies from "universal-cookie";

class Impostazioni extends Component{
    constructor(props){
        super(props);
        this.onChangeLogoHandler = this.onChangeLogoHandler.bind(this)
        this.onChangeBackgroundHandler= this.onChangeBackgroundHandler.bind(this)
        this.salva = this.salva.bind(this)

        this.state= {
            logo: "",
            sfondo: "",
            logoScritta: "",
            sfondoScritta: ""
        }
    }

    onChangeLogoHandler=event=>{
        this.setState({
          logo: event.target.files[0],
        })

        this.setState({logoScritta: "Logo inserito correttamente"})
    }

    onChangeBackgroundHandler(event){
        console.log(URL.createObjectURL(event.target.files[0]))
        this.setState({sfondoScritta: "Sfondo inserito correttamente"})
        this.setState({sfondo: event.target.files[0]})
    }

    salva(){
        var cookie = new Cookies()
        var fileUploader = new FileUploader("creappa", "AKIA4OXXCC6QQVU5Y7HM", "Qh9zS3gX5T3rbphrj1VQEpmlj6BevfQeuC2978sQ")
        fileUploader.uploadLogoAndBackground(this.state.logo, this.state.sfondo, cookie.get("app").subdomain)
            .then(async (locations) => {
                console.log(locations)
                this.setState({
                    logoScritta: "",
                    sfondoScritta: ""
                })

                alert("Risorse cambiate correttamente")
            })
            .catch(
                (error) =>  alert(error)
            )


        /*var url = URL.createObjectURL(event.target.files[0])
        console.log(url)
        this.setState({
            sfondoImg: event.target.files[0],
            sfondo : url
        })*/ 
    }

    render(){
        return(
            <div style={{padding: "30px", backgroundColor: "white", boxShadow: "5px 0 5px -2px #888"}} className="manage-section">
                <div style={{display:"inline-block", marginRight:"20px"}}>
                    <img src={ImpostazioniIcon}></img>
                </div>
                <div style={{display:"inline-block"}}>
                    <h5 style={{color: "rgba(198, 195, 189, 1)"}}>Impostazioni</h5>
                </div>
                
                <div style={{marginTop:"20px"}}>
                    <h6 style={{color: "rgba(198, 195, 189, 1)", textAlign: "left"}}>Logo app</h6>
                    <span style={{color: "rgba(198, 195, 189, 1)", fontSize: "8px", textAlign: "left"}}>massimo 250px*250px</span>
                    <br/>
                    
                    <div class="image-upload">
                        <label for="file-input">
                            <img src={caricaFile1}></img>
                        </label>
                        <input type="file" id="file-input" style={{display:"none"}} onChange={this.onChangeLogoHandler}></input> 
                    </div>
                    <p style={{color: "rgba(198, 195, 189, 1)", fontSize: "12px"}}>{this.state.logoScritta}</p>
                </div>

                <div style={{marginTop:"20px"}}>
                    <h6 style={{color: "rgba(198, 195, 189, 1)", textAlign: "left"}}>Immagine di Sfondo</h6>
                    <span style={{color: "rgba(198, 195, 189, 1)", fontSize: "8px", textAlign: "left"}}>massimo 2000px*2000px</span>
                    <br/>
                    <div class="image-upload">
                        <label for="file-input1">
                            <img src={caricaFile2} ></img>
                        </label>
                        <input type="file" id="file-input1" style={{display:"none"}} onChange={e => this.onChangeBackgroundHandler(e)}></input> 
                    </div>
                    <p style={{color: "rgba(198, 195, 189, 1)", fontSize: "12px"}}>{this.state.sfondoScritta}</p>
                    <br/>
                    <button className="share-btn gradient-btn" style={{color:"white", fontSize:"15px"}} onClick={this.salva}>Salva</button>
                </div>         
            </div>
        )
    }
}

export default Impostazioni;