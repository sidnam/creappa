import React, { Component } from "react"
import BrandingIcon from "../../images/Images/manage-sections-svgs/Branding.svg"
import "../../styles/appsmanagement.css"
import Cookies from "universal-cookie";

class Branding extends Component{
    constructor(props){
        super(props)
        this.state={
            principale: this.props.principale,
            secondario: this.props.secondario,
            sfondo: this.props.sfondo,
        }

        this.salva = this.salva.bind(this)
    }

    async salva(){
        var colori = {
            principale: this.state.principale,
            secondario: this.state.secondario,
            sfondo: this.state.sfondo,
        }

        console.log(colori)

        var cookie = new Cookies()
        const axios = require('axios').default
        const resp = await axios.post(
            'https://api.creappa.com/editColors',
            {
                "appIndex": cookie.get("app").index,
                "colors": colori,
                "email": cookie.get("account").email,
                "password": sessionStorage.getItem("pwd"),
            },
            { headers: { 'Content-Type': 'application/json' } }
        )
        
        alert("Colori salvati")
        console.log(resp);
    }

    render(){
        return(
            <div style={{padding: "30px", backgroundColor: "white", boxShadow: "5px 0 5px -2px #888"}} className="manage-section">
                <div style={{display:"inline-block", marginRight:"20px"}}>
                    <img src={BrandingIcon}></img>
                </div>
                <div style={{display:"inline-block"}}>
                    <h5 style={{color: "rgba(198, 195, 189, 1)"}}>Impostazioni</h5>
                </div>

                <div style={{marginTop: "20px"}}>
                    <h6 style={{color: "rgba(198, 195, 189, 1)"}}>Colore Principale</h6>
                    <input type="color" value={this.state.principale} style={{borderColor: "transparent"}} onChange={e => this.setState({principale: e.target.value})} style={{width: "230px", marginTop: "5px"}}></input>
                </div>

                <div style={{marginTop: "20px"}}>
                    <h6 style={{color: "rgba(198, 195, 189, 1)"}}>Colore testo</h6>
                    <input type="color" value={this.state.secondario} style={{width: "230px", marginTop: "5px"}} onChange={e => this.setState({secondario: e.target.value})}></input>
                </div>

                <div style={{marginTop: "20px"}}>
                    <h6 style={{color: "rgba(198, 195, 189, 1)"}}>Colore di sfondo</h6>
                    <input type="color" value={this.state.sfondo} style={{width: "230px", marginTop: "5px"}} onChange={e => this.setState({sfondo: e.target.value})}></input>
                </div>

                <br></br>
                <button className="share-btn gradient-btn" style={{color:"white", fontSize:"15px"}} onClick={this.salva}>Salva</button>
            </div>
        )
    }
}

export default Branding;