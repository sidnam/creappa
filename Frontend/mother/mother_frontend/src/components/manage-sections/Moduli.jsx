import React, {Component} from "react"
import { Row, Card, Modal, Button, ButtonGroup} from "react-bootstrap"
import ModuliIcon from "../../images/Images/manage-sections-svgs/Moduli.svg"
import { Link } from "react-router-dom"
import "../../styles/manage-sections.css"
import { common } from "@material-ui/core/colors"

import Schedule from "../modifica/Schedule"
import About from "../modifica/About"
import Where from "../modifica/Where"
import Contacts from "../modifica/Contacts"
import Shop from "../modifica/Shop"
import Date from "../modifica/Date"
import WebPage from "../modifica/WebPage"
import Galleria from "../modifica/Galleria"
import { Dropdown } from "react-bootstrap"
import Cookies from "universal-cookie"

function getRightComponentJson(componentType){
    console.log(componentType)
    var componente = ""
    switch (componentType) {
        case "About":
            componente = [
                {
                    "type": "About",
                    "testo": "Inserisci il testo da mostrare",
                    "titolo": "Chi siamo",
                    "immagine": "https://www.ccia-net.com/wp-content/uploads/2016/10/400x400-image.jpg"
                }
            ]
            break
        case "Shop":
            console.log("shop")
            componente = [
                {
                    "type": "Shop",
                    "titolo": "Negozio",
                    "testo": "inserisci un testo",
                    "prodotti": []
                }
            ]
            break
        case "Schedule":
            componente = [
                {
                    "type": "Schedule",
                    "titolo": "Orario",
                    "testo": "inserisci un testo",
                    "orari": [
                        {
                            "giorno": "lunedì",
                            "orario": "8:00-12:00/14:00-18:00",
                        },
                        {
                            "giorno": "martedì",
                            "orario": "8:00-12:00/14:00-18:00",
                        },
                        {
                            "giorno": "mercoledì",
                            "orario": "8:00-12:00/14:00-18:00",
                        }
                    ]
                }
            ]
            break
        case "Date":
            componente = [
                {
                    "type": "Date",
                    "titolo": "Negozio",
                    "testo": "Prendi un appuntamento",
                }
            ]
            break
        case "Report":
            componente = [
                {
                    "type": "Report"
                }
            ]
            break
        case "Webpage":
            componente = [
                {
                    "type": "WebPage",
                    "titolo": "Il nostro sito",
                    "testo": "Visita il nostro sito",
                    "sito": "www.google.com"
                }
            ]
            break
        case "Contacts":
            componente = [
                {
                    "type": "Contacts",
                    "titolo": "Contatti",
                    "contatti": [
                        {
                            "nome": "Indirizzo",
                            "testo": "Via Garibaldi"
                        },
                        {
                            "nome": "numero",
                            "testo": "888 8889944"
                        },
                        {
                            "nome": "Email",
                            "testo": "nome@email.it" 
                        }
                    ]
                }
            ]
            break
        case "Where":
            componente = [
                {
                    "type": "Where",
                    "titolo": "Dove ci troviamo",
                    "posizione": {
                        "lat": "45.44698568174512",
                        "lng": "9.19225804848081"
                    }
                }
            ]
            break
        case "Galleria":
            componente = [
                {
                    "type": "Galleria",
                    "titolo": "Galleria",
                    "galleria": [
                        {
                            "immagine": "https://www.auroraseriate.it/wp-content/themes/auroraseriate/assets/images/common/400x300.gif",
                            "descrizione": "testo di prova della prima slide"
                        },
                        {
                            "immagine": "https://www.auroraseriate.it/wp-content/themes/auroraseriate/assets/images/common/400x300.gif",
                            "descrizione": "testo di prova della seconda slide"
                        },
                        {
                            "immagine": "https://www.auroraseriate.it/wp-content/themes/auroraseriate/assets/images/common/400x300.gif",
                            "descrizione": "testo di prova della terza slide"
                        }
                    ]
                }
            ]
            break
        case "DataViewer":
            break
    }
    return componente
}

class Moduli extends Component {
    constructor(props){
        super(props)
        this.state = {
            selectedComponent: -1,
            modal: false,
            availableComponents : [],
            dropdownItems : []
        }
        this.showModal = this.showModal.bind(this)
        this.closeModal = this.closeModal.bind(this)
        this.modificaComponent = <div></div>
        this.addComponent = this.addComponent.bind(this)
    }

    async componentDidMount(){
        var axios = require("axios").default
        var data = await axios.get('https://api.creappa.com/getComponentsTypeList')
        console.log(data.data.componentsTypes)
        this.setState({availableComponents : data.data.componentsTypes})
    }

    async addComponent(componentType){
        var axios = require("axios").default
        var cookie = new Cookies()
        var bodyjson = {
            email : cookie.get('account').email,
            password: sessionStorage.getItem('pwd'),
            subdomain: cookie.get('app').subdomain,
            appIndex: cookie.get('app').index,
            components: getRightComponentJson(componentType),
        }
        var data =await axios.post('https://api.creappa.com/addComponents', bodyjson)
        console.log(data.data)
        if(data.data.status == "successful"){
            alert('Componente aggiunto correttamente')
            window.location.reload()
        }
        else{
            alert('Qualcosa è andato storto...')
        }
    }

    showModal(componente, componentIndex){

        var modalComponent = ""

        switch (componente.type) {
            case "Where":
                modalComponent = <Where posizione={componente.posizione} titolo={componente.titolo} testo={componente.testo} position={componente.posizione} index={componentIndex}/>
                break
            case "About":
                modalComponent = <About titolo={componente.titolo} testo={componente.testo} immagine={componente.immagine} index={componentIndex} />
                break
            case "Contacts":
                modalComponent = <Contacts index={componentIndex} contatti={componente.contatti} titolo={componente.titolo}/>
                break
            case "Schedule":
                modalComponent = <Schedule index={componentIndex} orari={componente.orari} titolo={componente.titolo}/>
                break
            case "Shop":
                modalComponent =  <Shop titolo={componente.titolo} testo={componente.testo} prodotti={componente.prodotti} index={componentIndex}/>
                break
            //return <Route exact path={"/admin/modifica/DataViewer" + this.controllo(res.data.components, componente, i)} render={() => <DataViwer />}></Route>
            case "Date":
                console.log(componente)
                modalComponent = <Date titolo={componente.titolo} testo={componente.testo} index={componentIndex}/>
                break
            case "WebPage":
                modalComponent = <WebPage titolo={componente.titolo} testo={componente.testo} sito={componente.sito} index={componentIndex}/>
                break
            case "Galleria":
                modalComponent = <Galleria titolo={componente.titolo} galleria={componente.galleria} index={componentIndex}/>
                break
        }

        this.setState({
            modificaComponent:modalComponent,
            modal: true
        })
    }

    closeModal(){
        this.setState({modal: false})
    }

    render(){
        //try{
        return(
            <div className="manage-section" >
                <Row className="justify-content-center" fluid>
                    <img className="manage-icon" src={ModuliIcon}></img>
                    <h2 className="manage-section-title1">Moduli</h2>
                </Row>
                
                <div style={{marginTop:"20px"}}>
                    <h4 className="installati-title">Installati</h4>
                    <div>
                    {
                        this.props.components.map((item, i) => {
                            switch(item.content.type){
                                case "Report":
                                    break
                                default:
                                    return(
                                        <Card className="moduli-container" key={i} onClick={() => this.showModal(item.content, item.index)} style={{marginTop:"2%"}}>
                                            <Card.Header>
                                                {item.content.type}
                                            </Card.Header>
                                        </Card>
                                    )
                                break
                            }

                        })
                    }
                    </div>
                </div>

                <div style={{marginTop:"20px"}}>
                    <Dropdown as={ButtonGroup}>
                        
                        <Dropdown.Toggle className="gradient-btn" style={{border:"0px", borderRadius:"0.8em"}}>Aggiungi</Dropdown.Toggle>
                        <Dropdown.Menu>
                        {   
                            this.state.availableComponents.map((component, index) => {
                                console.log(component.type)
                                return (<Dropdown.Item className="dditem" onClick={() => this.addComponent(component.type)}>
                                    {component.type}
                                </Dropdown.Item>)
                            }) 
                        }
                        </Dropdown.Menu>
                    </Dropdown>
                </div> 
                <Modal show={this.state.modal} onHide={this.closeModal} dialogClassName="gestisci-app-modal" size="lg">
                    <Modal.Body>
                        {
                            this.state.modificaComponent
                        }
                    </Modal.Body>
                </Modal>   
            </div>
        )
        /*} catch (TypeError){
            return(
                <div>sucasuca</div>
            )
        }*/
    }

}

export default Moduli