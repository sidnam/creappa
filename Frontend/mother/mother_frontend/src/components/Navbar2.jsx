import React, { Component } from "react"
import 'bootstrap/dist/css/bootstrap.css'
import {Link} from "react-router-dom"
import {Nav, Navbar, Form, Col, Row} from "react-bootstrap"
import Popup from "reactjs-popup"
import Cookies from 'universal-cookie'

class Navbar2 extends Component {

    cancelCookies(){
        var cookie=new Cookies();
        cookie.remove('account');
    }

    render() {
        return (
            <div>
                <Navbar bg="#00FFFFFF" expand="lg">
                    <Col>
                        <Row>
                            <Link to={"/myapps"} className="item-nero navbar-brand"><strong>CreaAppa</strong></Link>
                            <Navbar.Toggle aria-controls="basic-navbar-nav" />
                            <Navbar.Collapse id="basic-navbar-nav">
                                <Nav className="mr-auto">
                                    <Popup trigger={
                                        <Nav.Link href="/accsetting">Impostazioni</Nav.Link>
                                    }
                                    on={['hover', 'focus']}
                                    position="bottom left">
                                        <span>Accedi alle impostazioni del tuo profilo, cambia password o disdici l'abbonamento alla piattaforma.</span>
                                    </Popup>
                                </Nav>
                                <small className="contatti">
                                    <Col>
                                        <Row>
                                            <small>TEL: +39 347 158 0503</small>
                                        </Row>
                                        <Row>
                                            <small>MAIL: team@paltolab.com</small>
                                        </Row>
                                        <Row>
                                            <small>00156 Via Carlo Arturo Jemolo 83, Roma</small>   
                                        </Row>
                                    </Col>
                                </small>
                                <Form inline>
                                    <Link to={"/"} className="item-nero"><button className="btn" style={{backgroundColor: "#74d691"}} onClick={this.cancelCookies.bind(this)}>Logout</button></Link>
                                </Form>
                            </Navbar.Collapse> 
                        </Row>
                        <Row>
                            <div className="info">
                                <small><small><small>Copyright © 2020 PaltoLab | Tutti i diritti sono riservati</small></small></small>
                            </div>
                        </Row>
                    </Col>
                    
                </Navbar>
            </div>
        )
    }

}

export default Navbar2