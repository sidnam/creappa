import React, { Component } from "react"
import {Navbar, Nav, Button } from "react-bootstrap"
import "../styles/navbar.css"
import "../styles/appsmanagement.css"
import logo from '../images/Logo/Creappa logo.svg'
import ShareLogo from "../images/Images/share-icon.svg"
import { Link } from "react-router-dom"
import Cookies from 'universal-cookie'
import LogoutIcon from "../images/Svg_/logout.svg"
import settingsIcon from "../images/Svg_/account.svg"

class NavBarAdmin extends Component {

    constructor(props){
        super(props);
    }
    
    showAction = () => {
        this.props.showApps();
    }

    shareAction = () => {
        this.props.share();
    }

    render(){
        return(
            <Navbar className="navbar" expand="md">
                <Navbar.Brand href="/admin" style={{margin:"2%"}}>
                    <Link to="/appsmanagement"><img src={logo} alt="logo"/></Link>
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link>
                            <Button className="gestisci-app-btn" onClick={this.showAction}>GESTISCI APP</Button>
                        </Nav.Link>
                        <Nav.Link>
                            <Button className="share-btn gradient-btn" onClick={this.shareAction}>
                                <img src={ShareLogo}></img>
                                CONDIVIDI
                            </Button>
                        </Nav.Link>
                        <Nav.Link>
                            {this.props.appname}
                        </Nav.Link>
                    </Nav>
                </Navbar.Collapse>
                <Navbar.Collapse inline className="justify-content-end">
                    <Nav.Link href="/">
                            <Button className="logout-btn" variant="light">
                                <img src={LogoutIcon} className="logout-icon" alt="logout"></img>
                            </Button>
                    </Nav.Link>
                    <Nav.Link href="/accsetting">
                            <Button className="logout-btn" variant="light">
                                <img src={settingsIcon} className="logout-icon" alt="settings"></img>
                            </Button>
                    </Nav.Link>
                </Navbar.Collapse>
            </Navbar>
        )
    }
}

export default NavBarAdmin