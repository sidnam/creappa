import React, {Component} from "react"
import {Modal, Button, Row, Col, Form} from "react-bootstrap"
import BootstrapSwitchButton from 'bootstrap-switch-button-react'
import Cookies from "universal-cookie"

class ConfirmDateButton extends Component {

    constructor(props){
        super(props)
        this.state = {
            showModal: false
        }
        this.setShowModalOff = this.setShowModalOff.bind(this)
        this.setShowModalOn = this.setShowModalOn.bind(this)
        this.handleClickButton = this.handleClickButton.bind(this)
    }

    setShowModalOff(){
        this.setState({
            showModal: false
        })
    }

    setShowModalOn(){
        this.setState({
            showModal: true,
            switchState : false
        })
    }

    async handleClickButton()  {
        console.table(this.props.dateIndex, this.state.switchState)
        const axios = require('axios').default
        const cookies = new Cookies()
        const resp = await axios.post(
            'https://api.creappa.com/confirmDate',
            {
                "email" : cookies.get('account').email,
                "password" : sessionStorage.getItem("pwd"),
                "dateIndex" : this.props.dateIndex,
                "appIndex" : cookies.get('app').index,
                "approved" : this.state.switchState ? 1 : -1
            }
        )
        if(resp.data.status === "successful"){
            alert('Modifica avvenuta con successo!')
            window.location.reload()
        } else alert('Qualcosa è andato storto... \n:(')
    }

    render(){
        return (
            <div>
                <Button className="gradient-btn" variant="outline-light" onClick={this.setShowModalOn} disabled={this.props.approvationDatetime !== null ? true : false} >
                    {this.props.approvationDatetime !== null ? this.props.disabledTextButton : this.props.enabledTextButton}
                </Button>
                <Modal show={this.state.showModal} onHide={this.setShowModalOff} position="center" size="lg" centered>
                    <Modal.Header closeButton>
                    <Modal.Title ><strong></strong></Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Col>
                            <Row className="justify-content-center">
                                <Form.Label for="switch"><strong>Scegli se accettare o meno l'appuntamento</strong></Form.Label>
                                <BootstrapSwitchButton style="w-100 mx-3" className="switch" onlabel='Accetta' offlabel='Rifiuta' checked={this.state.switchState} 
                                    onChange={(checked) => { 
                                        this.setState({switchState: checked})
                                    }}
                                />
                            </Row>
                        </Col>
                    </Modal.Body>
                    <Modal.Footer>
                    <Button id="create-date" variant="secondary" onClick={this.setShowModalOff}>
                        Chiudi
                    </Button>
                    <Button className="gradient-btn" variant="outline-light" onClick={this.handleClickButton}>
                        Conferma
                    </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}

export default ConfirmDateButton