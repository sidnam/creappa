import React, { Component } from "react"
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row'
import "../styles/navbar.css"

import {Link} from "react-router-dom"
import { Col } from "react-bootstrap"

class NavBar extends Component {
    render(){
        return(
            <Navbar bg="#00FFFFFF" expand="lg">
                <Col>
                    <Row>
                        <Link to={"/"}><Navbar.Brand><strong>Creappa</strong></Navbar.Brand></Link>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="mr-auto">
                                <Nav.Link href="/nv">Mission</Nav.Link>
                                <Nav.Link href="/howto">How-To</Nav.Link>
                                <Nav.Link href="/pricing">Pricing</Nav.Link>
                                <Nav.Link href="/signup">Registrati!</Nav.Link>
                            </Nav>
                            <small className="contatti">
                                <Col>
                                    <Row>
                                        <small>TEL: +39 347 158 0503</small>
                                    </Row>
                                    <Row>
                                        <small>MAIL: team@paltolab.com</small>
                                    </Row>
                                    <Row>
                                        <small>00156 Via Carlo Arturo Jemolo 83, Roma</small>   
                                    </Row>
                                </Col>
                            </small>
                            <Form inline>
                                <Link to={"/login"}><Button variant="outline-dark">Login</Button></Link>
                            </Form>
                        </Navbar.Collapse>
                    </Row>
                    <Row>
                        <div className="info">
                            <small><small><small>Copyright © 2020 PaltoLab | Tutti i diritti sono riservati</small></small></small>
                        </div>
                    </Row>
                </Col>
            </Navbar>
        )
    }
}

export default NavBar