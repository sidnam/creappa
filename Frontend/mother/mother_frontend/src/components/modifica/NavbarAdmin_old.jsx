import React, { Component } from "react"
import "../styles/navbaradmin.css"
import { Navbar, Nav, NavDropdown, Form, FormControl, Button, Container, Col, Row, Modal, InputGroup } from "react-bootstrap"
import { Link } from "react-router-dom"
import Cookies from 'universal-cookie'
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';
import {Share, FileCopyOutlined} from "@material-ui/icons"

var QRCode = require('qrcode.react');


class NavbarAdmin extends Component {
    constructor(props) {
        super(props)
        this.state = {
            sidebarOpen: true,
            url: "",
            subdomain : "",
            showModal: false
        }
        this.getComponents = this.getComponents.bind(this)
        this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this)
        this.controllo = this.controllo.bind(this)
        this.handleClose = this.handleClose.bind(this)
        this.handleShow = this.handleShow.bind(this)
    }

    onSetSidebarOpen() {
        this.setState({ sidebarOpen: !this.state.sidebarOpen })
    }

    componentDidMount() {
        this.getComponents()
        var cookies = new Cookies();
        var url = 'https://' + cookies.get('app').subdomain + '.creappa.com/'
        this.setState(
            {
                url: url,
                subdomain : cookies.get('app').subdomain
            }
        )
    }

    async getComponents() {
        var cookie = new Cookies()
        var bodyApp = {
            "appIndex": cookie.get("app").index,
            "subdomain": cookie.get("app").subdomain
        }

        fetch("https://api.creappa.com/getConfigs", {
            method: "POST",
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(bodyApp)
        }).then(res => res.json())
            .then(res => {
                console.log("res")
                console.log(res)
                this.setState({
                    items: res.components.map((item, i) => (
                        (item.content.type === "Report")?console.log():<Link to={"/admin/modifica/" + item.content.type +  this.controllo(res.components, item, i)} className="text-hover"><li key={i} className="componente-sidenavbar"> - {item.content.type}</li></Link>
                    ), this)
                })

            }).catch((error) => {
                console.log("errore " + error)
            });
    }

    controllo(componenti, componente, indice){
        var somma = 0
        for(var i = 0; i < indice; i++){
            if(componenti[i].content.type === componente.content.type)
                somma += 1
        }
        return somma
    }

    handleClose(){
        this.setState({showModal:false})
    }

    handleShow(){
        this.setState({showModal:true})
    }

    render() {
        

        return (
            <div style={{height:"100%"}}>
                <Navbar bg="dark" variant="dark">
                    <Link to="/myapps"><Navbar.Brand className="text-hover">Creappa</Navbar.Brand></Link>
                    <button className="btn btn-link btn-sm order-1 order-lg-0" onClick={this.onSetSidebarOpen}><i className="fas fa-bars"></i></button>
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto"></Nav>
                        <Link to="/admin" className="text-hover"><h2 style={{color:"#ffffff"}} className="text-hover"><strong>{this.state.subdomain}</strong></h2></Link>
                        <NavDropdown title={<i className="fas fa-user fa-fw user-spaziatura"></i>} id="basic-nav-dropdown">
                            <NavDropdown.Divider/>
                            <NavDropdown.Item >Logout</NavDropdown.Item>
                        </NavDropdown>
                    </Navbar.Collapse>
                </Navbar>
                <div style={{ width: "150px", float: "left", display: (this.state.sidebarOpen)? "inline":"none", zIndex:"10"}} className="sidenavbar">
                    <ul className="navbar-item-deck">
                        <li className="header-sidenavbar">Negozio</li>
                        <Popup trigger={
                            <Link to="/admin/risorse" className="text-hover"><li className="item-sidenavbar">Risorse</li></Link>
                        }
                        on={['hover','focus']}
                        position='right center'
                        >
                            <span>Visualizza e modifica il logo, lo sfondo, il font, i colori e le icone dell'applicazione</span>
                        </Popup>
                        <Popup trigger={
                            <Link to="/admin/appuntamenti" className="text-hover"><li className="item-sidenavbar">Appuntamenti</li></Link>
                        }
                        on={['hover','focus']}
                        position='right center'
                        >
                            <span>Visualizza e approva gli appuntamenti che ti vengono richiesti</span>
                        </Popup>
                        <Popup trigger={
                            <Link to="/admin/modifica" className="text-hover"><li className="item-sidenavbar">Componenti</li></Link>
                        }
                        on={['hover','focus']}
                        position='right center'
                        >
                            <span>Aggiungi dei nuovi componenti alla tua app, oppure modifica quelli che hai già cliccando sui corrispondenti nomi qui sotto.</span>
                        </Popup>
                        {this.state.items}
                        {/*<li className="header-sidenavbar" style={{ marginTop: "20px" }} >Statistiche</li>
                        <Link to="/admin/visualizza"><li className="item-sidenavbar">Visualizza</li></Link>*/}
                            <Row className="justify-content-center">
                                <Button className="justify-content-center modal-margin share-button" style={{position:"absolute", bottom:"5%"}} onClick={this.handleShow}>
                                    Condividi!
                                    <Share>

                                    </Share>
                                </Button>
                            </Row>
                            <Modal show={this.state.showModal} onHide={this.handleClose}>
                                <Modal.Header closeButton>
                                <Modal.Title>Condividi la tua applicazione!</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <Row>
                                        <Col md={4} className="justify-content-center">
                                            <Row className="justify-content-center" >
                                                <QRCode value={this.state.url}/>
                                            </Row>
                                        </Col>
                                        <Col>
                                            <Row className="justify-content-center">
                                                <h6>Condividi questo QRCode oppure il link qua sotto per diffondere la tua app!</h6>
                                            </Row>
                                            <Row>
                                                <InputGroup className="mb-3">
                                                    <FormControl
                                                    value={this.state.url}
                                                    />
                                                    <InputGroup.Append>
                                                        <div style={{verticalAlign:"center"}}>
                                                            <FileCopyOutlined/>
                                                        </div>
                                                    </InputGroup.Append>
                                                </InputGroup>
                                            </Row>
                                        </Col>
                                    </Row>
                                </Modal.Body>
                                <Modal.Footer>
                                <Button variant="secondary" onClick={this.handleClose}>
                                    Close
                                </Button>
                                </Modal.Footer>
                            </Modal>
                            <Container className="modal-popup" >
                                
                            </Container>
                        
                    </ul>
                </div>

            </div>
        )
    }
}

export default NavbarAdmin;