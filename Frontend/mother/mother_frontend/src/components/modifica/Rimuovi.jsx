import React, { Component } from "react"
import { Link } from "react-router-dom"
import Cookies from 'universal-cookie'

class Rimuovi extends Component{
    constructor(props){
        super(props)
        this.rimuovi = this.rimuovi.bind(this)
    }
    render(){
        return(
            <div>
                <Link to="/appsmanagement"><button style={{width:"50%", float:"left"}} class="btn-danger btn" onClick={this.rimuovi}>ELIMINA</button></Link>
            </div>
        )
    }

    async rimuovi(){    
        var cookie = new Cookies()
        const axios = require('axios').default
        const resp = await axios.post(
            'https://api.creappa.com/deleteComponents',
            {
                "email": cookie.get("account").email,
                "password": sessionStorage.getItem("pwd"),
                "componentsIndexes": [this.props.index],
                "appIndex": cookie.get("app").index, 
            },
            { headers: { 'Content-Type': 'application/json' } }
        );
        window.location.reload(false);
        console.log(resp) 
    }
}

export default Rimuovi;