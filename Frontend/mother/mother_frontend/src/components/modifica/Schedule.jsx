import React, { Component } from "react"
import NavbarAdmin from "../NavbarAdmin"
import Rimuovi from "./Rimuovi"
import Salva from "./Salva"
import { Link } from "react-router-dom"

class Schedule extends Component {
    constructor(props) {
        super(props)
        this.modificaOrario = this.modificaOrario.bind(this)
        this.aggiungiOrario = this.aggiungiOrario.bind(this)
        this.rimuoviOrario = this.rimuoviOrario.bind(this)
        this.modificaGiorno = this.modificaGiorno.bind(this)
        this.salva = this.salva.bind(this)
        this.state = {
            titolo: this.props.titolo,
            orari: this.props.orari
        }
    }

    rimuoviOrario(i) {
        var joined = this.state.orari
        joined.splice(i, 1)
        this.setState({ orari: joined })
    }

    aggiungiOrario() {
        var joined = this.state.orari
        joined.push({
            "giorno": "giorno",
            "orario": "orario"
        })
        this.setState({ orari: joined })
    }
    salva() {
        var body = {
            "titolo": this.state.titolo,
            "orari": this.state.orari,
            "type": "Schedule"
        }
        Salva(body, this.props.index)
    }
    modificaOrario(orario, i) {
        var joined = this.state.orari
        joined[i].orario = orario
        this.setState({ orari: joined })
    }
    modificaGiorno(giorno, i) {
        console.log("ciao")
        var joined = this.state.orari
        joined[i].giorno = giorno
        this.setState({ orari: joined })
    }
    render() {
        return (
            <div>
                <div style={{ marginLeft: "5%", height: "calc(100vh - 60px)", marginTop: "10px", width: "100vw - 220px", textAlign: "center", overflow: "scroll", fontFamily:'"Sofia Pro-Light", Helvetica, Arial, serif'}}>
                    <h1><strong><input value={this.state.titolo} onChange={e=>this.setState({titolo:e.target.value})} style={{textAlign:"center", fontWeight:"bold"}}></input></strong></h1>
                    <table className="tabella-orario">
                        <tr>
                            <th>Giorno</th>
                            <th>Orario</th>
                        </tr>
                        {this.state.orari.map((orario, i) => (
                            <tr>
                                <td><Link onClick={e => this.rimuoviOrario(i)}><span style={{ float: "left", color: "red" }}>X</span></Link><input onChange={e => this.modificaGiorno(e.target.value, i)} value={orario.giorno}></input></td>
                                <td><input value={orario.orario} onChange={e => this.modificaOrario(e.target.value, i)}></input></td>
                            </tr>
                        ))}
                    </table>
                    <br></br>
                    <button className="btn btn-outline-light gradient-btn" onClick={this.aggiungiOrario}>+</button>
                    <div style={{ width: "40%", padding: "10px", margin: "0 auto" }}>
                        <button className="btn-outline-light gradient-btn btn" style={{ width: "45%", float: "left" }} onClick={this.salva}>SALVA</button>
                        <Rimuovi index={this.props.index} />
                    </div>
                </div>
            </div>
        )
    }
}

export default Schedule;