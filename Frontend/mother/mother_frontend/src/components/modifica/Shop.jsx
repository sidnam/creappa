import React, { Component } from "react"
import NavbarAdmin from "../NavbarAdmin"
import Rimuovi from "./Rimuovi"
import Salva from "./Salva"
import FileUploader from "../../utilities/FileUploader"
import Cookies from "universal-cookie"
import { CircularProgress } from "@material-ui/core"

class Shop extends Component {
    constructor(props) {
        super(props)
        this.state = {
            titolo: this.props.titolo,
            testo: this.props.testo,
            prodotti: this.props.prodotti,
            loading: -1
        }
        console.log(" Index")
        console.log(this.props.index)
        console.log("prodotti:")
        console.log(this.props.prodotti)
        this.modificaProdottoDescrizione = this.modificaProdottoDescrizione.bind(this)
        this.modificaProdottoPrezzo = this.modificaProdottoPrezzo.bind(this)
        this.modificaProdottoTitolo = this.modificaProdottoTitolo.bind(this)
        this.modificaImmagineProdotto = this.modificaImmagineProdotto.bind(this)
        this.aggiungiProdotto = this.aggiungiProdotto.bind(this)
        this.rimuoviProdotto = this.rimuoviProdotto.bind(this)
        this.salva = this.salva.bind(this)
    }
    style = {
        marginTop: "10px",
        width: "80%",
        maxWidth: "800px",
        textAlign: "center",
        margin: "0 auto"
    }

    aggiungiProdotto() {
        console.log(this.state.prodotti)
        var joined = this.state.prodotti.concat({ "descrizione": "descrizione", "img": "https://www.auroraseriate.it/wp-content/themes/auroraseriate/assets/images/common/400x300.gif", "prezzo": "10", "titolo": "titolo" })
        this.setState({ prodotti: joined })
    }

    rimuoviProdotto(index) {
        console.log("index" + index)
        var joined = this.state.prodotti
        joined.splice(index, 1)
        this.setState({ prodotti: joined })
    }

    modificaProdottoTitolo(testo, i) {
        var joined = this.state.prodotti
        joined[i].titolo = testo
        this.setState({ prodotti: joined })
    }

    modificaProdottoDescrizione(testo, i) {
        var joined = this.state.prodotti
        joined[i].descrizione = testo
        this.setState({ prodotti: joined })
    }

    modificaProdottoPrezzo(testo, i) {
        var joined = this.state.prodotti
        joined[i].prezzo = testo
        this.setState({ prodotti: joined })
    }

    modificaImmagineProdotto(immagine, i, nomeImmagine) {
        this.setState({ loading: i })
        var cookie = new Cookies()
        var fileUploader = new FileUploader("creappa", "AKIA4OXXCC6QQVU5Y7HM", "Qh9zS3gX5T3rbphrj1VQEpmlj6BevfQeuC2978sQ")
        fileUploader.uploadFilePromise("resources/" + cookie.get('app').subdomain + "/" + nomeImmagine, immagine)
            .then(async (location) => {
                var joined = this.state.prodotti
                joined[i].img = location
                this.setState({ prodotti: joined })
                this.setState({ loading: -1 })
            })
    }

    salva() {
        console.log(this.state.prodotti)
        var body = {
            "titolo": this.state.titolo,
            "testo": this.state.testo,
            "prodotti": this.state.prodotti,
            "type": "Shop"
        }
        Salva(body, this.props.index)
    }
    render() {
        return (
            <div>
                <div style={{ marginLeft: "5%", height: "calc(100vh - 60px)", marginTop: "10px", width: "100vw - 220px", textAlign: "center", overflow: "scroll", fontFamily:'"Sofia Pro-Light", Helvetica, Arial, serif' }}>
                    <h1><strong>Shop</strong></h1>
                    <input placeholder="Titolo" className={"form-control"} style={this.style} value={this.state.titolo} onChange={e => (this.setState({ titolo: e.target.value }))} /> <br></br>
                    <textarea className={"form-control"} placeholder="testo" onChange={e => (this.setState({ testo: e.target.value }))} cols="1000" rows="10" style={this.style} value={this.state.testo} /> <br></br>
                    <h2>Prodotti:</h2>
                    <button className="btn btn-outline-light gradient-btn" onClick={this.aggiungiProdotto}>+</button> aggiungi prodotto
                    <div className="contenitore-immagini">
                        {
                            this.state.prodotti !== undefined && this.state.prodotti.length !== 0 ?
                                (this.state.prodotti.map((prodotto, i) => (
                                    i == this.state.loading ? <div className="loading"> <br /><CircularProgress className="crc-prg" style={{ marginBottom: "20px", marginTop:"20px" }} color='primary'></CircularProgress><br /></div> : (<div class="card card-shop spaziatura-shop" key={i}>
                                        <img alt="img" class="card-img-top immagine-shop " src={prodotto.img} alt="Card image cap" />
                                        <div class="custom-file" style={{ width: "100%", marginBottom: "20px" }}>
                                            <input type="file" onChange={e => this.modificaImmagineProdotto(e.target.files[0], i, e.target.files[0].name)} class="custom-file-input" id="inputGroupFile02" />
                                            <label class="custom-file-label" for="inputGroupFile02">Scegli immagine</label>
                                        </div>
                                        <input value={prodotto.titolo} style={{ width: "90%", margin: "0 auto", fontWeight: 550 }} className="card-header form-control titolo-shop" onChange={e => (this.modificaProdottoTitolo(e.target.value, i))}></input>
                                        <div className="card-body">
                                            <input value={prodotto.descrizione} onChange={e => (this.modificaProdottoDescrizione(e.target.value, i))} class="form-control card-text testo-shop"></input>
                                            <footer className="blockquote-footer testo-shop">Prezzo: <input size="3" style={{ borderRadius: "5px", border: "1px solid lightGrey", marginTop: "5px" }} value={prodotto.prezzo} onChange={e => this.modificaProdottoPrezzo(e.target.value, i)}></input>$ </footer>
                                        </div>
                                        <input className="btn btn-outline-danger" style={{ width: "30%", margin: "0 auto" }} value="Elimina" onClick={e => this.rimuoviProdotto(i)}></input>
                                    </div>)
                                ), this)) : <div style={{ marginTop: "30px" }}><i>Non ci sono elementi nello shop</i></div>
                        }
                    </div>
                    <br></br>
                    <div style={{ width: "40%", padding: "10px", margin: "0 auto" }}>
                        <button className="btn-outline-light gradient-btn btn" style={{ width: "45%", float: "left" }} onClick={this.salva}>SALVA</button>
                        <Rimuovi index={this.props.index} />
                    </div>
                </div>
            </div>
        )
    }
}

export default Shop;