import React, { Component } from "react"
import NavbarAdmin from "../NavbarAdmin"
import Rimuovi from "./Rimuovi"
import Salva from "./Salva"
import { Link } from "react-router-dom"

class Contacts extends Component {
    constructor(props) {
        super(props)
        this.state = {
            contatti: this.props.contatti,
            titolo: this.props.titolo
        }
        this.modificaContatto = this.modificaContatto.bind(this)
        this.modificaTitolo = this.modificaTitolo.bind(this)
        this.aggiungiContatto = this.aggiungiContatto.bind(this)
        this.rimuoviContatto = this.rimuoviContatto.bind(this)
        this.modificaTesto = this.modificaTesto.bind(this)
        this.salva = this.salva.bind(this)
    }

    salva(){
        var body = {
            "titolo": this.state.titolo,
            "contatti": this.state.contatti,
            "type": "Contacts"
        }
        Salva(body, this.props.index)
    }

    aggiungiContatto() {
        var joined = this.state.contatti
        joined.push({
            "nome": "contatto",
            "testo": "testo"
        })
        this.setState({ orari: joined })
    }
    rimuoviContatto(i) {
        var joined = this.state.contatti
        joined.splice(i, 1)
        this.setState({ contatti: joined })
    }
    modificaTesto(testo, index) {
        var joined = this.state.contatti
        joined[index].testo = testo
        this.setState({ contatti: joined })
    }
    modificaContatto(contatto, index) {
        var joined = this.state.contatti
        joined[index].nome = contatto
        this.setState({ contatti: joined })
    }
    modificaTitolo(titolo) {

    }
    render() {
        console.log(this.state.contatti)
        return (
            <div>
                <div style={{ marginLeft: "5%", height: "calc(100vh - 60px)", marginTop: "10px", width: "100vw - 220px", textAlign: "center", overflow: "scroll", fontFamily:'"Sofia Pro-Light", Helvetica, Arial, serif' }}>
                    <h1><input style={{borderRadius:"20px"}} value={this.state.titolo} onChange={e=>this.setState({titolo:e.target.value})} style={{textAlign:"center", fontWeight:"bold"}}></input></h1>                    <table className="tabella-orario">
                        <tr>
                            <th>Contatto</th>
                            <th>Testo</th>

                        </tr>
                        {this.state.contatti.map((contatto, i) => (
                            <tr>
                                <td><Link onClick={e => this.rimuoviContatto(i)}><span style={{ float: "left", color: "red" }}>X</span></Link><input className={"form-control"} style={{width:"40%", margin:"0 auto"}} onChange={e => this.modificaContatto(e.target.value, i)} value={contatto.nome}></input></td>
                                <td><input value={contatto.testo} className={"form-control"} onChange={e => this.modificaTesto(e.target.value, i)}></input></td>
                            </tr>
                        ))}
                    </table>
                    <button className="btn btn-outline-light gradient-btn" onClick={this.aggiungiContatto}>+</button>
                    <div style={{ width: "40%", padding: "10px", margin: "0 auto" }}>
                        <button className="btn-outline-light btn gradient-btn" style={{ width: "45%", float: "left" }} onClick={this.salva}>SALVA</button>
                        <Rimuovi index={this.props.index} />
                    </div>

                </div>
            </div>
        )
    }
}

export default Contacts;