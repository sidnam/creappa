import React, { Component } from "react"
import "../../styles/style.css"
import NavbarAdmin from "../NavbarAdmin"
import Rimuovi from "./Rimuovi"
import { uploadFile } from 'react-s3';
import Cookies from "universal-cookie";
import Salva from "./Salva"
import { Form, Col, Row } from "react-bootstrap";
import FileUploader from "../../utilities/FileUploader"

class About extends Component {
    constructor(props) {
        super(props)
        this.state = {
            titolo: this.props.titolo,
            testo: this.props.testo,
            selectedImgFile: "",
            imgBlobPath: ""
        }
        console.log(this.props.index)
        this.salva = this.salva.bind(this)
        this.onChangeImgTarget = this.onChangeImgTarget.bind(this)
    }

    style = {
        marginTop: "10px",
        width: "80%",
        maxWidth: "800px",
        textAlign: "center",
    }

    async salva() {
        var img = ""
        var cookie = new Cookies()

        var titolo = this.state.titolo.replace("'", "\'")
        var testo = this.state.testo.replace("'", "\'")
        console.log(testo)
        var fileUploader = new FileUploader("creappa", "AKIA4OXXCC6QQVU5Y7HM", "Qh9zS3gX5T3rbphrj1VQEpmlj6BevfQeuC2978sQ")
        fileUploader.uploadFilePromise("resources/" + cookie.get('app').subdomain + "/" + this.state.selectedImgFile.name, this.state.selectedImgFile)
            .then(async (location) => {
                console.log(location)
                let body = {
                    "type": "About",
                    "titolo": titolo,
                    "testo": testo,
                    "immagine": location
                }
                location !== null ? await Salva(body, this.props.index) : console.log("null")
            })

    }

    onChangeImgTarget = event => {
        console.log(event.target.files[0])
        var url = URL.createObjectURL(event.target.files[0])
        this.setState({
            selectedImgFile: event.target.files[0],
            loaded: false,
            imgBlobPath: url
        })
    }

    getAboutImage(components) {
        var imgPath = ""
        components.map((component, index) => {
            if (component.content.type === "About")
                imgPath = component.content.immagine
        })
        return imgPath
    }

    async componentDidMount() {
        const cookie = new Cookies()
        const axios = require('axios').default
        const res = await axios.post(
            'https://api.creappa.com/getConfigs',
            {
                "appIndex": cookie.get("app").index,
                "subdomain": cookie.get("app").subdomain,
            },
            { headers: { 'Content-Type': 'application/json' } }
        )
        var alreadyExistingAboutImage = this.getAboutImage(res.data.components)
        this.setState({ imgBlobPath: alreadyExistingAboutImage })

    }

    render() {
        return (
            <div>
                <div style={{ marginLeft: "5%", height: "calc(100vh - 60px)", marginTop: "10px", width: "100vw - 220px", textAlign: "center", overflow: "scroll", fontFamily:'"Sofia Pro-Light", Helvetica, Arial, serif' }}>
                    <h1><strong>About</strong></h1>
                    <Form>
                        <Col>
                            <Form.Row className="justify-content-center">
                                <Form.Control placeholder="Titolo" style={this.style} value={this.state.titolo} onChange={e => (this.setState({ titolo: e.target.value }))} /> <br></br>
                                <Form.Control as="textarea" placeholder="Testo" onChange={e => { e.target.value.includes("'") ? alert('Non puoi inserire il carattere: "\'"') : (this.setState({ testo: e.target.value })) }} cols="1000" rows="10" style={this.style} value={this.state.testo} /> <br></br>
                            </Form.Row>
                            <Form.Row className="justify-content-center">
                            </Form.Row>
                            <img src={this.state.imgBlobPath} style={this.style} alt="Copertina"></img>
                            <br/>
                            <div class="custom-file" style={this.style}>
                                <input type="file" onChange={this.onChangeImgTarget} class="custom-file-input" id="inputGroupFile02" />
                                <label class="custom-file-label" for="inputGroupFile02">Scegli immagine</label>
                            </div>
                        </Col>
                    </Form>
                    <div style={{ width: "40%", padding: "10px", margin: "0 auto" }}>
                        <button className="btn-outline-light btn gradient-btn" style={{ width: "45%", float: "left" }} onClick={this.salva}>SALVA</button>
                        <Rimuovi index={this.props.index} />
                    </div>
                </div>    
            </div>
        )
    }
}

export default About;