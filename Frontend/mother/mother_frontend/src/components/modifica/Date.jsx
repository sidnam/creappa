import React, { Component } from "react"
import NavbarAdmin from "../NavbarAdmin"
import Rimuovi from "./Rimuovi"
import Salva from "./Salva"
import { Link } from "react-router-dom"

class Date extends Component {
    constructor(props) {
        super(props)
        this.state = {
            titolo: this.props.titolo,
            testo: this.props.testo
        }
        this.salva = this.salva.bind(this)
    }

    salva(){
        var body = {
            "titolo": this.state.titolo,
            "testo": this.state.testo,
            "type": "Date"
        }
        Salva(body, this.props.index)
    }
    render() {
        return (
            <div>
                <div style={{ marginLeft: "5%", height: "calc(100vh - 60px)", marginTop: "10px", width: "100vw - 220px", textAlign: "center", overflow: "scroll", fontFamily:'"Sofia Pro-Light", Helvetica, Arial, serif'}}>
                    <h1><input value={this.state.titolo} placeholder="titolo" onChange={e => this.setState({ titolo: e.target.value })} style={{ textAlign: "center" , fontWeight:"bold"}}></input></h1>
                    <p><textarea value={this.state.testo} placeholder="testo" style={{ maxWidth: "800px" }} onChange={e => this.setState({ testo: e.target.value })} cols="1000" rows="10"></textarea></p>
                    <div style={{ width: "40%", padding: "10px", margin: "0 auto" }}>
                        <button className="btn-outline-light btn gradient-btn" style={{ width: "45%", float: "left" }} onClick={this.salva}>SALVA</button>
                        <Rimuovi index={this.props.index} />
                    </div>

                </div>
            </div>
        )
    }
}

export default Date;