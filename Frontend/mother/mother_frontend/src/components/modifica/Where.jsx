import React, { Component } from "react"
import "../../styles/style.css"
import NavbarAdmin from "../NavbarAdmin"
import Rimuovi from "./Rimuovi"
import { Link } from "react-router-dom"
import { ReactBingmaps } from 'react-bingmaps';
import Salva from "./Salva"


class Where extends Component {
    constructor(props) {
        super(props)
        this.state = {
            titolo: this.props.titolo,
            testo: this.props.testo,
            position: this.props.posizione,
            searchInput: "",
            boundary: {
                "location":['Lecco'],
                "option":{
                  entityType: 'PopulatedPlace'
                },
                "polygonStyle" :{
                  fillColor: 'rgba(161,224,255,0.4)',
                  strokeColor: '#a495b2',
                  strokeThickness: 2
                }
              },

        }
        this.AddPushPinOnClick = this.AddPushPinOnClick.bind(this)
        this.salva = this.salva.bind(this)
    }

    style = {
        marginTop: "10px",
        width: "80%",
        maxWidth: "800px",
        textAlign: "center",
    }

    salva() {
        console.log(this.state.posizione)
        var body = {
            "titolo": this.state.titolo,
            "testo": this.state.testo,
            "posizione": this.state.position,
            "type": "Where"
        }
        Salva(body, this.props.index)
    }
    AddPushPinOnClick(location) {
        console.log(location)
        this.setState({
            position: {
                lat: location.latitude,
                lng: location.longitude
            }
        })
    }

    handleSubmit(event) {
        if (this.state.searchInput !== null && this.state.searchInput !== "") {
            this.setState({
                boundary: {
                    "search": this.state.searchInput,
                    "polygonStyle": {
                        fillColor: 'rgba(161,224,255,0.4)',
                        strokeColor: '#a495b2',
                        strokeThickness: 2
                    },
                    "option": {
                        entityType: 'PopulatedPlace'
                    }
                }
            })
        }
        event.preventDefault();
    }

    render() {
        console.log()
        return (
            <div>
                <div style={{ marginLeft: "5%", height: "calc(100vh - 60px)", marginTop: "10px", width: "100vw - 220px", textAlign: "center", overflow: "scroll", fontFamily:'"Sofia Pro-Light", Helvetica, Arial, serif' }}>
                    <h1><strong>Where</strong></h1>
                    <input placeholder="Titolo" style={this.style} value={this.state.titolo} onChange={e => (this.setState({ titolo: e.target.value }))} /> <br></br>
                    <textarea placeholder="testo" onChange={e => (this.setState({ testo: e.target.value }))} cols="1000" rows="10" style={this.style} value={this.state.testo} /> <br></br>

                    <span style={{ 'display': 'inline-block' }}>
                        <form onSubmit={this.handleSubmit.bind(this)}>
                            <input type="text" placeholder="search place, pin, city"
                                onChange={(event) => { this.setState({ searchInput: event.target.value }) }}
                                value={this.state.searchInput}>
                            </input>
                            <input type="submit" value="Search" />
                        </form>
                    </span>
                    <ReactBingmaps
                        className="mappa-bing"
                        bingmapKey="AjZqm8thFeWufp6k0NsAOET6UzcISnQHH7EOjv62DZWtXGj20E3miG_nbyT8tqYG"
                        center={[this.state.position.lat, this.state.position.lng]}
                        boundary={this.state.boundary}
                        getLocation={
                            { addHandler: "click", callback: this.AddPushPinOnClick }
                        }
                        zoom={500}
                        pushPins={
                            [
                                {
                                    "location": [this.state.position.lat, this.state.position.lng], "option": { color: 'red' }, "addHandler": { "type": "click", callback: this.callBackMethod }
                                }
                            ]
                        }
                    >
                    </ReactBingmaps>

                    <div style={{ width: "40%", padding: "10px", margin: "0 auto" }}>
                        <Link to="/appsmanagement"><button className="btn-outline-light gradient-btn btn" style={{ width: "45%", float: "left" }} onClick={this.salva}>SALVA</button></Link>
                        <Rimuovi index={this.props.index} />
                    </div>
                </div>
            </div>
        )
    }
}

export default Where;