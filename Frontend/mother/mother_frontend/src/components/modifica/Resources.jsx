import React, { Component } from "react"
import "../../styles/style.css"
import NavbarAdmin from "../NavbarAdmin"
import { uploadFile } from 'react-s3';
import Cookies from "universal-cookie";
import { Link } from "react-router-dom"
import { Form , Col, Image} from "react-bootstrap";
import FileUploader from "../../utilities/FileUploader"

class Resources extends Component {

    style = {
        marginTop: "10px",
        width: "80%",
        maxWidth: "800px",
        textAlign: "center",
    }

    constructor(props) {
        super(props)
        this.state = {
            titolo: this.props.titolo,
            testo: this.props.testo,
            immagine: this.props.immagine,
            selectedBackgroundFile : "",
            selectedLogoFile : "",
            logoBlobPath : "",
            backgroundBlobPath : ""
        }
        console.log(this.props.index)
        this.salva = this.salva.bind(this)
        this.onChangeBackgroundHandler = this.onChangeBackgroundHandler.bind()
        this.onChangeLogoHandler = this.onChangeLogoHandler.bind()
        this.getLogoPath = this.getLogoPath.bind(this)
        this.getBackgroundImagePath = this.getBackgroundImagePath.bind(this)
    }

    getLogoPath(resources) {
        var logoIndex = 0;
        resources.map(function (risorsa, i) {
          if (risorsa.content.type === "logo")
            logoIndex = i
        }
        )
        console.log(resources[logoIndex].content.path)
        return resources[logoIndex].content.path
    }
    
    getBackgroundImagePath(resources) {
        var backgroundImageIndex = 0;
        resources.map(function (risorsa, i) {
          if (risorsa.content.type === "background_image")
            backgroundImageIndex = i
        })
        console.log(resources[backgroundImageIndex].content.path)
        return resources[backgroundImageIndex].content.path
      }

    async salva(){
        console.log(this.state.selectedLogoFile)
        var cookie = new Cookies()
        var fileUploader = new FileUploader("creappa", "AKIA4OXXCC6QQVU5Y7HM", "Qh9zS3gX5T3rbphrj1VQEpmlj6BevfQeuC2978sQ")
        fileUploader.uploadLogoAndBackground(this.state.selectedLogoFile, this.state.selectedBackgroundFile, cookie.get('app').subdomain)
            .then(async (locations) => {
                console.log(locations)
                window.location.href="/admin"
            })
            .catch(
                (error) =>  alert(error)
            )
        
        const axios = require('axios').default
        const resp = await axios.post(
            'https://api.creappa.com/editComponent',
            {
                "appIndex": cookie.get("app").index,
                "componentIndex" : this.props.index,
                "newContent":{
                    "type":"About",
                    "titolo": this.state.titolo,
                    "testo":this.state.testo,
                    "immagine":this.state.immagine
                },
                "email": cookie.get("account").email,
                "password": sessionStorage.getItem("pwd"),
            },
            { headers: { 'Content-Type': 'application/json' } }
        )

        switch(resp.data.status){
            
        }

        alert("Componente salvato correttamente")
        
    }

    onChangeLogoHandler=event=>{
        console.log(event.target.files[0])
        var url = URL.createObjectURL(event.target.files[0])
        this.setState({
            selectedLogoFile: event.target.files[0],
            loaded: false,
            logoBlobPath: url
        })
    }
      
    onChangeBackgroundHandler=event=>{
        console.log(event.target.files[0])
        var url = URL.createObjectURL(event.target.files[0])
        this.setState({
            selectedBackgroundFile: event.target.files[0],
            loaded: false,
            backgroundBlobPath: url
        })
    }

    async componentDidMount(){
        const axios = require('axios').default
        const cookie = new Cookies();
        var bodyApp = {
            "appIndex": cookie.get("app").index,
            "subdomain": cookie.get("app").subdomain
        }
        var resp = await axios.post("https://api.creappa.com/getConfigs", bodyApp)
        console.log(resp.data.resources)
        this.setState({
            logoBlobPath : this.getLogoPath(resp.data.resources),
            backgroundBlobPath : this.getBackgroundImagePath(resp.data.resources)
        })
    }

    render() {
        return (
            <div>
                <NavbarAdmin />
                <div style={{ height: "calc(100vh - 60px)", marginTop: "10px", width: "100vw - 220px", textAlign: "center", overflow: "scroll" }}>
                    <h1><strong>Risorse</strong></h1>
                    <Form className="change-logo-background">
                        <Col>
                            <Form.Row>
                                <Col>
                                    <Form.Label><strong>Logo</strong></Form.Label>
                                    <Form.Control type="file" placeholder="Enter your new logo..." onChange={this.onChangeLogoHandler}/>
                                </Col>
                                <Col>
                                    <img className="logo-img" src={this.state.logoBlobPath} alt="_" style={{width:"30%"}}></img>
                                </Col>
                            </Form.Row>
                            <br/>
                            <Form.Row>
                                <Col>
                                    <Form.Label><strong>Background</strong></Form.Label>
                                    <Form.Control type="file" placeholder="Enter your new logo..." onChange={this.onChangeBackgroundHandler}/>
                                </Col>
                                <Col>
                                    <img className="background-img" src={this.state.backgroundBlobPath} alt="_" style={{width:"30%"}}></img>
                                </Col>
                            </Form.Row>
                        </Col>
                    </Form>
                    <br></br>
                    <div style={{ width: "40%", padding: "10px", margin: "0 auto" }}>
                        <button className="btn-primary btn" style={{ width: "45%"}} onClick={this.salva}>SALVA</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default Resources;