import React, { Component } from "react";
import NavbarAdmin from "../NavbarAdmin"
import { Carousel } from "react-bootstrap"
import Rimuovi from "./Rimuovi"
import Salva from "./Salva"
import FileUploader from "../../utilities/FileUploader"
import Cookies from "universal-cookie"
import { CircularProgress } from "@material-ui/core"


class Galleria extends Component {
    constructor(props) {
        super(props)
        this.state = {
            titolo: this.props.titolo,
            galleria: this.props.galleria,
            loading: -1
        }
        this.modificaTestoGalleria = this.modificaTestoGalleria.bind(this)
        this.modificaImmagine = this.modificaImmagine.bind(this)
        this.salva = this.salva.bind(this)
        this.aggiungiSlide = this.aggiungiSlide.bind(this)
    }

    aggiungiSlide() {
        var joined = this.state.galleria.concat(
            {
                "descrizione": "descrizione",
                "immagine": "https://www.auroraseriate.it/wp-content/themes/auroraseriate/assets/images/common/400x300.gif",
            }
        )
        this.setState({ galleria: joined })
    }

    salva() {
        var body = {
            "titolo": this.state.titolo,
            "galleria": this.state.galleria,
            "type": "Galleria"
        }
        console.log(this.state.galleria)
        Salva(body, this.props.index)
    }

    modificaTestoGalleria(testo, i) {
        var joined = this.state.galleria
        joined[i].descrizione = testo
        this.setState({ galleria: joined })
    }

    modificaImmagine(immagine, i, nomeImmagine) {
        this.setState({ loading: i })
        console.log(immagine)
        var cookie = new Cookies()
        var fileUploader = new FileUploader("creappa", "AKIA4OXXCC6QQVU5Y7HM", "Qh9zS3gX5T3rbphrj1VQEpmlj6BevfQeuC2978sQ")
        console.log(immagine)

        fileUploader.uploadFilePromise("resources/" + cookie.get('app').subdomain + "/" + nomeImmagine, immagine)
            .then(async (location) => {
                alert("Immagine caricata correttamente")
                var joined = this.state.galleria
                joined[i].immagine = location
                this.setState({ galleria: joined })
                this.setState({ loading: -1 })
            })
    }

    render() {
        return (
            <div>
                <div style={{ marginLeft: "5%", height: "calc(100vh - 60px)", marginTop: "10px", width: "100vw - 220px", textAlign: "center", overflow: "scroll", fontFamily:'"Sofia Pro-Light", Helvetica, Arial, serif' }}>
                    <h1>Galleria</h1>
                    <hr />
                    <h3>Anteprima</h3>
                    <Carousel style={{ maxWidth: "400px", margin: "0 auto" }}>
                        {this.state.galleria.map((slide, i) =>
                            <Carousel.Item>
                                <img
                                    className="d-block w-100"
                                    src={slide.immagine}
                                    alt={i + 1 + "slide"}
                                />
                                <Carousel.Caption>
                                    <p>{slide.descrizione}</p>
                                </Carousel.Caption>
                            </Carousel.Item>
                        )}
                    </Carousel>
                    <hr />
                    <h3>Modifica</h3>
                    <div style={{ marginBottom: "20px" }}><button className="btn btn-outline-light gradient-btn" onClick={this.aggiungiSlide}>+</button> <span>Aggiungi foto</span></div>
                    {this.state.galleria.map((slide, i) => (
                        i == this.state.loading ? <div className="loading"> <br /><CircularProgress className="crc-prg" style={{ marginBottom: "20px" }} color='primary'></CircularProgress><br /></div> : (
                            <div>
                                <img src={slide.immagine} style={{ maxWidth: "400px", height: "auto" }}></img>
                                <div className="input-group" style={{ maxWidth: "400px", margin: "0 auto" }}>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" onChange={e => this.modificaImmagine(e.target.files[0], i, e.target.files[0].name)} id="inputGroupFile02" />
                                            <label class="custom-file-label" for="inputGroupFile02">Scegli immagine</label>
                                        </div>
                                    </div>
                                </div>
                                <input className={"form-control"} style={{ maxWidth: "400px", margin: "0 auto", fontWeight: "550" }} value={slide.descrizione} onChange={e => this.modificaTestoGalleria(e.target.value, i)}></input>
                                <br></br>
                                <br></br>
                            </div>)
                        ))}
                    <div style={{ width: "40%", padding: "10px", margin: "0 auto" }}>
                        <button className="btn-outline-light btn gradient-btn" style={{ width: "45%", float: "left" }} onClick={this.salva}>SALVA</button>
                        <Rimuovi index={this.props.index} />
                    </div>
                </div>
            </div>
        )
    }
}

export default Galleria;