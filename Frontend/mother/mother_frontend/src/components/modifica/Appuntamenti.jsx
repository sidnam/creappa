import React, { Component, useState } from "react"
import NavbarAdmin from "../components/NavbarAdmin"
import Cookies from 'universal-cookie'
import Add from '@material-ui/icons/Add'
import Popup from "reactjs-popup";
import DatePicker from "react-datepicker";

import { Button,Modal,Table, Row, Col, Form, Alert } from "react-bootstrap";
import { CircularProgress } from '@material-ui/core';
import ConfirmDateButton from "../components/ConfirmDateButton"
import "react-datepicker/dist/react-datepicker.css";
import 'bootstrap/dist/css/bootstrap.css'


class Appuntamenti extends Component {

    constructor(props){
        super(props)
        this.state = {
            dates : [],
            loadingDates : false,
            showCreateDateModal: false,
            targetEmail: "",
            formError: false,
            dateDatetime: new Date(),
            dateDescription: ""
        }
        this.onDateCreateClick = this.onDateCreateClick.bind(this)
        this.onDateCreateClose = this.onDateCreateClose.bind(this)
        this.onDateCreateConfirm = this.onDateCreateConfirm.bind(this)
        this.targetEmailChangeHandler = this.targetEmailChangeHandler.bind(this)

    }

    async componentDidMount() {
        const axios = require('axios').default;
        const cookie = new Cookies();
        var tb = document.getElementById("tableBody");
        
        var bodyjson = {
            email: cookie.get("account").email,
            password: sessionStorage.getItem("pwd"),
            appIndex: cookie.get("app").index
        }
        this.setState({loadingDates: true})
        const resp = await axios.post('https://api.creappa.com/getDatesPerAdmin',bodyjson);
        this.setState({dates: resp.data.dates, loading: false})
    }


    dateStatusFromNumberToText(number){
        switch(number){
            case 1:
                return <h6>Yes</h6>
            case 0:
                return <h6>Pending</h6>
            case -1:
                return <h6>No</h6>
        }
    }

    validateEmail(mail) 
    {
        if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(mail))
        {
            return (true)
        }
            return (false)
    }

    onDateCreateClick = event => {
        console.log(event.target)
        this.setState({showCreateDateModal : true})
    }

    onDateCreateConfirm = async (event) => {
        console.log(event)
        if(/* ! this.validateEmail(this.state.targetEmail) || */ this.state.dateDescription.length < 12 || this.state.dateDatetime < new Date())
            this.setState({formError: true})
        else{
            const axios = require('axios').default
            var cookies = new Cookies()
            const resp = await axios.post(
                'https://api.creappa.com/askForDateAdminSide',
                {
                    "targetEmail" : this.state.targetEmail,
                    "email" : cookies.get('account').email,
                    "password" : cookies.get('account').pswd,
                    "appIndex" : cookies.get("app").index,
                    "dateDescription" : this.state.dateDescription,
                    "date" : {
                        "day" : this.state.dateDatetime.getDay,
                        "month" : this.state.dateDatetime.getMonth,
                        "year" : this.state.dateDatetime.getFullYear,
                        "hour" : this.state.dateDatetime.getHours,
                        "minute" : this.state.dateDatetime.getMinutes
                    }
                },
                { headers: { 'Content-Type': 'application/json' } }
            )
            console.log(resp.data)
            if(resp.data.status === "successful")
                alert('Appuntamento richiesto!')
            else
                
                alert('Qualcosa è andato storto')
        }
    }

    onDateCreateClose = event => {
        this.setState({showCreateDateModal: false})
    }

    targetEmailChangeHandler = event => {
        console.log(event.target.value)
        this.setState({targetEmail: event.target.value})
    }

    datePickerChangeHanlder = date => {
        console.log(date)
        this.setState({dateDatetime: date})
    }

    dateDescriptionChangeHandler = event => {
        console.log(event.target.value)
        this.setState({dateDescription: event.target.value})
    }


    render() {
        const ExampleCustomInput = ({ value, onClick }) => (
            <Button onClick={onClick}>
              {value}
            </Button>)

        return (
            <div>
                <NavbarAdmin />
                <div style={{ marginLeft: "5%", height: "calc(100vh - 60px)", marginTop: "10px", width: "100vw - 220px", textAlign : "center", overflow : "scroll"}}>
                    <h1><strong>Appuntamenti</strong></h1>
                    <br/>
                    <Table >
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Indice</th>
                                <th scope="col">Richiesto il</th>
                                <th scope="col">Data</th>
                                <th scope="col" >Descrizione</th>
                                <th scope="col">Approvata</th>
                                <th scope="col">Approvata il</th>
                                <th scope="col">Richiedente</th>
                            </tr>
                            </thead>
                        <tbody>
                            {
                                (!this.state.loading) ?
                                this.state.dates.map((appuntamento, index) => {
                                    return(<tr>
                                        <td>
                                            <ConfirmDateButton dateIndex={appuntamento.index} approvationDatetime={appuntamento.approvationDate} disabledTextButton="Confermato" enabledTextButton="Conferma"/>
                                        </td>
                                        <td>{index+1}</td>
                                        <td>{appuntamento.requestDate}</td>
                                        <td>{appuntamento.date}</td>
                                        <td style={{maxWidth:"500px", overflow:"hidden"}}>{appuntamento.description}</td>
                                        <td>{this.dateStatusFromNumberToText(appuntamento.approved)}</td>
                                        <td>{appuntamento.approvationDate}</td>
                                        <td>{appuntamento.user.name}</td>
                                    </tr>)
                                }) : <CircularProgress></CircularProgress>
                            }
                        </tbody>
                    </Table>
                    <Popup trigger={
                        <Button className="btn-circle" onClick={this.onDateCreateClick} style={{position:"absolute", right:"5%", bottom:"5%"}}>
                            <Add>
                            </Add>
                        </Button>
                    } on={['hover', 'focus']} position='top center'
                    >
                        <span>Invia una richiesta di appuntamento tramite mail</span>
                    </Popup>

                    
                    <Modal show={this.state.showCreateDateModal} onHide={this.onDateCreateClose} position="center" size="lg" centered>
                        <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-vcenter"><strong>Crea un appuntamento</strong></Modal.Title>
                        </Modal.Header>
                        <Form id="create-date-form">
                            <Modal.Body>
                            {
                                this.state.formError ?
                                <Alert variant="danger" onClose={() => this.setState({formError: false})} dismissible >
                                    <Alert.Heading>Qualcosa è andato storto! Completa tutti i campi</Alert.Heading>
                                    <p>
                                        Completa tutti i campi del Form, altrimenti non possiamo inoltrare la tua richiesta! 😢
                                    </p>
                                </Alert> : <div></div>

                            }
                                <Form.Group>
                                    <Row>
                                        <Col>
                                            <Form.Label for="target-email"><strong>Email Utente:</strong></Form.Label>
                                            <Form.Control className="target-email" type="email" placeholder="Email a cui inviare l'appuntamento"  onChange={this.targetEmailChangeHandler}></Form.Control>
                                        </Col>
                                        <Col>
                                            <Row>
                                                <Form.Label for="date-picker"><strong>Data appuntamento:</strong></Form.Label>
                                            </Row>
                                            <Row>
                                                <DatePicker
                                                    showTimeSelect
                                                    selected={this.state.dateDatetime}
                                                    onChange={this.datePickerChangeHanlder}
                                                    customInput={<ExampleCustomInput/>}
                                                />
                                            </Row>
                                        </Col>
                                    </Row>
                                </Form.Group>
                                <br/>
                                <Form.Group className="align-items-center">
                                    
                                    <Col>
                                        <Row>
                                            <Form.Label for="description-box"><strong>Descrizione</strong></Form.Label>
                                            <Form.Control className="description-box" as="textarea" rows={3} placeholder="Descrizione dell'appuntamento" onChange={this.dateDescriptionChangeHandler}/>
                                        </Row>
                                    </Col>
                                </Form.Group>
                            </Modal.Body>
                            <Modal.Footer>
                            <Button id="create-date" variant="secondary" onClick={this.onDateCreateClose}>
                                Chiudi
                            </Button>
                            <Button onClick={this.onDateCreateConfirm}>
                                Invia Richiesta 
                            </Button>
                            </Modal.Footer>
                        </Form>
                    </Modal>
                </div>
            </div>
        )
    }
}

export default Appuntamenti;