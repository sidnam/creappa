import React, { Component } from "react"
import './index.css';
import HomePage from "./routes/HomePage"
import Mission from "./routes/Mission"
import Howto from "./routes/Howto"
import Pricing from "./routes/Pricing"
import Login from "./routes/NewLogin"
import SignUp from "./routes/SignUp"
//import MyApps from "./routes/AppManagement"
import NewApp from "./routes/NewApp"
import "./images/old_img/blue.png"
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import AdminPage from './routes/AdminPage'
import Modifica from "./components/Modifica"
import Visualizza from "./components/Visualizza"
import Cookies from 'universal-cookie'
import Where from "./components/modifica/Where"
import About from "./components/modifica/About"
import Contacts from "./components/modifica/Contacts"
import Schedule from "./components/modifica/Schedule"
import Shop from "./components/modifica/Shop"
import Date from "./components/modifica/Date"
import WebPage from "./components/modifica/WebPage"
import Resources from "./components/modifica/Resources"
import PaymentPage from "./routes/PaymentPage"
import AccountSettings from "./routes/AccountSettings"
import Galleria from "./components/modifica/Galleria"
//import NewUploadLogo from "./routes/NewUploadLogo";
import Newnavbar from "./components/Newnavbar"
import GoodPay from "./routes/GoodPay"
import BadPay from "./routes/BadPay"
import BrokenHeart from "./routes/BrokenHeart"
import ContactUs from "./routes/ContactUs"
import Pwdrecover from "./routes/Pwdrecover"
import AppsManagement from "./routes/AppManagement"
import Register from "./routes/newRegister"
import ShareyaApp from "./components/ShareyaApp";

class App extends Component {
    constructor(props) {
        super(props)
        this.state = {
            compModifica: ""
        }
        this.salva = this.salva.bind(this)
        this.login = this.login.bind(this)
        this.controllo = this.controllo.bind(this)
    }

    componentDidMount() {
        var cookie = new Cookies()
        try {
            if (this.login(cookie.get("account").email, sessionStorage.getItem("pwd"))) {
                console.log("loggato")
                this.salva(cookie.get("app").index, cookie.get("app").subdomain)
            }
            else {
                console.log("non loggato")
            }
        } catch (error) {
            console.log(error)
        }
    }

    async login(email, password){
        const axios=require('axios').default;
        var un=email;
        var pwd=password;
        const resp = await axios.post(
            'https://api.creappa.com/login',
            { email: un, password: pwd },
            { headers: { 'Content-Type': 'application/json' } }
        )
        if(resp.data.status === "successful"){
            return true
        } else {
            return false
            alert("Login Errato: "+resp.data.status)
        }
    }
    async salva(index, subdomain) {
        var cookie = new Cookies()
        const axios = require('axios').default;
        var bodyApp = {
            "appIndex": index,
            "subdomain": subdomain
        }
        const res = await axios.post(
            'https://api.creappa.com/getConfigs',
            bodyApp,
            { headers: { 'Content-Type': 'application/json' } }
        )

        this.setState({
            compModifica: res.data.components.map(function (componente, i) {
                switch (componente.content.type) {
                    case "Where":
                        return <Route exact path={"/appsmanagement/modifica/Where" + this.controllo(res.data.components, componente, i)} render={() => <Where posizione={componente.content.posizione} titolo={componente.content.titolo} testo={componente.content.testo} position={componente.content.posizione} index={componente.index}/>}></Route>
                    case "About":
                        return <Route exact path={"/appsmanagement/modifica/About" + this.controllo(res.data.components, componente, i)} render={() => <About titolo={componente.content.titolo} testo={componente.content.testo} immagine={componente.content.immagine} index={componente.index} />}></Route>
                    case "Contacts":
                        return <Route exact path={"/appsmanagement/modifica/Contacts" + this.controllo(res.data.components, componente, i)} render={() => <Contacts index={componente.index} contatti={componente.content.contatti} titolo={componente.content.titolo}/>}></Route>
                    case "Schedule":
                        return <Route exact path={"/appsmanagement/modifica/Schedule" + this.controllo(res.data.components, componente, i)} render={() => <Schedule index={componente.index} orari={componente.content.orari} titolo={componente.content.titolo}/>}></Route>
                    case "Shop":
                        return <Route exact path={"/appsmanagement/modifica/Shop" + this.controllo(res.data.components, componente, i)} render={() => <Shop titolo={componente.content.titolo} testo={componente.content.testo} prodotti={componente.content.prodotti} index={componente.index}/>}></Route>
                    case "DataViewer":
                    //return <Route exact path={"/admin/modifica/DataViewer" + this.controllo(res.data.components, componente, i)} render={() => <DataViwer />}></Route>
                    case "Date":
                        console.log(componente.content)
                        return <Route exact path={"/appsmanagement/modifica/Date" + this.controllo(res.data.components, componente, i)} render={() => <Date titolo={componente.content.titolo} testo={componente.content.testo} index={componente.index}/>}></Route>
                    case "WebPage":
                        return <Route exact path={"/appsmanagement/modifica/WebPage" + this.controllo(res.data.components, componente, i)} render={() => <WebPage titolo={componente.content.titolo} testo={componente.content.testo} sito={componente.content.sito} index={componente.index}/>}></Route>
                    case "Galleria":
                        return <Route exact path={"/appsmanagement/modifica/Galleria" + this.controllo(res.data.components, componente, i)} render={() => <Galleria titolo={componente.content.titolo} galleria={componente.content.galleria} index={componente.index}/>}></Route>
                    }
            }, this)
        })
    }

    controllo(componenti, componente, indice) {
        var somma = 0
        for (var i = 0; i < indice; i++) {
            if (componenti[i].content.type === componente.content.type)
                somma += 1
        }
        return somma
    }
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path="/Mission" component={Mission} />
                    <Route exact path="/nv" component={Newnavbar} />
                    <Route exact path="/Howto" component={Howto} />
                    <Route exact path="/Pricing" component={Pricing} />
                    <Route exact path="/Login" component={Login} />
                    <Route exact path="/signup" component={Register} />
                    <Route exact path="/appsmanagement" component={AppsManagement} />
                    <Route exact path="/newapp" component={NewApp} />
                    <Route exact path="/admin" component={AdminPage} />
                    <Route exact path="/payment" component={PaymentPage} />
                    <Route exact path="/admin/modifica" component={Modifica}></Route>
                    {this.state.compModifica}
                    <Route exact path="/admin/visualizza" component={Visualizza}></Route>
                    <Route exact path="/" component={HomePage} />
                    <Route exact path="/admin/risorse" component={Resources}></Route>
                    <Route exact path="/accsetting" component={AccountSettings}></Route>
                    {/*<Route exact path="/newuploadlogo" component={NewUploadLogo}></Route>*/}
                    <Route exact path="/goodpay" component={GoodPay}></Route>
                    <Route exact path="/badpay" component={BadPay}></Route>
                    <Route exact path="/brokenheart" component={BrokenHeart}></Route>
                    <Route exact path="/contactus" component={ContactUs}></Route>
                    <Route exact path="/passwordrec" component={Pwdrecover}></Route>
                    <Route exact path="/test" component={Where}></Route>
                </Switch>
            </BrowserRouter>
        )
    }
}

export default App;