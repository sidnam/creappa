

class FileUploader {
    
    constructor(bucketName, accessKeyId, secretAccessKey){

        this.state = {
            BUCKET_NAME : bucketName,
            IAM_ACCESS_KEY : accessKeyId,
            IAM_SECRET_KEY : secretAccessKey
        }
    }

    uploadLogoAndBackground(logo = "",background = "",domain){
        return new Promise((resolve, reject) => {
            var locations={
                logo: "",
                background: ""
            }
            if(logo !== ""){
                this.uploadFilePromise("resources/" + domain + "/logo.png", logo)
                .then((fileLocation) => {
                    console.log(fileLocation)
                    locations.logo = fileLocation

                    if(locations.background !== "" && locations.logo !== "")
                        resolve(locations)
                })
                .catch((error) => {
                    console.log(error)
                    reject(error)
                })
            }
            else{
                locations.logo = "https://creappa.s3.amazonaws.com/resources/" + domain + "/logo.png"
            }
            
            if(background !== ""){
                this.uploadFilePromise("resources/" + domain + "/background.png", background)
                .then((fileLocation) => {
                    console.log(fileLocation)
                    locations.background = fileLocation
                    
                    if(locations.background !== "" && locations.logo !== "")
                        resolve(locations)
                })
                .catch((error) => {
                    console.log(error)
                    reject(error)
                })
            }
            else{
                locations.background = "https://creappa.s3.amazonaws.com/resources/" + domain + "/background.png"
            }

            if(locations.background !== "" && locations.logo !== "")
            resolve(locations)

        })
    }

    uploadFilePromise(filename, file){
        return new Promise((resolve, reject) => {
            let AWS = require('aws-sdk')

            let s3 = new AWS.S3({
                accessKeyId: this.state.IAM_ACCESS_KEY,
                secretAccessKey: this.state.IAM_SECRET_KEY,
                Bucket: this.state.BUCKET_NAME
            })

            console.log(this.state.BUCKET_NAME)
        
            s3.upload(
                {
                    Bucket : this.state.BUCKET_NAME,
                    Key : filename,
                    Body : file,
                    ACL : "public-read"
                },
                function(err, data) {
                    if(err){
                        reject(err)
                    }
                        resolve(data.Location)
                }
            )
        })
    }
}

export default FileUploader