import React, {Component} from "react"
import { Link } from "react-router-dom"
import {Row, Button, Navbar, Col, Nav, Modal} from "react-bootstrap"
import "../styles/appsmanagement.css"
//import NavbarAdmin from "../components/NavbarAdmin";
import ManageSection from "../components/ManageSection"
import ShareYourApp from "../components/ShareyaApp"
import BrandingIcon from "../images/Images/manage-sections-svgs/Branding.svg"
import ModuliIcon from "../images/Images/manage-sections-svgs/Moduli.svg"
import SchermateIcon from "../images/Images/manage-sections-svgs/Schermate.svg"
import PrenotazioniIcon from "../images/Images/manage-sections-svgs/Prenotazioni.svg"
import ImpostazioniIcon from "../images/Images/manage-sections-svgs/Impostazioni.svg"
import GuidaIcon from "../images/Images/manage-sections-svgs/Guida.svg"
import phoneIllustration from "../images/Illustrations/phone-illustration.svg" 
import ShareLogo from "../images/Images/share-icon.svg"
import logo from '../images/Logo/Creappa logo.svg'
import MyApps from "./MyApps"
import FileUploader from "../utilities/FileUploader"
import Branding from "../components/manage-sections/Branding"
import Impostazioni from "../components/manage-sections/Impostazioni"
import Prenotazioni from "../components/manage-sections/Prenotazioni"
import Moduli from "../components/manage-sections/Moduli"
import NavBarAdmin from "../components/NavbarAdmin"
import Cookies from "universal-cookie"

class AppsManagement extends Component{

    /************************************************************************* */
    constructor(props){
        super(props)
        this.state = {
            manageSectionIndex: null,
            appUrl : "",
            showShareModal : false,
            showAppsModal : false,
            modulo: "",
            logo: "",
            sfondo: "",
            sfondoImg : "",
            appConfigs: "",
            appChoosen : false
        }
        this.chooseManageSection = this.chooseManageSection.bind(this)
        this.handleCloseShare = this.handleCloseShare.bind(this)
        this.handleShowShare = this.handleShowShare.bind(this)
        this.handleShowApps = this.handleShowApps.bind(this)
        this.handleCloseApps = this.handleCloseApps.bind(this)
        this.choosenApp = this.choosenApp.bind(this)

    }

    async componentDidMount(){

        if(!sessionStorage.getItem('pwd'))
            window.location.pathname = '/login'
        var cookie = new Cookies()
            var axios = require('axios').default
            
        try{
            var subdomain = cookie.get("app").subdomain
            if(this.state.appConfigs == ""){
                await this.choosenApp()
            }
            else{ 
                this.setState({appChoosen: true})
            }
                
        } catch (TypeError) {
            this.setState({appChoosen: false})
        }
    }

    async choosenApp(){
        
        try{
            var cookie = new Cookies()
            var axios = require('axios').default
            var subdomain = cookie.get("app").subdomain
            var bodyApp = {
                "appIndex": cookie.get("app").index,
                "subdomain": subdomain,
            }
            var res = await axios.post(
                'https://api.creappa.com/getConfigs',
                bodyApp,
                { headers: { 'Content-Type': 'application/json' } }
            )
            console.log("dati restituiti: ")
            console.log(res.data)
            this.setState({
                appConfigs : res.data,
                appUrl : "https://" + subdomain + ".creappa.com/",
                appChoosen: true
            })
         
        }catch(TypeError) {
            console.log('errore nel fecthing dei dati')
        }
    }

    //************************************************************************ */
    chooseManageSection(){

        switch(this.state.manageSectionIndex){
            case 0:
                this.setState({modulo: 
                    <Branding principale={this.state.appConfigs.colors.principale} secondario={this.state.appConfigs.colors.secondario} sfondo={this.state.appConfigs.colors.sfondo}/>
                })
                break 
            case 1:
                this.setState({modulo: 
                    <Moduli components={this.state.appConfigs.components}></Moduli>
                })
                break
            case 2:
                this.setState({modulo: <h1>Schermata</h1> })
                break
            case 3:
                this.setState({modulo: <Prenotazioni/> })
                break
            case 4:
                this.setState({modulo: <Impostazioni/> })
                break
            case 5:
                this.setState({modulo: <h1>Statistiche</h1>})
                break
            case 6:
                this.setState({modulo: <h1>Guida</h1>})
                break
        }
    }

    /************************************************************************* */
    onChangeSectionHandler(index){
        console.log(this.state.appChoosen)
        if(this.state.appChoosen == true){
            this.state.manageSectionIndex=index
            this.chooseManageSection();

        } else {
            this.handleShowApps()
        }
    }

    //*************************************************** */
    handleCloseShare(){
        this.setState({showShareModal:false})
    }

    //*************************************************** */
    handleShowShare(){
        this.setState({showShareModal:true})
    }

    //*************************************************** */
    handleShowApps(){
        this.setState({showAppsModal:true})
    }

    //*************************************************** */
    handleCloseApps(){
        this.setState({showAppsModal:false})
    }

    //*************************************************** */
    render(){
        return(
            <div style={{height:"100%"}}>
                <NavBarAdmin showApps={this.handleShowApps} share={this.handleShowShare}/>
                <Modal show={this.state.showShareModal} onHide={this.handleCloseShare} size="lg">
                    <Modal.Body>
                        <ShareYourApp url={this.state.appUrl}/>
                    </Modal.Body>
                </Modal>
                <Modal show={this.state.showAppsModal} onHide={this.handleCloseApps} dialogClassName="gestisci-app-modal" size="lg">
                    <Modal.Body>
                        <MyApps handleCloseApps={this.handleCloseApps} handleChooseApp={this.choosenApp}></MyApps>
                    </Modal.Body>
                </Modal>
                <Row className="top-nv-shadow"/>
                <Col fluid className="left-nav">
                    <br/>
                    <div onClick={()=>this.onChangeSectionHandler(0)}><ManageSection title="Branding" image={BrandingIcon} ></ManageSection></div>
                    <div onClick={()=>this.onChangeSectionHandler(1)}><ManageSection title="Moduli" image={ModuliIcon}></ManageSection></div>
                    {/*<div onClick={()=>this.onChangeSectionHandler(2)}><ManageSection title="Schermate" image={SchermateIcon}></ManageSection></div>*/}
                    <div onClick={()=>this.onChangeSectionHandler(3)}><ManageSection title="Prenotazioni" image={PrenotazioniIcon}></ManageSection></div>
                    <div onClick={()=>this.onChangeSectionHandler(4)}><ManageSection title="Impostazioni" image={ImpostazioniIcon}></ManageSection></div>
                    {/*<ManageSection title="Statistiche"></ManageSection>*/}
                    {/*<div onClick={()=>this.onChangeSectionHandler(6)}><ManageSection title="Guida" image={GuidaIcon}></ManageSection></div>*/}
                </Col>
                <Col fluid className="manage-content">
                    {this.state.modulo}
                </Col>
                <div className="preview">
                    <img src={phoneIllustration}></img>
                </div>
            </div>
        )
    }
}

export default AppsManagement