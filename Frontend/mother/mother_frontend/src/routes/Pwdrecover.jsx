import React, {Component} from "react";
import logo from "../images/Logo/Creappa logo.svg"
import "../styles/nextpay.css"
import {Container, Row, Form, Button, Card} from "react-bootstrap"

class Login extends Component {

  constructor() {
    super();
    this.mailInput = React.createRef();
  }

  async sendMail(){
      const axios=require('axios').default;
      var un=this.mailInput.current.value;
      const resp = await axios.post(
          'https://api.creappa.com/recoverPassword',
          { email: un },
          { headers: { 'Content-Type': 'application/json' } }
      )
      if(resp.data.status === "successful"){
        alert("Ti abbiamo inviato una mail con la tua password")
        window.location.pathname="/";
      } else {
        alert("C'è stato un problema.")
      }
  }

  render() {
    return (
        <div >
            <div className="immagine-sfondo-na">
                <img className="logo psw-logo" src={logo} alt="logo" to="/"/>
                <Container fluid>
                    <Row className="justify-content-sm-center">
                        <Card className="creation-card">
                            <h3 className="header">Inserisci la tua mail</h3>
                            <Form id="creatore" className="app-form">
                                <Form.Group className="app-form-element">
                                    <Form.Control className="email-input" type="email" placeholder="Inserisci email..." ref={this.mailInput}></Form.Control>
                                </Form.Group>
                            </Form>
                            <Form.Row>
                              <Button className="gradient-btn  accedi-btn" variant="light" onClick={this.sendMail.bind(this)}>
                                 RECUPERA LA PASSWORD
                              </Button>
                            </Form.Row>  
                        </Card>
                    </Row>
                </Container>
            </div>
        </div>
    )
  }
}

export default Login;
