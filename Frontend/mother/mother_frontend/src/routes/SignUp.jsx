import React, { Component } from "react"
import NavBar from "../components/Navbar";
import "../styles/login.css"
import Cookies from 'universal-cookie';
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";

class SignUp extends Component{
    constructor(){
        super();
        this.nameInput = React.createRef();
        this.bnameInput = React.createRef();
        this.pivaInput = React.createRef();
        this.emailInput = React.createRef();
        this.passInput = React.createRef();
        this.addInput = React.createRef();
        this.ncivInput = React.createRef();
        this.surnameInput =React.createRef();
        this.bdInput=React.createRef();
        this.cellInput=React.createRef();
        this.cfInput=React.createRef();
    }

    async sendSignin(){
        const cookie = new Cookies();
        const axios=require('axios').default;

        var name=this.nameInput.current.value;
        var surname=this.surnameInput.current.value;
        var cf=this.cfInput.current.value;
        var birthd=new Date(this.bdInput.current.value);
        var cell=this.cellInput.current.value;
        var bname=this.bnameInput.current.value;
        var piva=this.pivaInput.current.value;
        var mail=this.emailInput.current.value;
        var pwd=this.passInput.current.value;
        var addr=this.addInput.current.value;
        var nciv=this.ncivInput.current.value;

        var bodyjson={ 
            name: name,
            surname: surname,
            email: mail, 
            password: pwd,
            bornday: {
                day: birthd.getDate(),
                month: (birthd.getMonth()+1),
                year: birthd.getFullYear()
            },
            codFis: cf,
            cellularNumber: cell
        };
        const resp = await axios.post(
            'https://api.creappa.com/registerUser',
            bodyjson,
            { headers: { 'Content-Type': 'application/json' } }
        )
        console.log('ok1')
        if(resp.data.status === "successful"){
            bodyjson={
                email: mail,
                businessName: bname,
                password: pwd,
                partitaIVA: piva,
                address: addr,
                numeroCivico: parseInt(nciv)
            }
            
            const resp2 = await axios.post(
                'https://api.creappa.com/registerBusiness',
                bodyjson,
                { headers: { 'Content-Type': 'application/json' } }
            )
            if(resp2.data.status === "successful"){
                cookie.set('account', {
                    email: mail,
                    pswd: pwd
                }, { path : "/"})
                window.location.pathname="/payment"
            } else {
                alert("Registrazione Errata: "+resp2.data.status)
            }
        } else {
            alert("Registrazione Errata: "+resp.data.status)
        }
    }

    render(){
        return(
            <div>
                <div className="immagine-sfondo-signup">
                    <NavBar />
                    <Form className="signup-container">
                        <div className="margini">
                            <div>
                                <h2><strong>Registrazione</strong></h2>
                            </div>

                            <Form.Row className="form-signup">
                                <Col>
                                    <Form.Label>Nome</Form.Label>
                                    <Form.Control  type="text" placeholder="Inserisci il tuo nome..." ref={this.nameInput}/>
                                </Col>
                                <Col>
                                    <Form.Label>Cognome</Form.Label>
                                    <Form.Control  type="text" placeholder="Inserisci il tuo cognome..." ref={this.surnameInput}/>
                                </Col>
                            </Form.Row>
                            <Form.Row>
                                <Col>
                                    <Form.Label>Codice Fiscale</Form.Label>
                                    <Form.Control type="text" placeholder="Inserisci il tuo codice fiscale..." ref={this.cfInput}/>
                                    <Form.Text muted>
                                        Se sei un cittadino straniero non in possesso di codice fiscale italiano, inserisci il codice: 000000000000000
                                    </Form.Text>
                                </Col>
                                <Col>
                                    <Form.Label>Data di Nascita</Form.Label>
                                    <Form.Control  type="date" ref={this.bdInput}/>
                                </Col>
                            </Form.Row>
                            <Form.Group>
                                <Form.Label>Numero di Telefono</Form.Label>
                                <Form.Control  type="tel" placeholder="Inserisci il tuo numero di telefono..." ref={this.cellInput}/>
                                <Form.Label>Nome dell'impresa</Form.Label>
                                <Form.Control  type="text" placeholder="Inserisci il nome della tua impresa..." ref={this.bnameInput}/>
                                <Form.Label>Partita IVA</Form.Label>
                                <Form.Control type="number" placeholder="Inserisci la partita IVA della tua impresa..." ref={this.pivaInput}/>
                            </Form.Group>
                            <Form.Row>
                                <Col>
                                    <Form.Label>Email</Form.Label>
                                    <Form.Control  type="email" placeholder="Inserisci la tua email..." ref={this.emailInput}/>
                                </Col>
                                <Col>
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control  type="password" placeholder="Inserisci la tua nuova password..." ref={this.passInput}/>
                                </Col>
                            </Form.Row>
                            <Form.Row>
                                <Col>
                                    <Form.Label>Indirizzo</Form.Label>
                                    <Form.Control type="text" placeholder="Inserisci l'indirizzo della sede della tua impresa..." ref={this.addInput}/>
                                </Col>
                                <Col>
                                    <Form.Label>Numero Civico</Form.Label>
                                    <Form.Control  type="number" placeholder="inserisci il numero civico della sede della tua impresa..." ref={this.ncivInput}/>
                                </Col>
                            </Form.Row>
                            <br/>
                            <p className="account-message">
                                Hai già un account? 
                                <a href="/login"  className="account-button">
                                    Login!
                                </a>
                            </p>
                            <div className="submit-button-div">
                                <button className="submit-button submit-button-signup center" style={{backgroundColor: "#74d691"}} onClick={this.sendSignin.bind(this)}>Submit</button>
                            </div>
                        </div>
                    </Form>
                </div>
            </div>
        )
    }
}
export default SignUp