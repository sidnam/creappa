import React, { Component } from "react"
import "../styles/pricing.css"
import Newnavbar from "../components/Newnavbar";
import {Card, CardDeck, Button, Row, Container, Col} from "react-bootstrap"
import basicImg from "../images/Svg_/rocket.svg"
import proImg from "../images/Svg_/diamond.svg"
import {Link} from "react-router-dom"

class Pricing extends Component {
    render() {
        return (
            <div>
                <div className="immagine-sfondo-price">
                    <Newnavbar/>
                    <Container fluid className="mission-content">
                        <Row>
                            <div className="tit">I nostri piani</div>
                        </Row>
                        <Row>
                            <div className="par">Tutti gli abbonamenti hanno durata di 1 anno, potrai cancellarti in ogni momento</div>
                        </Row>
                        <CardDeck className="justify-content-md-center" style={{marginBottom:"5%"}}>
                            <Card>
                                <Card.Body  className="plan-box">
                                    <Row className="box-prize">
                                        <Col>
                                            <img className="basic-plan" src={basicImg} alt="basicplan"/>
                                        </Col>
                                        <Col>
                                            <div className="card-tit">Basic</div>
                                            <div className="basic-price">9,90 €</div>
                                            <div className="card-cont">+IVA/anno</div>
                                        </Col>
                                    </Row>
                                    <div className="card-funclist">
                                        <ol>
                                            <li>Tutti i dispositivi supportati</li>
                                            <li>Caratteristiche illimitate</li>
                                            <li>Modifiche ad app illimitate</li>
                                            <li>Supporto tecnico (orario lavorativo)</li>
                                            <li>Creazione di 1 applicazione</li>
                                        </ol>
                                    </div>
                                    <Row className="justify-content-sm-center btn-center">
                                        <Link className="link-btn" to={"/login"}>
                                            <Button className="gradient-btn btn-plan" variant="light" block>
                                                <div className="btnTitle">SCEGLI</div>
                                            </Button>
                                        </Link>
                                    </Row>
                                    <Row className="justify-content-center">
                                        <div className="card-end">(l'offerta comprende un periodo di prova di 1 mese)</div>
                                    </Row>
                                    
                                </Card.Body>
                            </Card>
                            <Card>
                                <Card.Body className="plan-box">
                                    <Row className="box-prize">
                                        <Col>
                                            <img className="basic-plan" src={proImg} alt="proplan"/>
                                        </Col>
                                        <Col>
                                            <div className="card-tit">Business</div>
                                            <div className="pro-price">39,90 €</div>
                                            <div className="card-cont">+IVA/anno</div>
                                        </Col>
                                    </Row>
                                    <div className="card-funclist">
                                        <ol>
                                            <li>Tutti i dispositivi supportati</li>
                                            <li>Caratteristiche illimitate</li>
                                            <li>Modifiche ad app illimitate</li>
                                            <li>Supporto tecnico (orario lavorativo)</li>
                                            <li>Creazione fino a 5 applicazione</li>
                                        </ol>
                                    </div>
                                    <Row className="justify-content-sm-center btn-center">
                                        <Link className="link-btn" to={"/login"}>
                                            <Button className="gradient-btn btn-plan" variant="light" block>
                                                <div className="btnTitle">SCEGLI</div>
                                            </Button>
                                        </Link>
                                    </Row>
                                    <Row className="justify-content-center">
                                        <div className="card-end">(l'offerta non comprende un periodo di prova)</div>
                                    </Row>
                                    
                                </Card.Body>
                            </Card>
                        </CardDeck>
                    </Container>
                </div>
            </div>
        )
    }
}
export default Pricing