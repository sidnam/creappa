import React, { Component } from "react"
import Navbar2 from "../components/Navbar2"
import "../styles/myapps.css"
import 'bootstrap/dist/css/bootstrap.css'
import Cookies from 'universal-cookie'
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';
import {Container, Row, Button, Card, ButtonGroup} from 'react-bootstrap'
import {CircularProgress} from "@material-ui/core"
import qs from 'qs'

import {Link} from "react-router-dom"
import PayPalBtn from "../components/PayPalBtn"

class MyApps extends Component{

    constructor(props){
        super(props)
        this.state = {
            applications: "",
            subscription: "",
            disableBtn: false,
            hasntPay:true,
            type: "",
            planQuant: 0,
            oldPlan: ""
        }
        this.handleOnCardClick = this.handleOnCardClick.bind(this)
    }

    async activateSub(){
        const axios = require('axios').default;
        var token=await axios({
            method:"POST",
            url:'https://api.sandbox.paypal.com/v1/oauth2/token',
            auth:{
                username:'ARsQyOh4CzmSbX1dPH61I4fJ1FePhjoKrxlr890-qKletuKWRJS1HLEao8MdQeaLgebDcDITK5kSxC92',
                password:'EKuK8uW2tnHHJrBXBsryZGAuSToCSjeRkQnPfUr1ayLawr91UXernox4DVy3piDr5lB4_i241k1rZu_V'
            },
            data:qs.stringify({
                grant_type:"client_credentials"
            }),
            headers:{
                'Access-Control-Allow-Origin':'*',
                'Access-Control-Allow-Credentials':true,
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Headers':'Authorization,Content-Type,Accept',
                'Content-Type':'application/x-www-form-urlencoded'
            }
        })
        var reqHeader={
            headers:{
                'Content-Type':'application/json',
                Authorization:'Bearer '+ token.data.access_token
            }
        }
        var bodyjson={
            reason:"_"
        }
        axios.post('https://api.sandbox.paypal.com/v1/billing/subscriptions/'+this.state.subscription.data.id+'/activate',bodyjson,reqHeader)
            .then((resp)=>{
                alert("Il tuo abbonamento è stato riattivato.")
                window.location.pathname="/myapps"
            }).catch((error)=>{
                alert(error);
                this.forceUpdate();
                return;
            });
    }

    async componentDidMount(){
        const axios = require('axios').default;
        const cookie = new Cookies();
        var qs=require('qs');
        var bodyjson = {
            email: cookie.get("account").email,
            password: sessionStorage.getItem("pwd")
        }
        var token=await axios({
            method:"POST",
            url:'https://api.sandbox.paypal.com/v1/oauth2/token',
            auth:{
                username:'ARsQyOh4CzmSbX1dPH61I4fJ1FePhjoKrxlr890-qKletuKWRJS1HLEao8MdQeaLgebDcDITK5kSxC92',
                password:'EKuK8uW2tnHHJrBXBsryZGAuSToCSjeRkQnPfUr1ayLawr91UXernox4DVy3piDr5lB4_i241k1rZu_V'
            },
            data:qs.stringify({
                grant_type:"client_credentials"
            }),
            headers:{
                'Access-Control-Allow-Origin':'*',
                'Access-Control-Allow-Credentials':true,
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Headers':'Authorization,Content-Type,Accept',
                'Content-Type':'application/x-www-form-urlencoded'
            }
        })
        var reqHeader = {
            headers:{
                'Content-Type':'application/json',
                Authorization:'Bearer '+ token.data.access_token
            }
        }
        var pay = await axios.post('https://api.creappa.com/getAccountInfo', bodyjson);
        this.setState({
            oldPlan:pay.data.info.plan
        })
        var sub,planQuant;
        if(pay.data.info.payment!=="0"){
            sub = await axios.get('https://api.sandbox.paypal.com/v1/billing/subscriptions/' + pay.data.info.payment,reqHeader);
            this.setState({
                subscription: sub,
                hasntPay: false
            })
            if(sub.data.status!=='ACTIVE'){
                this.setState({
                    disableBtn:true,
                    isSusp:true 
                })     
            }
            switch(pay.data.info.plan){
                case 0:
                    this.setState({
                        planQuant:1
                    })
                    break;
                case 1:
                    this.setState({
                        planQuant:5
                    })
                    break;
            }
        } else {
            this.setState({
                disableBtn:true,
                hasntPay:true
            })
        }
        const resp = await axios.post('https://api.creappa.com/getAppsPerHolder', bodyjson);
        if(resp.data.status==="successful"){
            var apps = resp.data.apps;
            var i=0;
            if(apps.length>planQuant){
                alert("Abbiamo constatato che stai pagando per meno app di quelle che effettivamente hai. Cambia piano per poter iniziare a modificare le tue applicazioni.")
                this.setState({
                    disableBtn:true
                })
            }
            console.log('setting apps in state')
            this.setState({
                applications: apps
            })
        }
    }

    handleOnCardClick(app){
        const cookie = new Cookies();
        cookie.set('app', {
            index: app.index,
            subdomain:app.subdomain
        }, { path : "/"})
        window.location.pathname="/admin"
    }

    planChoosing(btn){
        this.setState({
            type:btn
        })
    }

    render(){
        console.log(this.state)
        return(
            <div>
                <div className="immagine-sfondo-pa">
                    <Navbar2/>
                    <div>
                        {
                            this.state.applications!=="" && this.state.hasntPay ?
                            <div className="not-pay-advise-div">
                                <p>Ops... Non hai ancora sottoscritto un abbonamento... Fallo Subito!</p>
                                <p>Scegli un piano di abbonamento:</p>
                                <ButtonGroup>
                                    <Button variant="outline-info" onClick={() => this.planChoosing("0")}>Piano Base</Button>
                                    <Button variant="outline-info" onClick={() => this.planChoosing("1")}>Piano Business</Button>
                                </ButtonGroup>
                                {
                                    this.state.type !== "" ?
                                    <div className="not-pay-advise-div"><PayPalBtn type={this.state.type}></PayPalBtn></div> :
                                    <div/>
                                }
                            </div> 
                        : this.state.isSusp ? 
                            <div className="not-pay-advise-div">
                                <p>Oh... Il tuo abbonamento risulta sospeso... Riattivalo Subito!</p>
                                <Row>
                                    <Button variant="success" onClick={()=>this.activateSub()}>Clicca qui!</Button>
                                    <p style={{position:"absoulute",bottom:0}}>oppure</p>
                                    {
                                        this.state.oldPlan ==="0" ?
                                        <div>
                                            <Popup trigger={
                                                <Button variant="outline-info" onClick={()=>this.planChoosing("1")}>Upgrade!</Button>
                                            }
                                                on={['hover', 'focus']}
                                                position='top center'
                                            >
                                                <span>Passa subito al piano di abbonamento "Business"!</span>
                                            </Popup>
                                        </div> 
                                        :
                                        <div>
                                            <Popup trigger={
                                                <Button variant="outline-info" onClick={()=>this.planChoosing("0")}>Downgrade!</Button>
                                                }
                                                on={['hover', 'focus']}
                                                position='top center'
                                            >
                                                <span>Conto troppo salato? Passa all'abbonamento "Base"</span>
                                            </Popup>
                                        </div>
                                    }
                                </Row>
                                {
                                    this.state.type !== "" ?
                                    <div className="not-pay-advise-div"><PayPalBtn type={this.state.type}></PayPalBtn></div> :
                                    <div/>
                                }
                            </div>
                        : 
                            <div/>
                        }
                    </div>
                    <div id="cardDeck" className="card-deck contenitore-cards">
                        {
                            this.state.applications === "" ? 
                            <div className="loading"> <CircularProgress className="crc-prg" color='primary'></CircularProgress></div> :
                            this.state.applications.map((app, index) => {
                                return (
                                    <Card className="card" style={{minWidth:'25%',margin:'1%'}}>
                                        <Card.Body>
                                            <Card.Title><strong>{app.subdomain}</strong></Card.Title>
                                            <Card.Text>Data di creazione: {app.creation_date}</Card.Text>
                                        </Card.Body>
                                        <Card.Footer className="card-footer">
                                            <Button className="manage-button" disabled={ false/* this.state.applications.length > this.state.subscription.data.quantity ? true : false */} onClick={() => this.handleOnCardClick(app)}>Gestisci</Button>
                                        </Card.Footer>
                                    </Card>
                                )
                            })
                        }
                    </div>
                    <div style={{width:"100%"}}>
                        {
                            this.state.applications!=="" && this.state.planQuant>=this.state.applications.length && !this.state.isSusp ?
                            <Link to={"/newapp"}>
                                <Popup trigger={open=>(
                                    <Button className="add-button" variant="outline-dark" open="open">+</Button>
                                )}
                                position="top center"
                                on={['hover', 'focus']}
                                closeOnDocumentClick
                                >
                                    <span>Inizia subito a creare la tua nuova app!</span>
                                </Popup>
                            </Link> :
                            <div/>
                        }
                    </div>
                </div>
            </div>
        )
    }
}
export default MyApps