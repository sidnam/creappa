import React, { Component } from "react"
import Navbar2 from "../components/Navbar2"
import "../styles/myapps.css"
import 'bootstrap/dist/css/bootstrap.css'
import Cookies from 'universal-cookie'
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';
import {Col, Row, Button, Card, ButtonGroup} from 'react-bootstrap'
import {CircularProgress} from "@material-ui/core"
import qs from 'qs'


import uploadLogoBlank from '../images/Images/upload-logo-blank.svg'
class MyApps extends Component{

    constructor(props){
        super(props)
        this.state = {
            applications: [],
            subscription: "",
            disableBtn: false,
            hasntPay:true,
            type: "",
            planQuant: 0,
            oldPlan: ""
        }
        this.handleOnCardClick = this.handleOnCardClick.bind(this)
    }

    async activateSub(){
        const axios = require('axios').default;
        var token=await axios({
            method:"POST",
            url:'https://api.sandbox.paypal.com/v1/oauth2/token',
            auth:{
                username:'ARsQyOh4CzmSbX1dPH61I4fJ1FePhjoKrxlr890-qKletuKWRJS1HLEao8MdQeaLgebDcDITK5kSxC92',
                password:'EKuK8uW2tnHHJrBXBsryZGAuSToCSjeRkQnPfUr1ayLawr91UXernox4DVy3piDr5lB4_i241k1rZu_V'
            },
            data:qs.stringify({
                grant_type:"client_credentials"
            }),
            headers:{
                'Access-Control-Allow-Origin':'*',
                'Access-Control-Allow-Credentials':true,
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Headers':'Authorization,Content-Type,Accept',
                'Content-Type':'application/x-www-form-urlencoded'
            }
        })
        var reqHeader={
            headers:{
                'Content-Type':'application/json',
                Authorization:'Bearer '+ token.data.access_token
            }
        }
        var bodyjson={
            reason:"_"
        }
        axios.post('https://api.sandbox.paypal.com/v1/billing/subscriptions/'+this.state.subscription.data.id+'/activate',bodyjson,reqHeader)
            .then((resp)=>{
                alert("Il tuo abbonamento è stato riattivato.")
                window.location.pathname="/appsmanagement"
            }).catch((error)=>{
                alert(error);
                this.forceUpdate();
                return;
            });
    }

    async componentDidMount(){
        const axios = require('axios').default;
        const cookie = new Cookies();
        var qs=require('qs');
        var bodyjson = {
            email: cookie.get("account").email,
            password: sessionStorage.getItem("pwd")
        }
        var token=await axios({
            method:"POST",
            url:'https://api.sandbox.paypal.com/v1/oauth2/token',
            auth:{
                username:'ARsQyOh4CzmSbX1dPH61I4fJ1FePhjoKrxlr890-qKletuKWRJS1HLEao8MdQeaLgebDcDITK5kSxC92',
                password:'EKuK8uW2tnHHJrBXBsryZGAuSToCSjeRkQnPfUr1ayLawr91UXernox4DVy3piDr5lB4_i241k1rZu_V'
            },
            data:qs.stringify({
                grant_type:"client_credentials"
            }),
            headers:{
                'Access-Control-Allow-Origin':'*',
                'Access-Control-Allow-Credentials':true,
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Headers':'Authorization,Content-Type,Accept',
                'Content-Type':'application/x-www-form-urlencoded'
            }
        })
        var reqHeader = {
            headers:{
                'Content-Type':'application/json',
                Authorization:'Bearer '+ token.data.access_token
            }
        }
        var pay = await axios.post('https://api.creappa.com/getAccountInfo', bodyjson);
        this.setState({
            oldPlan:pay.data.info.plan
        })
        var sub,planQuant;
        if(pay.data.info.payment!=="0"){
            sub = await axios.get('https://api.sandbox.paypal.com/v1/billing/subscriptions/' + pay.data.info.payment,reqHeader);
            this.setState({
                subscription: sub,
                hasntPay: false
            })
            if(sub.data.status!=='ACTIVE'){
                this.setState({
                    disableBtn:true,
                    isSusp:true 
                })     
            }
            switch(pay.data.info.plan){
                case 0:
                    this.setState({
                        planQuant:1
                    })
                    break;
                case 1:
                    this.setState({
                        planQuant:5
                    })
                    break;
            }
        } else {
            this.setState({
                disableBtn:true,
                hasntPay:true
            })
        }
        const resp = await axios.post('https://api.creappa.com/getAppsPerHolder', bodyjson);
        if(resp.data.status==="successful"){
            var apps = resp.data.apps;
            var i=0;
            if(apps.length>planQuant){
                alert("Abbiamo constatato che stai pagando per meno app di quelle che effettivamente hai. Cambia piano per poter iniziare a modificare le tue applicazioni.")
                this.setState({
                    disableBtn:true
                })
            }
            console.log('setting apps in state')
            this.setState({
                applications: apps
            })
        }
    }

    handleOnCardClick(app){
        const cookie = new Cookies();
        cookie.set('app', {
            index: app.index,
            subdomain:app.subdomain
        }, { path : "/"})
        this.props.handleCloseApps()
        this.props.handleChooseApp()
    }

    planChoosing(btn){
        this.setState({
            type:btn
        })
    }

    render(){
        console.log(this.state)
        return(
            <div className="card-deck contenitore-cards">
                {
                   this.state.applications.map((app, index)=>{
                    return (
                    <Card style={{minWidth:'45%',margin:'1%'}}>
                        <Card.Body>
                            <Row>
                                <Col>
                                    <img src={uploadLogoBlank}></img>
                                </Col>
                                <Col className="app-info">
                                    <span className="app-name"> {app.subdomain}</span>
                                    <label className="app-creation-date-label">Data di Creazione:</label>
                                    <div className="app-creation-date">{app.creation_date}</div>
                                </Col>
                                
                            </Row>
                        </Card.Body>
                        <Card.Footer className="app-card-footer justify-content-center" onClick={() => this.handleOnCardClick(app)} >
                            <Button className="app-card-footer-btn" disabled={index >= this.state.subscription.data.quantity ? true : false } onClick={ () => this.handleOnCardClick(app)}>GESTISCI APP</Button>
                        </Card.Footer>
                    </Card>
                    )
                   })
                }
                <Card style={{minWidth:'45%',margin:'1%'}}>
                        <Card.Body>
                            <Row>
                                <Col>
                                    <img src={uploadLogoBlank}></img>
                                </Col>
                                <Col className="app-info">
                                    <span className="app-name">Crea la tua app ora!</span>
                                </Col>
                                
                            </Row>
                        </Card.Body>
                        <Card.Footer className="app-card-footer justify-content-center" onClick={() => window.location.pathname='/newapp'}>
                            CREA APP
                        </Card.Footer>
                    </Card>
            </div>
        )
    }
}
export default MyApps