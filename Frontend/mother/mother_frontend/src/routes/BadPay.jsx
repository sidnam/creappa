import React, { Component } from "react"
import {Link} from "react-router-dom"
import '../styles/nextpay.css'
import logo from "../images/Logo/Creappa logo.svg"
import {Card, Button} from 'react-bootstrap'
import badpayImg from "../images/Illustrations/error payment.svg"

class BadPay extends Component{

    render(){
        return(
            <div>
                <div className="immagine-sfondo-na">
                    <img className="logo" src={logo} alt="logo"/>
                    <Card className="blocco">
                        <img className="cardimg" src={badpayImg} alt="bad pay"/>
                        <div className="bloctitle">Oh no!<br/>Il pagamento non è andato a buon fine!</div>
                        <div className="cardcont">Non ti preoccupare!<br/>Controlla il tuo account Paypal per capire se il pagamento è avvenuto, in tal caso vuoldire che è un problema dovuto ai nostri server, quindi contatta l'assistenza per risolvere il problema. Se invece Paypal non ha registrato il pagamento ripeti la procedura.</div>
                        <center>
                            <Link to={"/appsmanagement"}><Button className="gradient-btn" variant="light"><div className="btnTitle">Vai alla dashboard!</div></Button></Link>
                        </center>
                    </Card>
                </div>
            </div>
        )
    }
}
export default BadPay