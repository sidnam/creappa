import React, { Component } from "react"
import {Form, Row, Card, Container, Button} from "react-bootstrap";
import Cookies from 'universal-cookie';

import FileUploader from "../utilities/FileUploader";

import logo from "../images/Logo/Creappa logo.svg"
import blackApp from "../images/Images/template-pngs/black-app.jpg"
import orangeApp from "../images/Images/template-pngs/orange-app.jpg"
import lightGreenApp from "../images/Images/template-pngs/light-green-app.jpg"

import 'bootstrap/dist/css/bootstrap.css'
import '../styles/newapp.css'
import uploadLogoBlank from '../images/Images/upload-logo-blank.svg'
import { Link } from "react-router-dom"

//**********************************************************************************************************

//**********************************************************************************************************

class NewApp extends Component{
    constructor(props){
        super(props)
        this.typeInput=React.createRef();
        this.domainInput=React.createRef();

        this.onChangeLogoHandler = this.onChangeLogoHandler.bind(this)
        this.sendNewApp = this.sendNewApp.bind(this)
        this.setDomain = this.setDomain.bind(this)
        this.state = {
            progress: 0,
            domain:"",
            selectedLogoFile : "",
            selectedBlobLogoFile: uploadLogoBlank,
            selectedFont : "Select a Font",
        }

        this.fonts = [
            'Roboto', 'Arial', 'Helvetica', 'Segoe Script', 'Verdana', 'Ubuntu', 
            'Times New Roman', 'Work Sans', 'Helvetica Neue', 'Open Sans', 'Marvel', 'Sora',
            'Abel', 'Prompt', 'Sedan', 'Myriad Pro', 'Montserrat', 'Josefin Slab'
        ]

    }

     chooseTemplateComponents(templateIndex){
         // 0 --> BAR  black
         // 2 --> NEGOZIO MUSICALE blackblack
         // 3 --> NEGOZIO aranzione
         // 1 --> MEDICO light green
        switch(templateIndex){
            case 0:
                return ({
                "components": [
                {
                        "orari": [
                            {
                                "giorno": "lunedi",
                                "orario": "8:00-12:00/14:00-18:00"
                            },
                            {
                                "giorno": "martedi",
                                "orario": "8:00-12:00/14:00-18:00"
                            },
                            {
                                "giorno": "mercoledi",
                                "orario": "8:00-12:00/14:00-18:00"
                            }
                        ],
                        "testo": "inserisci un testo",
                        "titolo": "Orario",
                        "type": "Schedule"
                },
                {

                    "immagine": "https://creappa.s3.amazonaws.com/resources/paltolab/image.jpg%3Ff%3Ddetail_558%26h%3D720%26w%3D1280%26%24p%24f%24h%24w%3Dd5eb06a.jpeg",
                    "testo": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices odio vitae sapien dapibus, et porttitor lorem tempor. Nulla nisi mauris, consectetur ac nibh ac, semper porttitor eros. Cras fringilla urna ut ex maximus ullamcorper. Ut fringilla mattis vulputate. Curabitur et ligula in mauris laoreet consectetur sit amet sit amet turpis. Nunc vestibulum dignissim quam id consequat. Sed eleifend neque vitae finibus elementum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. ",
                    "titolo": "Chi siamo",
                    "type": "About"

                },
                {

                        "prodotti": [
                            {
                                "descrizione": "Comprami! Non te ne pentirai",
                                "img": "https://creappa.s3.amazonaws.com/resources/paltolab/alcolici-a-minorenni-635432.660x368.jpg",
                                "prezzo": "10",
                                "titolo": "Drink"
                            },
                            {
                                "descrizione": "Comprami, non te ne pentirai!",
                                "img": "https://creappa.s3.amazonaws.com/resources/paltolab/old-fashioned-cocktail.jpg",
                                "prezzo": "10",
                                "titolo": " Drink2"
                            }
                        ],
                        "testo": "inserisci un testo",
                        "titolo": "Negozio",
                        "type": "Shop"

                },
                {

                    "galleria": [
                        {
                            "descrizione": "testo di prova della prima slide",
                            "immagine": "https://www.auroraseriate.it/wp-content/themes/auroraseriate/assets/images/common/400x300.gif"
                        },
                        {
                            "descrizione": "testo di prova della seconda slide",
                            "immagine": "https://www.auroraseriate.it/wp-content/themes/auroraseriate/assets/images/common/400x300.gif"
                        },
                        {
                            "descrizione": "testo di prova della terza slide",
                            "immagine": "https://www.auroraseriate.it/wp-content/themes/auroraseriate/assets/images/common/400x300.gif"
                        }
                    ],
                    "titolo": "Galleria",
                    "type": "Galleria"
                },
                {

                    "testo": "Prendi un appuntamento",
                    "titolo": "Prendi appuntamento",
                    "type": "Date"
                },
                {

                    "sito": "www.barallangolo.com",
                    "testo": "Visita il nostro sito! Puoi trovare un sacco di informazioni utili sui nostri dipendenti!",
                    "titolo": "Il nostro sito",
                    "type": "WebPage"
                },
                {

                    "posizione": {
                        "lat": "45.44698568174512",
                        "lng": "9.19225804848081"
                    },
                    "titolo": "Dove ci troviamo",
                    "type": "Where"

                },
                {
                    "type": "Report"
                },
                {
                    "contatti": [
                        {
                            "nome": "Indirizzo",
                            "testo": "Via Garibaldi"
                        },
                        {
                            "nome": "numero",
                            "testo": "888 8889944"
                        },
                        {
                            "nome": "Email",
                            "testo": "nome@email.it"
                        }
                    ],
                    "titolo": "Contatti",
                    "type": "Contacts"
                }
            ]
            })
            break
            case 1:
                return ({
                    "components": [
                    {
                        "orari": [
                            {
                                "giorno": "lunedi",
                                "orario": "8:00-12:00/14:00-18:00"
                            },
                            {
                                "giorno": "martedi",
                                "orario": "8:00-12:00/14:00-18:00"
                            },
                            {
                                "giorno": "mercoledi",
                                "orario": "8:00-12:00/14:00-18:00"
                            }
                        ],
                        "testo": "inserisci un testo",
                        "titolo": "Orario",
                        "type": "Schedule"
                    },
                    {

                        "immagine": "https://www.ondamusicale.it/images/2017/Stefano/novembre1/Music_Center.jpg",
                        "testo": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices odio vitae sapien dapibus, et porttitor lorem tempor. Nulla nisi mauris, consectetur ac nibh ac, semper porttitor eros. Cras fringilla urna ut ex maximus ullamcorper. Ut fringilla mattis vulputate. Curabitur et ligula in mauris laoreet consectetur sit amet sit amet turpis. Nunc vestibulum dignissim quam id consequat. Sed eleifend neque vitae finibus elementum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. ",
                        "titolo": "Chi siamo",
                        "type": "About"

                    },
                    {

                        "prodotti": [
                            {
                                "descrizione": "Comprami! Non te ne pentirai",
                                "img": "https://8a476395db43219b7f09-f3e6caa3c30433b43b78e616f256f2b3.ssl.cf3.rackcdn.com/tamburo-operasnd1465ns-rullante-opera4618.png",
                                "prezzo": "50",
                                "titolo": "Strumento musicale"
                            },
                            {
                                "descrizione": "Comprami, non te ne pentirai!",
                                "img": "https://images.dampi.it/it/computedimage/950-4-4-ambra-chitarra-classica-a.i83929-kE8szhP-h1800-l1.jpg",
                                "prezzo": "230",
                                "titolo": " Chitarra"
                            }
                        ],
                        "testo": "inserisci un testo",
                        "titolo": "Negozio",
                        "type": "Shop"

                    },
                    {

                        "galleria": [
                            {
                                "descrizione": "testo di prova della prima slide",
                                "immagine": "https://www.auroraseriate.it/wp-content/themes/auroraseriate/assets/images/common/400x300.gif"
                            },
                            {
                                "descrizione": "testo di prova della seconda slide",
                                "immagine": "https://www.auroraseriate.it/wp-content/themes/auroraseriate/assets/images/common/400x300.gif"
                            },
                            {
                                "descrizione": "testo di prova della terza slide",
                                "immagine": "https://www.auroraseriate.it/wp-content/themes/auroraseriate/assets/images/common/400x300.gif"
                            }
                        ],
                        "titolo": "Galleria",
                        "type": "Galleria"

                    },
                    {

                        "testo": "Prendi un appuntamento",
                        "titolo": "Prendi appuntamento",
                        "type": "Date"

                    },
                    {

                        "sito": "www.strumentoMusicale.com",
                        "testo": "Visita il nostro sito! Puoi trovare un sacco di informazioni utili sui nostri dipendenti!",
                        "titolo": "Il nostro sito",
                        "type": "WebPage"

                    },
                    {

                        "posizione": {
                            "lat": "45.44698568174512",
                            "lng": "9.19225804848081"
                        },
                        "titolo": "Dove ci troviamo",
                        "type": "Where"

                    },
                    {
                        "type": "Report"
                    },
                    {

                        "contatti": [
                            {
                                "nome": "Indirizzo",
                                "testo": "Via Garibaldi"
                            },
                            {
                                "nome": "numero",
                                "testo": "888 8889944"
                            },
                            {
                                "nome": "Email",
                                "testo": "nome@email.it"
                            }
                        ],
                        "titolo": "Contatti",
                        "type": "Contacts"
                    }
                ]
                })
                break
                case 3:
                    return ({
                    "components": [
                    {
                        "orari": [
                            {
                                "giorno": "lunedi",
                                "orario": "8:00-12:00/14:00-18:00"
                            },
                            {
                                "giorno": "martedi",
                                "orario": "8:00-12:00/14:00-18:00"
                            },
                            {
                                "giorno": "mercoledi",
                                "orario": "8:00-12:00/14:00-18:00"
                            }
                        ],
                        "testo": "inserisci un testo",
                        "titolo": "Orario",
                        "type": "Schedule"
                    },
                    {
                        "immagine": "https://www.ondamusicale.it/images/2017/Stefano/novembre1/Music_Center.jpg",
                        "testo": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices odio vitae sapien dapibus, et porttitor lorem tempor. Nulla nisi mauris, consectetur ac nibh ac, semper porttitor eros. Cras fringilla urna ut ex maximus ullamcorper. Ut fringilla mattis vulputate. Curabitur et ligula in mauris laoreet consectetur sit amet sit amet turpis. Nunc vestibulum dignissim quam id consequat. Sed eleifend neque vitae finibus elementum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. ",
                        "titolo": "Chi siamo",
                        "type": "About"
                    },
                    {
                        "prodotti": [
                            {
                                "descrizione": "Comprami! Non te ne pentirai",
                                "img": "https://corsisicurezzalavoroweb.it/wp-content/uploads/2019/06/visite-mediche-lavoro.jpg",
                                "prezzo": "50",
                                "titolo": "Visita medica"
                            }
                        ],
                        "testo": "inserisci un testo",
                        "titolo": "Negozio",
                        "type": "Shop"

                    },
                    {
                        "galleria": [
                            {
                                "descrizione": "testo di prova della prima slide",
                                "immagine": "https://www.auroraseriate.it/wp-content/themes/auroraseriate/assets/images/common/400x300.gif"
                            },
                            {
                                "descrizione": "testo di prova della seconda slide",
                                "immagine": "https://www.auroraseriate.it/wp-content/themes/auroraseriate/assets/images/common/400x300.gif"
                            },
                            {
                                "descrizione": "testo di prova della terza slide",
                                "immagine": "https://www.auroraseriate.it/wp-content/themes/auroraseriate/assets/images/common/400x300.gif"
                            }
                        ],
                        "titolo": "Galleria",
                        "type": "Galleria"

                    },
                    {
                        "testo": "Prendi un appuntamento",
                        "titolo": "Prendi appuntamento",
                        "type": "Date"
                    },
                    {
                        "sito": "www.strumentoMusicale.com",
                        "testo": "Visita il nostro sito! Puoi trovare un sacco di informazioni utili sui nostri dipendenti!",
                        "titolo": "Il nostro sito",
                        "type": "WebPage"
                    },
                    {
                        "posizione": {
                            "lat": "45.44698568174512",
                            "lng": "9.19225804848081"
                        },
                        "titolo": "Dove ci troviamo",
                        "type": "Where"
                    },
                    {
                        "type": "Report"
                    },
                    {
                        "contatti": [
                            {
                                "nome": "Indirizzo",
                                "testo": "Via Garibaldi"
                            },
                            {
                                "nome": "numero",
                                "testo": "888 8889944"
                            },
                            {
                                "nome": "Email",
                                "testo": "nome@email.it"
                            }
                        ],
                        "titolo": "Contatti",
                        "type": "Contacts"
                    }
                ]
            })
            break
            case 3:
                return ({
                "components": [
                        {

                            "orari": [
                                {
                                    "giorno": "lunedi",
                                    "orario": "8:00-12:00/14:00-18:00"
                                },
                                {
                                    "giorno": "martedi",
                                    "orario": "8:00-12:00/14:00-18:00"
                                },
                                {
                                    "giorno": "mercoledi",
                                    "orario": "8:00-12:00/14:00-18:00"
                                }
                            ],
                            "testo": "inserisci un testo",
                            "titolo": "Orario",
                            "type": "Schedule"

                        },
                        {

                            "immagine": "https://www.ondamusicale.it/images/2017/Stefano/novembre1/Music_Center.jpg",
                            "testo": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ultrices odio vitae sapien dapibus, et porttitor lorem tempor. Nulla nisi mauris, consectetur ac nibh ac, semper porttitor eros. Cras fringilla urna ut ex maximus ullamcorper. Ut fringilla mattis vulputate. Curabitur et ligula in mauris laoreet consectetur sit amet sit amet turpis. Nunc vestibulum dignissim quam id consequat. Sed eleifend neque vitae finibus elementum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. ",
                            "titolo": "Chi siamo",
                            "type": "About"

                        },
                        {
                            "prodotti": [
                                {
                                    "descrizione": "Comprami! Non te ne pentirai",
                                    "img": "https://media-cdn.tripadvisor.com/media/photo-s/10/8a/cb/60/specialita-piatti-ristorante.jpg",
                                    "prezzo": "25",
                                    "titolo": "Piatto 1"
                                },
                                {
                                    "descrizione": "Comprami, non te ne pentirai!",
                                    "img": "https://www.ristorantedarosa.com/files/.thumbs/home/slider-2020/2000x/da-rosa-ristorante-pesce-como-polpo-patate.jpg",
                                    "prezzo": "30",
                                    "titolo": " Piatto 2"
                                }
                            ],
                            "testo": "inserisci un testo",
                            "titolo": "Negozio",
                            "type": "Shop"

                        },
                        {
                            "galleria": [
                                {
                                    "descrizione": "testo di prova della prima slide",
                                    "immagine": "https://www.auroraseriate.it/wp-content/themes/auroraseriate/assets/images/common/400x300.gif"
                                },
                                {
                                    "descrizione": "testo di prova della seconda slide",
                                    "immagine": "https://www.auroraseriate.it/wp-content/themes/auroraseriate/assets/images/common/400x300.gif"
                                },
                                {
                                    "descrizione": "testo di prova della terza slide",
                                    "immagine": "https://www.auroraseriate.it/wp-content/themes/auroraseriate/assets/images/common/400x300.gif"
                                }
                            ],
                            "titolo": "Galleria",
                            "type": "Galleria"

                        },
                        {

                            "testo": "Prendi un appuntamento",
                            "titolo": "Prendi appuntamento",
                            "type": "Date"

                        },
                        {

                            "sito": "www.strumentoMusicale.com",
                            "testo": "Visita il nostro sito! Puoi trovare un sacco di informazioni utili sui nostri dipendenti!",
                            "titolo": "Il nostro sito",
                            "type": "WebPage"

                        },
                        {
                            "posizione": {
                                "lat": "45.44698568174512",
                                "lng": "9.19225804848081"
                            },
                            "titolo": "Dove ci troviamo",
                            "type": "Where"

                        },
                        {

                            "type": "Report"

                        },
                        {
                            "contatti": [
                                {
                                    "nome": "Indirizzo",
                                    "testo": "Via Garibaldi"
                                },
                                {
                                    "nome": "numero",
                                    "testo": "888 8889944"
                                },
                                {
                                    "nome": "Email",
                                    "testo": "nome@email.it"
                                }
                            ],
                            "titolo": "Contatti",
                            "type": "Contacts"

                        }
                    ]
                })
                break
        }
    }

        // 0 --> BAR  black
         // 2 --> NEGOZIO MUSICALE blackblack
         // 3 --> NEGOZIO aranzione
         // 1 --> MEDICO light green

    chooseTemplateColors(templateIndex){
        switch(templateIndex){
            case 1:
                return{
                    principale:"#7baea5",
                    secondario: "#ffffff",
                    sfondo: "#ffffff"
                }
            case 2:
                return {
                    principale:"#000000",
                    secondario: "#ffffff",
                    sfondo: "#ffffff"
                }
                break
            case 3:
                return{
                    principale:"#da7f26",
                    secondario: "#ffffff",
                    sfondo: "#ffffff"
                }
                break
            case 0:
                return{
                    principale:"#263238",
                    secondario: "#ffffff",
                    sfondo: "#ffffff"
                }
        }
    }

    chooseTemplateBackground(templateIndex){
        switch(templateIndex){
            case 0:
                return "https://creappa.s3.eu-central-1.amazonaws.com/resources/background-template/sfondo1.jpg"
            case 1:
                return "https://creappa.s3.eu-central-1.amazonaws.com/resources/background-template/sfondo2.jpg"
            case 2:
                return "https://creappa.s3.eu-central-1.amazonaws.com/resources/background-template/sfondo3.jpg"
            case 3:
                return "https://creappa.s3.eu-central-1.amazonaws.com/resources/background-template/sfondo4.jpg"
        }
    }


    onChangeLogoHandler=event=>{
        var url = URL.createObjectURL(event.target.files[0])
        console.log(event.target.files[0])
        console.log(url)
        
        this.setState({
          selectedLogoFile: event.target.files[0],
          selectedBlobLogoFile: url,
          loaded: false
        })
    }

    //**********************************************************************************************************

    onChangeBackgroundHandler=event=>{
        console.log(event.target.files[0])
        this.setState({
            selectedBackgroundFile: event.target.files[0],
            loaded: false
        })
    }

    //**********************************************************************************************************

    async setDomain(event){
        const axios=require('axios').default;
        var newDomain=event.target.value;
        const domavailable= await axios.get('https://api.creappa.com/isDomainAvailable', {
            params: {
                subdomain:newDomain
            }
        })
        if(domavailable.data.domainStatus === 'available'){
            this.setState({domain: newDomain});
        } else {
            alert("Dominio NON disponibile!\nRiprova con un dominio differente.")
        }
    }

    //**********************************************************************************************************

    async componentDidMount(){
        const axios=require('axios').default;
        const resp = await axios.get('https://api.creappa.com/getComponentsTypeList');
        if(resp.data.status==="successful"){
            var components=resp.data.componentsTypes;
            var i=0;
            this.setState({
                availablePlugins: resp.data.componentsTypes
            })
        }
    }

    //**********************************************************************************************************

    async sendNewApp(){
        console.log('sendNewApp')
        const cookie = new Cookies();
        const axios = require('axios').default;
        var domain = this.state.domain;
        var plugins =  []
        this.chooseTemplateComponents(this.state.selectedTemplate).components.map((component, index) => plugins.push(component))
        

        var fileUploader = new FileUploader("creappa", "AKIA4OXXCC6QQVU5Y7HM", "Qh9zS3gX5T3rbphrj1VQEpmlj6BevfQeuC2978sQ")
        fileUploader.uploadLogoAndBackground(this.state.selectedLogoFile, this.state.selectedBackgroundFile, domain)
            .then(async (locations) => {
                var bodyjson = {
                    email: cookie.get("account").email,
                    password: sessionStorage.getItem("pwd"),
                    subdomain: domain,
                    resources: [
                        {
                            type: "logo",
                            path: locations.logo
                        },
                        {
                            type: "background_image",
                            path: this.chooseTemplateBackground(this.state.selectedTemplate)
                        }
                    ],
                    components: plugins,
                    colors:this.chooseTemplateColors(this.state.selectedTemplate),
                    font: this.state.selectedFont
                }
                console.log(bodyjson)
                const resp = await axios.post('https://api.creappa.com/createNewApp',bodyjson);
                console.log(resp)
                if(resp.data.status==="successful"){
                    alert("L'app è stata creata correttamente ed è disponibile al seguente link: "+resp.data.url)
                    window.location.pathname="/appsmanagement"
                } 
            })
    }

    
//**********************************************************************************************************

    enableSubmit(){
        var subbtn=document.getElementById("subbtn");
        subbtn.disabled=false;
    }

    //**********************************************************************************************************

    nextStep(){
        switch(this.state.progress){
            case 0:
                if(this.state.domain != "")
                    this.setState({progress: this.state.progress + 1 });
                else
                    alert('Ti sei dimenticato di inserire il nome dell\' applicazione')
            break;
            case 3:
                this.sendNewApp()
            default:
                this.setState({progress: this.state.progress + 1 });
            break;
        }
    }

    //**********************************************************************************************************

    prevStep(){
        this.setState({progress: this.state.progress - 1 });
    }

    //**********************************************************************************************************
    
    render(){
        switch(this.state.progress){
            case 0: return(
                <div >
                    <div className="immagine-sfondo-na">
                        <Link to="/"><img src={logo} alt="logo"/></Link>
                        <Container fluid>
                            <Row className="justify-content-sm-center">
                                <Card className="creation-card">
                                    <h3 className="header">Inserisci il nome della tua app</h3>
                                    <center><div className="par">
                                        Questo nome verrà usato anche come dominio web dell'applicazione, quindi non inserire caratteri di punteggiatura o speciali (come ad esempio ,.:;...@#)
                                    </div></center>
                                    <Form id="creatore" className="app-form">
                                        <Form.Group className="app-form-element">
                                            <Form.Control type="text" placeholder="Inserisci il nome dell'applicazione..." onChange={this.setDomain}/>
                                        </Form.Group>
                                    </Form>
                                    <center><Button className="gradient-btn" variant="light" onClick={this.nextStep.bind(this)}><div className="btnTitle">Avanti</div></Button></center>
                                </Card>
                            </Row>
                        </Container>
                        
                    </div>
                </div>
            )
            // PAGINA 2: UPLOAD LOGO
            case 1: return(
                <div >
                    <div className="immagine-sfondo-na">
                        <Link to="/"><img src={logo} alt="logo"/></Link>
                        <Container fluid>
                            <Row className="justify-content-sm-center">
                                <Card className="creation-card">
                                    <h3 className="header">Carica il tuo logo</h3>
                                    <center><div className="par">
                                        Puoi caricarlo anche più tardi
                                    </div></center>
                                    <Form id="creatore" className="app-form">
                                        <Form.Row className="app-form-element justify-content-center">
                                            <img className="uploaded-logo" src={this.state.selectedBlobLogoFile}/>
                                            <Form.Control id="logo" type="file" onChange={this.onChangeLogoHandler}/>
                                        </Form.Row>
                                    </Form>
                                    <center>
                                        <Button className="gradient-btn" variant="light" onClick={this.prevStep.bind(this)}>
                                            <div className="btnTitle">Indietro</div>
                                        </Button>
                                        <Button className="gradient-btn" variant="light" onClick={this.nextStep.bind(this)}>
                                            <div className="btnTitle">Avanti</div>
                                        </Button>
                                    </center>
                                </Card>
                            </Row>
                        </Container>
                        
                    </div>
                </div>
                )

                //PAGINA 3: SCELTA FONT
                case 2:return(
                    <div >
                    <div className="immagine-sfondo-na">
                        <Link to="/"><img src={logo} alt="logo"/></Link>
                        <Container fluid>
                            <Row className="justify-content-sm-center">
                                <Card className="creation-card">
                                    <h3 className="header">Scegli il font che più ti piace</h3>
                                    <center><div className="par">
                                        Puoi caricarlo anche più tardi
                                    </div></center>
                                    <table className="font-table">
                                        {
                                            this.fonts.map((font, index) => {
                                                return(
                                                    <div onClick={()=>this.state.selectedFont=font} className="single-font">{font}</div>
                                                )
                                            })
                                        }
                                    </table>
                                    <center>
                                        <Button className="gradient-btn" variant="light" onClick={this.prevStep.bind(this)}>
                                            <div className="btnTitle">Indietro</div>
                                        </Button>
                                        <Button className="gradient-btn" variant="light" onClick={this.nextStep.bind(this)}>
                                            <div className="btnTitle">Avanti</div>
                                        </Button>
                                    </center>
                                </Card>
                            </Row>
                        </Container>
                    </div>
                </div>
            )

            //PAGINA 4: SCELTA TEMPLATE
            case 3: return(
                <div>
                    <div >
                    <div className="immagine-sfondo-na">
                        <img className="logo" src={logo} alt="logo"/>
                        <Container fluid>
                            <Row className="justify-content-sm-center">
                                <Card className="creation-card">
                                    <h3 className="header">Scegli il Template che si adatta alla tua attività</h3>
                                    <center><div className="par">
                                        Puoi cambiarlo anche più tardi
                                    </div></center>
                                    <Row className="justify-content-center template-imgs-row">
                                        <div className="div-img" onClick={() => this.setState({selectedTemplate:0})}>
                                            <img src={blackApp} className="template-app"></img>

                                        </div>
                                        <div className="div-img" onClick={() => this.setState({selectedTemplate:3})}>
                                            <img src={orangeApp} className="template-app"></img>
                                        </div>
                                        <div className="div-img" onClick={() => this.setState({selectedTemplate:1})}>
                                            <img src={lightGreenApp} className="template-app"></img>
                                        </div>
                                    </Row>
                                    <br/>
                                    <center>
                                        <Button className="gradient-btn" variant="light" onClick={this.prevStep.bind(this)}>
                                            <div className="btnTitle">Indietro</div>
                                        </Button>
                                        <Button className="gradient-btn" variant="light" onClick={this.nextStep.bind(this)}>
                                            <div className="btnTitle">Avanti</div>
                                        </Button>
                                    </center>
                                </Card>
                            </Row>
                        </Container>
                        
                    </div>
                </div>
                </div>
            )
        }
    }
}

//**********************************************************************************************************

export default NewApp