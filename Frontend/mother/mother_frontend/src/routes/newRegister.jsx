import React, { Component } from "react";
import logo from "../images/Logo/Creappa logo.svg"
import googleIcon from "../images/Logo/google-icon@2x.svg"
import "../styles/nextpay.css"
import { Link } from "react-router-dom"
import Cookies from 'universal-cookie'
import { Col, Row, Form, Button, Card } from "react-bootstrap"

class Register extends Component {

    constructor() {
        super();
        this.state = {
            index: 0,
            name: "",
            surname: "",
            cf: "",
            birthd: "",
            cell: "",
            bname: "",
            piva: "",
            mail: "",
            pwd: "",
            addr: "",
            nciv: ""
        }

        this.sendSignup = this.sendSignup.bind(this);
    }

    async sendSignup() {
        const variabili = {
            name: this.state.name,
            surname: this.state.surname,
            cf: this.state.cf,
            birthd : new Date(this.state.birthd),
            cell : this.state.cell,
            bname : this.state.bname,
            piva : this.state.piva,
            mail: this.state.mail,
            pwd: this.state.pwd,
            addr: this.state.addr,
            nciv : this.state.nciv
        }

        for(var key in variabili){
            if (variabili[key] == ""){
                alert("Riempi tutti i campi prima di continuare");
                return;
            }
        }

        if(variabili.pwd.length < 8) return alert("Password troppo corta (minimo 8 caratteri)");
        if(variabili.name.length <= 0) return alert("Nome troppo corto");
        if(variabili.surname.length <= 0) return alert("Cognome troppo corto");

        if(variabili.cf.length < 16) return alert("Codice fiscale non valido");
        if(variabili.cell.length < 13) return alert("Il numero di telefono non è valido (deve iniziare con +39)");
        if(variabili.bname.length <= 0) return alert("Il nome del business è invalido");
        if(variabili.piva.length != 11) return alert("La partita IVA non è corretta");
        if(variabili.addr.length <= 0) return alert("L'indirizzo inserito non è valido");
        if(variabili.nciv.length <= 0) return alert("Il numero civico non è corretto");

        console.log(variabili);
        const cookie = new Cookies();
        const axios = require('axios').default;
        const birthd = new Date(this.state.birthd);

        var bodyjson = {
            name: this.state.name,
            surname: this.state.surname,
            email: this.state.mail,
            password: this.state.pwd,
            bornday: {
                day: birthd.getDate(),
                month: (birthd.getMonth() + 1),
                year: birthd.getFullYear()
            },
            codFis: this.state.cf,
            cellularNumber: this.state.cell
        };
        const resp = await axios.post(
            'https://api.creappa.com/registerUser',
            bodyjson,
            { headers: { 'Content-Type': 'application/json' } }
        )
        console.log('ok1')
        if (resp.data.status === "successful") {
            bodyjson = {
                email: this.state.mail,
                businessName: this.state.bname,
                password: this.state.pwd,
                partitaIVA: this.state.piva,
                address: this.state.addr,
                numeroCivico: parseInt(this.state.nciv)
            }

            const resp2 = await axios.post(
                'https://api.creappa.com/registerBusiness',
                bodyjson,
                { headers: { 'Content-Type': 'application/json' } }
            )
            if (resp2.data.status === "successful") {
                alert("Registrazione avvenuta con successo");
                sessionStorage.setItem("pwd",this.state.pwd)
                cookie.set('account', {
                    email: this.state.mail
                }, { path: "/" })
                window.location.pathname = "/payment"
            } else {
                alert("Registrazione del business Errata: " + resp2.data.status)
            }
        } else {
            alert("Registrazione Errata: " + resp.data.status)
        }
    }

    render() {
        return (
            <div className="immagine-sfondo-na">
                <Link className="content-logo"  to="/">
                    <img className="logo" src={logo} alt="logo" />
                </Link>
                <Card className="blocco">
                    <Col className="content-signup">
                        <p className="bloctitle"> REGISTRATI </p>
                        <br />
                        {this.state.index == 0 ?
                            (<Form>
                                <Form.Row>
                                    <Form.Label for="name" className="cardcont" style={{ marginLeft: "-10px", marginBottom: "-10px", color: "rgba(90,90,90, 1)" }}>Nome*</Form.Label>
                                    <Form.Control name="name" className="email-input" type="text" value={this.state.nome} placeholder="Tuo nome" onChange={e => this.setState({ name: e.target.value })} style={{ borderRadius: "15px" }}></Form.Control>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Label for="surname" className="cardcont" style={{ marginLeft: "-10px", marginBottom: "-10px", color: "rgba(90,90,90, 1)" }}>Cognome*</Form.Label>
                                    <Form.Control className="email-input" type="text" value={this.state.surname} placeholder="Tuo cognome" onChange={e => this.setState({ surname: e.target.value })} style={{ borderRadius: "15px" }}></Form.Control>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Label for="codice fiscale" className="cardcont" style={{ marginLeft: "-10px", marginBottom: "-10px", color: "rgba(90,90,90, 1)" }}>Codice fiscale* </Form.Label>
                                    <Form.Control className="email-input" type="text" value={this.state.cf} placeholder="Tuo codice fiscale" onChange={e => this.setState({ cf: e.target.value })} style={{ borderRadius: "15px" }}></Form.Control>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Label for="cellulare" className="cardcont" style={{ marginLeft: "-10px", marginBottom: "-10px", color: "rgba(90,90,90, 1)" }}>Cellulare* </Form.Label>
                                    <Form.Control className="email-input" type="text" value={this.state.cell} placeholder="Tuo cellulare" onChange={e => this.setState({ cell: e.target.value })} style={{ borderRadius: "15px" }}></Form.Control>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Label for="data di nascita" className="cardcont" style={{ marginLeft: "-10px", marginBottom: "-10px", color: "rgba(90,90,90, 1)" }}>Data di nascita*</Form.Label>
                                    <Form.Control className="email-input" type="date" value={this.state.birthd} placeholder="Tua data di nascita" onChange={e => this.setState({ birthd: e.target.value })} style={{ borderRadius: "15px" }}></Form.Control>
                                </Form.Row>

                                <br />
                                <Form.Row>
                                    <Button onClick={e => this.setState({ index: 1 })} className="accedi-btn gradient-btn" variant="light" style={{ color: "white" }}>
                                        AVANTI
                                </Button>
                                </Form.Row>
                            </Form>)
                            : (<Form>
                                <Form.Row>
                                    <Form.Label for="business name" className="cardcont" style={{ marginLeft: "-10px", marginBottom: "-10px", color: "rgba(90,90,90, 1)" }}>Business name*</Form.Label>
                                    <Form.Control name="business name" className="email-input" type="text" value={this.state.bname} placeholder="Tuo business name" onChange={e => this.setState({ bname: e.target.value })} style={{ borderRadius: "15px" }}></Form.Control>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Label for="" className="cardcont" style={{ marginLeft: "-10px", marginBottom: "-10px", color: "rgba(90,90,90, 1)" }}>Partita IVA*</Form.Label>
                                    <Form.Control className="email-input" type="text" value={this.state.piva} placeholder="Tua partita IVA" onChange={e => this.setState({ piva: e.target.value })} style={{ borderRadius: "15px" }}></Form.Control>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Label for="" className="cardcont" style={{ marginLeft: "-10px", marginBottom: "-10px", color: "rgba(90,90,90, 1)" }}>Indirizzo*</Form.Label>
                                    <Form.Control className="email-input" type="text"  value={this.state.addr} placeholder="Tuo indirizzo" onChange={e => this.setState({ addr: e.target.value })} style={{ borderRadius: "15px" }}></Form.Control>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Label for="" className="cardcont" style={{ marginLeft: "-10px", marginBottom: "-10px", color: "rgba(90,90,90, 1)" }}>Numero civico*</Form.Label>
                                    <Form.Control className="email-input" type="text"  value={this.state.nciv} placeholder="Tuo numero civico" onChange={e => this.setState({ nciv: e.target.value })} style={{ borderRadius: "15px" }}></Form.Control>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Label for="email-input" className="cardcont" style={{ marginLeft: "-10px", marginBottom: "-10px", color: "rgba(90,90,90, 1)" }}>Email*</Form.Label>
                                    <Form.Control className="password-input" type="email" value={this.state.mail} placeholder="Tua Email" onChange={e => this.setState({ mail: e.target.value })} style={{ borderRadius: "15px" }}></Form.Control>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Label for="password-input" className="cardcont" style={{ marginLeft: "-10px", marginBottom: "-10px", color: "rgba(90,90,90, 1)" }}>Password*</Form.Label>
                                    <Form.Control className="password-input" type="password" value={this.state.pwd} placeholder="Tua password" onChange={e => this.setState({ pwd: e.target.value })} style={{ borderRadius: "15px" }}></Form.Control>
                                </Form.Row>
                                <br />
                                <Form.Row>
                                    <Button onClick={e => this.setState({ index: 0 })} className="accedi-btn gradient-btn" variant="light" style={{ color: "white" }}>
                                        INDIETRO
                            </Button>
                                </Form.Row>
                                <Form.Row>
                                    <Button onClick={this.sendSignup} className="accedi-btn gradient-btn" variant="light" style={{ color: "white" }}>
                                        REGISTRATI
                                    </Button>
                                </Form.Row>
                            </Form>)}
                        <div>
                        </div>
                    </Col>
                </Card>
            </div>
        )
    }
}

export default Register;
