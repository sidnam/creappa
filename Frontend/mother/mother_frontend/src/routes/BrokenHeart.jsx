import React, { Component } from "react"
import {Link} from "react-router-dom"
import '../styles/nextpay.css'
import logo from "../images/Logo/Creappa logo.svg"
import {Card, Button} from 'react-bootstrap'
import brokenImg from "../images/Illustrations/Heartbroken.svg"

class BrokenHeart extends Component{

    render(){
        return(
            <div>
                <div className="immagine-sfondo-na">
                    <img className="logo" src={logo} alt="logo"/>
                    <Card className="blocco">
                        <img className="cardimg" src={brokenImg} alt="heartbroken"/>
                        <div className="bloctitle">Oh no! Ci dispiace vederti andare via!</div>
                        <div className="cardcont">Puoi sempre ripensarci se vuoi, riattivando il tuo abbonamento dalla pagina del tuo account.</div>
                        <center>
                            <Link to={"/"}><Button className="gradient-btn" variant="light"><div className="btnTitle">ESCI</div></Button></Link>
                        </center>
                    </Card>
                </div>
            </div>
        )
    }
}
export default BrokenHeart