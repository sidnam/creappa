import React, { Component } from "react"
import NavbarAdmin from "../components/NavbarAdmin"
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Where from "../components/modifica/Where"
import Modifica from "../components/Modifica"
import Visualizza from "../components/Visualizza"
import Login from "./NewLogin"
import SignUp from "./SignUp"
import AppsManagement from "./AppManagement"
import NewApp from "./NewApp"
import HomePage from "./HomePage"
import About from "../components/modifica/About"
import Cookies from 'universal-cookie'
import Contacts from "../components/modifica/Contacts"
import Shop from "../components/modifica/Shop"
import Schedule from "../components/modifica/Schedule"
import Date from "../components/modifica/Date"
import WebPage from "../components/modifica/WebPage"
import Galleria from "../components/modifica/Galleria"

class AdminPage extends Component {
    constructor(props) {
        super(props)
        this.getComponents = this.getComponents.bind(this)
        this.caricaComponenti = this.caricaComponenti.bind(this)
        this.controllo = this.controllo.bind(this)
        this.state = {
            resources : ""
        }
    }

    componentDidMount() {
        this.getComponents()
    }

    caricaComponenti(res) {
        ReactDOM.render(
            <BrowserRouter>
                <Switch>
                    <Route exact path={"/admin"} render={() => <AdminPage />}></Route>
                    <Route exact path="/admin/modifica" component={Modifica}></Route>
                    <Route exact path="/admin/visualizza" component={Visualizza}></Route>
                    {res.components.map(function (componente, i) {
                        console.log(componente)
                        switch (componente.content.type) {
                            case "Where":
                                return <Route exact path={"/admin/modifica/Where" + this.controllo(res.data.components, componente, i)} render={() => <Where posizione={componente.content.posizione} titolo={componente.content.titolo} testo={componente.content.testo} position={componente.content.posizione} index={componente.index}/>}></Route>
                            case "About":
                                return <Route exact path={"/admin/modifica/About" + this.controllo(res.data.components, componente, i)} render={() => <About titolo={componente.content.titolo} testo={componente.content.testo} immagine={componente.content.immagine} index={componente.index} />}></Route>
                            case "Contacts":
                                return <Route exact path={"/admin/modifica/Contacts" + this.controllo(res.data.components, componente, i)} render={() => <Contacts index={componente.index} contatti={componente.content.contatti} titolo={componente.content.titolo}/>}></Route>
                            case "Schedule":
                                return <Route exact path={"/admin/modifica/Schedule" + this.controllo(res.data.components, componente, i)} render={() => <Schedule index={componente.index} orari={componente.content.orari} titolo={componente.content.titolo}/>}></Route>
                            case "Shop":
                                return <Route exact path={"/admin/modifica/Shop" + this.controllo(res.data.components, componente, i)} render={() => <Shop titolo={componente.content.titolo} testo={componente.content.testo} prodotti={componente.content.prodotti} index={componente.index}/>}></Route>
                            case "DataViewer":
                            //return <Route exact path={"/admin/modifica/DataViewer" + this.controllo(res.data.components, componente, i)} render={() => <DataViwer />}></Route>
                            case "Date":
                                console.log(componente.content)
                                return <Route exact path={"/admin/modifica/Date" + this.controllo(res.data.components, componente, i)} render={() => <Date titolo={componente.content.titolo} testo={componente.content.testo} index={componente.index}/>}></Route>
                            case "WebPage":
                                return <Route exact path={"/admin/modifica/WebPage" + this.controllo(res.data.components, componente, i)} render={() => <WebPage titolo={componente.content.titolo} testo={componente.content.testo} sito={componente.content.sito} index={componente.index}/>}></Route>
                                case "Galleria":
                                    return <Route exact path={"/admin/modifica/Galleria" + this.controllo(res.data.components, componente, i)} render={() => <Galleria titolo={componente.content.titolo} testo={componente.content.testo} sito={componente.content.sito} index={componente.index}/>}></Route>
                            }
                    }, this)}
                    <Route exact path="/Login" component={Login} />
                    <Route exact path="/signup" component={SignUp} />
                    <Route exact path="/appsmanagement" component={AppsManagement} />
                    <Route exact path="/newapp" component={NewApp} />
                    <Route exact path="/" component={HomePage} />
                </Switch>
            </BrowserRouter>,
            document.getElementById("root"))
    }

    controllo(componenti, componente, indice) {
        var somma = 0
        for (var i = 0; i < indice; i++) {
            if (componenti[i].content.type === componente.content.type)
                somma += 1
        }
        return somma
    }

    async getComponents() {
        var cookie = new Cookies()
        var bodyApp = {
            "appIndex": cookie.get("app").index,
            "subdomain": cookie.get("app").subdomain
        }
        fetch("https://api.creappa.com/getConfigs", {
            method: "POST",
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(bodyApp)
        }).then(res => res.json())
            .then(res => {
                this.caricaComponenti(res)
                this.setState({
                    resources : res.resources
                })
            }).catch((error) => {
                console.log("errore " + error)
            });
    }


    render() {
        //this.getComponents()
        return (
            <div>
                <NavbarAdmin />
                <div style={{ display: "inline-block", marginLeft: "10px", marginTop: "10px" }}>

                </div>
            </div>
        )
    }
}


export default AdminPage;