import React from "react";
import { Component } from "react";
import Newnavbar from "../components/Newnavbar";
import Footbar from "../components/Footbar";
import "../styles/new_mission.css";
import teamsvg from "../images/Illustrations/save-money.svg";
import vectorsvg from "../images/Illustrations/success-app.svg";
import top from "../images/Illustrations/2cell.png";
import {Col, Row, Accordion, Card, Button, Container} from "react-bootstrap";
import {Link} from "react-router-dom"

class Mission extends Component {
	
  	render(){ return(
		<div>
			<Newnavbar/>
			<Container fluid>
				<Row className="rectangle-106" sm={2} xs={1}>
					<Col>
						<h1 className="crea-la-tu-vita-45244">
							Vogliamo aiutare artigiani e commercianti!
						</h1>
					</Col>
					<Col>
						<img className="smartphone-drawing-2" src={top} alt="2 cell"/>
					</Col>
				</Row>
				<Row className="our-plat">
					<div className="con-la-nos-ita-487354 sofiapro-light-black-25px">
						Con la nostra piattaforma vogliamo dare la possibilità a tutti i piccoli imprenditori o liberi professionisti di creare un'app innovativa per gestire la propria attività.
					</div>
				</Row>
				<Row sm={2} xs={1}>
					<Col>
						<img className="team-svg" src={teamsvg} alt="teamsvg"/>
					</Col>
					<Col className="low-cost">
						<div className="un-team-de-esso-72812">Semplicità a basso costo</div>
						<div className="il-team-di-liqua-9250 sofiapro-light-stack-17px">
							Vogliamo offrire un sostegno alle piccole imprese, agli artigiani e ai commercianti che vogliono aumentare le proprie entrate. 
							<br/>
							Grazie all’uso di questa semplice ed economica Applicazione è possibile per chiunque comunicare con i propri clienti, informarli delle offerte last-minute, degli sconti o delle promozioni in corso e anche allestire una vetrina fotografica con le immagini del prodotto o del servizio in vendita. 
							<br/>
                            Rimanere in contatto con il cliente e fidelizzarlo aiuta a incrementare le vendite. <br/>
							Per questo abbiamo ideato questo sistema per creare App e abbiamo voluto tenerlo a basso costo, soli € 9,99 l’anno, per dare il nostro contributo alla ripresa economica.
						</div>
					</Col>
				</Row>
				<Row sm={2} xs={1} className="secondSection">
					<Col className="content-second-section">
						<div className="abbiamo-cr-genze-9752 sofiapro-regular-normal-black-30px">
							<>Insieme possiamo vincere</>
						</div>
						<div className="creappa-ti-esign-9917">
						Per raggiungere la nostra mission abbiamo creato un sistema semplicissimo per realizzare una App accattivante e utile. <br/>
                        Bastano soli 4 passaggi per realizzare una App che potrai modificare, aggiornare o migliorare in futuro in qualsiasi momento e ogni volta che vuoi. <br/>
                        L’abbiamo studiata per essere utilizzata in modo autonomo da commercianti e artigiani però, se ci fossero esigenze particolari, mettiamo a disposizione il nostro team di esperti. 
                        <br/><br/>
						Insieme possiamo tornare a crescere!   		
						</div>
					</Col>
					<Col className="vector-svg">
					</Col>
				</Row>
				<Row className="rectangle-104" sm={2} xs={1}>
					<Col>
						<div className="faq-61812 sofiapro-semi-bold-black-30px">FAQ</div>
						<div className="alcune-cos-pere-61814 sofiapro-regular-normal-gray-20px">Alcune cose che devi sapere</div>
						<Accordion defaultActiveKey="0" style={{margin:"3%"}}>
							<Card>
								<Card.Header>
									<Accordion.Toggle as={Button} variant="button" eventKey="0">
										Come iniziare ad usare Creappa?
									</Accordion.Toggle>
								</Card.Header>
								<Accordion.Collapse eventKey="0">
									<Card.Body>
										Registrati e prova la nostra piattaforma, quando la tua app ti convincerà sarai pronto a publicarla e condividerla. Avrai gratuitamente accesso alla tua app per i primi 30 giorni, dopodichè avrai bisogno di fare l’upgrade. L’abbonamento è annuale.
									</Card.Body>
								</Accordion.Collapse>
							</Card>
							<Card>
								<Card.Header>
									<Accordion.Toggle as={Button} variant="button" eventKey="1">
										Quando pago per il piano scelto?
									</Accordion.Toggle>
								</Card.Header>
								<Accordion.Collapse eventKey="1">
									<Card.Body>
										Dopo il mese di prova, se sottoscrivi una piano "Base"; al momento della sottoscrizione per quanto riguarda il piano "Business".
									</Card.Body>
								</Accordion.Collapse>
							</Card>
							<Card>
								<Card.Header>
									<Accordion.Toggle as={Button} variant="button" eventKey="2">
										Quando e come posso cancellarmi?
									</Accordion.Toggle>
								</Card.Header>
								<Accordion.Collapse eventKey="2">
									<Card.Body>
										Puoi cancellarti in qualsiasi momento, fai ATTENZIONE però, cancellare l'abbonamento avrà effetto immediato. 
									</Card.Body>
								</Accordion.Collapse>
							</Card>
							<Card>
								<Card.Header>
									<Accordion.Toggle as={Button} variant="button" eventKey="3">
										Cosa succede se non rinnovo dopo il mese di prova?
									</Accordion.Toggle>
								</Card.Header>
								<Accordion.Collapse eventKey="3">
									<Card.Body>
										L'app non sarà più modificabile nè sarà possibile gestirla. Inoltre dopo un certo periodo non sarà più disponibile neanche ai clienti.
									</Card.Body>
								</Accordion.Collapse>
							</Card>
						</Accordion>
					</Col>
					<Col style={{paddingTop:"10%"}} className="text-accordion">
						<div className="hai-ancora-ande-48745 sofiapro-regular-normal-black-30px">Hai Ancora Domande?</div>
						<p className="non-hai-tr-menti-7084">
							<>
							Non hai trovato tutto quello che stavi cercando? Nessun problema! Sentiti libero di scrivere al team di supporto...
							<br/>Contattaci per qualsiasi informazione riguardo la piattaforma, le apps create con Creappa, abbonamenti o pagamenti.
							</>
						</p>
						<Link to={"/contactus"}>
							<Button className="gradient-btn btn-contact" variant="light" style={{padding: "15px 40px", borderRadius: "12px", marginBottom: "5%"}}>
							<div  className='btnTitle contact_us' style={{fontWeight: "bold"}}>
								CONTATTACI!
							</div>
							</Button>
						</Link>
					</Col>
				</Row>
			</Container>
			<Footbar/>
		</div>
	);};
}

export default Mission;




