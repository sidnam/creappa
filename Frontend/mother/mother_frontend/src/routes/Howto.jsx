import React, { Component } from "react"
import "../styles/howto.css"
import img from "../images/old_img/time.png"
import Newnavbar from "../components/Newnavbar";
import {Link} from 'react-router-dom';
import {Row, Col, Container, Button} from 'react-bootstrap';
import top from "../images/Illustrations/hero-img.svg";
import vectors from "../images/Background/modules-img.svg";
import templateIcon from "../images/Svg_/registrati-icon.svg";
import compIcon from "../images/Svg_/choose-logo-icon.svg";
import shareIcon from "../images/Svg_/share.svg";
import previewIcon from "../images/Svg_/template.svg";
import Footbar from "../components/Footbar";
import style from "../images/Background/put-your-style.svg";
import qr from "../images/Svg_/qr-code.svg";


class Howto extends Component {
    render() {
        return (
            <Container fluid>
                <Newnavbar/>
                <Row className="sfondo-svg" sm={2} xs={1}>
                    <Col>
						<h1 className="title">
							Pronto a creare<br>
                            </br> la tua APP?
						</h1>
					</Col>
                    <Col>
						<img className="smartphone-drawing-2" src={top} alt="3 cell"/>
					</Col>
                </Row>
                <Row>
					<div className="mainP sofiapro-light-black-25px">
                        <b style={{fontWeight: "600"}}>Creappa ti aiuterà a portare le tue idee alla realtà!</b><br/><br/>
                    </div>
				</Row>
                <Row sm={2} xs={1}>
                    <Col className="custom-app-img">
                        <img className="modules-svg" src={vectors} alt="vectors"/>
					</Col>
					<Col className="custom-app">
						<div className="cont-title">Noi ti diamo gli strumenti</div>
						<div className="cont-par sofiapro-light-stack-17px">
							<>Ti mettiamo a disposizione una piattaforma web da utilizzare tramite PC o smartphone. <br></br>
                              In qualsiasi momento puoi accedere per modificare forma e contenuti. <br></br>
                              Per esempio aggiungere foto, inviare offerte o modificare le funzioni della tua APP.</>
						</div>
					</Col>
				</Row>
				<Row sm={2} xs={1} className="section-qr">
					<Col className="create-template">
						<div className="cont-title">
							<>Tu mettici il tuo stile!</>
						</div>
						<div className="cont-par sofiapro-light-stack-17px">
                        -  Registrati <br/>
                        -  Scegli nome e logo<br/>
                        -  Scegli il template<br/>
                        -  Condividi con i tuoi clienti
                        </div>

                        <div className="qr-box sofiapro-light-stack-17px">
                          <img className="qr-code" src={qr} alt="vectors"/>

                          <div className="cont-title-qr">
                          <bold>Condividi con link o codice QR</bold>
						  </div>
                          <div className="cont-par sofiapro-light-stack-17px">  
                          <>La piattaforma creerà un link, <br/> 
                          da condividere con i tuoi clienti, <br/> 
                          e un codice QR da cui è possibile scaricare l'APP!</>
                          </div>
                        </div>
					</Col>

				    <Col className="create-template-img">
                        <img className="vectors-svg" src={style} alt="vectors"/>
					</Col>
				</Row>
                <Row className="greyarea justify-content-md-center">
                    <center>
                        <div className="end-title">
							<>Bastano 4 passaggi semplici e veloci</>
						</div>
					
                        <Row className="justify-content-md-center" sm={4} xs={2}>
                            <Col>
                                <img className="icon" src={templateIcon} alt="vectors"/>
                                <div className="details">
                                Registrati
                                </div>
                            </Col>
                            <Col>
                                <img className="icon" src={compIcon} alt="vectors"/>
                                <div className="details">
                                Scegli nome e logo 
                                </div>
                            </Col>
                            <Col>
                                <img className="icon" src={previewIcon} alt="vectors"/>
                                <div className="details">
                                Scegli il template
                                </div>
                            </Col>
                            <Col>
                                <img className="icon" src={shareIcon} alt="vectors"/>
                                <div className="details">
                                Condivi coi tuoi clienti
                                </div>
                            </Col>
                        </Row>
                        <Row className="justify-content-md-center">
                        <Link to={"/signup"} style={{display:"block", marginLeft:"auto",marginRight:"auto", marginBottom:"5%"}}>
                                    <Button className="gradient-btn try" variant="light">
                                        <div className="btnTitle">PROVA SUBITO!</div>
                                    </Button>
                                </Link>
                        </Row>
                    </center>
                </Row>


                
                <Footbar/>
            </Container>
        )
    }
}
export default Howto
