import React, { Component } from "react"
import {Link} from "react-router-dom"
import '../styles/contactus.css'
import {Card, Row, Col} from 'react-bootstrap'
import Newnavbar from "../components/Newnavbar"
import phoneIcon from "../images/Svg_/telephone.svg"
import mailIcon from "../images/Svg_/support.svg"
import mapIcon from "../images/Svg_/location.svg"

class PaymentPage extends Component{

    render(){
        return(
            <div>
                <div className="immagine-sfondo-cu">
                    <Newnavbar/>
                    <Card className="card-cu">
                        <center>
                            <Row className="justify-content-sm-center contact-team-title">
                                <div className="cu-title">
                                    Contatta il team di Creappa
                                </div>
                            </Row>
                            <Row className="justify-content-sm-center">
                                <div className="cu-par">
                                    Hai bisogno di maggiori informazioni? Bug da sistemare? Chiedici o riportaci qualsiasi problema!<br/> Grazie per il tuo aiuto!
                                </div>
                            </Row>
                            <Row className="justify-content-sm-center contact-content" sm={4} xs={2}>
                                <Col className="tel">
                                    <img className="icon tel" src={phoneIcon} alt="phone"/>
                                    <div className="cu-par">
                                        <a href="tel:+393471580503">+39 347 158 0503</a>
                                    </div>
                                </Col>
                                <Col className="mail">
                                    <img className="icon" src={mailIcon} alt="mail"/>
                                    <a className="cu-par" href="mailto:team@paltolab.com">
                                        team@paltolab.com
                                    </a>
                                </Col>
                                <Col className="address">
                                    <img className="icon" src={mapIcon} alt="map"/><br/>
                                    <a className="cu-par" href="https://www.google.com/maps/place/Via+Carlo+Arturo+Jemolo,+83,+00156+Roma+RM/@41.947677,12.5857154,17z/data=!3m1!4b1!4m5!3m4!1s0x132f649032aaea97:0x93d230b142c0a333!8m2!3d41.947673!4d12.5879041">Via Carlo Arturo Jemolo 83, Roma</a>
                                </Col>
                            </Row>
                        </center>
                    </Card>
                </div>
            </div>
        )
    }
}
export default PaymentPage



