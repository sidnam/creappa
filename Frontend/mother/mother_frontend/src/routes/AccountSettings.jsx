import React, { Component } from "react"
import NavbarAdminAS from "../components/NavbarAdminAS";
import "../styles/accountsettings.css";
import 'bootstrap/dist/css/bootstrap.css';
import {Button, Form, Col, Row, Tab, Tabs, Container} from 'react-bootstrap';
import Cookie from 'universal-cookie'
import { GetApp } from "@material-ui/icons";
import qs from 'qs'
import {Link} from "react-router-dom"
import Popup from "reactjs-popup";
import PayPalBtn from "../components/PayPalBtn";
import { Card } from "@material-ui/core";

class AccountSettings extends Component {

    constructor(){
        super();
        this.emailInput=React.createRef();
        this.phoneInput=React.createRef();
        this.editEmail = this.editEmail.bind(this);
        this.editPhoneN= this.editPhoneN.bind(this);
        this.state={
            plan:"",
            paypal:false,
            isSusp:false
        }
    }

    async componentDidMount(){
        const axios = require('axios').default;
        var account=new Cookie();
        var bodyjson={
            email:account.get("account").email,
            password:sessionStorage.getItem("pwd")
        }
        const resp = await axios.post('https://api.creappa.com/getAccountInfo',bodyjson);
        if(resp.data.status==="successful"){
            this.name=resp.data.info.name;
            this.surname=resp.data.info.surname;
            this.cf=resp.data.info.codiceFiscale;
            this.birthday=new Date(resp.data.info.birthday).toISOString().split("T")[0];
            this.email=resp.data.info.email;
            this.phoneN=resp.data.info.phoneNumber;
            this.payment=resp.data.info.payment;
            this.state.plan=resp.data.info.plan;
            var token=await axios({
                method:"POST",
                url:'https://api.sandbox.paypal.com/v1/oauth2/token',
                auth:{
                    username:'ARsQyOh4CzmSbX1dPH61I4fJ1FePhjoKrxlr890-qKletuKWRJS1HLEao8MdQeaLgebDcDITK5kSxC92',
                    password:'EKuK8uW2tnHHJrBXBsryZGAuSToCSjeRkQnPfUr1ayLawr91UXernox4DVy3piDr5lB4_i241k1rZu_V'
                },
                data:qs.stringify({
                    grant_type:"client_credentials"
                }),
                headers:{
                    'Access-Control-Allow-Origin':'*',
                    'Access-Control-Allow-Credentials':true,
                    'Access-Control-Allow-Methods': 'POST',
                    'Access-Control-Allow-Headers':'Authorization,Content-Type,Accept',
                    'Content-Type':'application/x-www-form-urlencoded'
                }
            })
            var reqHeader = {
                headers:{
                    'Content-Type':'application/json',
                    Authorization:'Bearer '+ token.data.access_token
                }
            }
            axios.get('https://api.sandbox.paypal.com/v1/billing/subscriptions/' + resp.data.info.payment,reqHeader).then((sub)=>{
                if(sub.data.status!=='ACTIVE'){
                    this.setState({
                        isSusp:true
                    })     
                }
            });
            this.forceUpdate();
        } else {
            alert("Il recupero delle informazioni ha causato qualche problema.")
        }
    }

    async editEmail(){
        const axios = require('axios').default;
        var email=this.emailInput.current.value;
        var account=new Cookie();
        var bodyjson={
            email:account.get("account").email,
            password:sessionStorage.getItem("pwd"),
            fieldToEdit:"email",
            newValue:email
        }
        const resp = await axios.post('https://api.creappa.com/editAccountField',bodyjson);
        if(resp.data.status==="successful"){
            this.email=email;
            account.set("account",{ email: this.email });
            alert("Nuova Email: "+email);
            this.forceUpdate();
        } else {
            alert("Qualcosa è andato storto. Controlla che la nuova email: "+email+" sia scritta nel formato corretto.")
            window.location.pathname="/appsmanagement"
        }
    }

    async editPhoneN(){
        const axios = require('axios').default;
        var phoneN=this.phoneInput.current.value;
        var account=new Cookie();
        var bodyjson={
            email:account.get("account").email,
            password:sessionStorage.getItem("pwd"),
            fieldToEdit:"phoneNumber",
            newValue:phoneN
        }
        const resp = await axios.post('https://api.creappa.com/editAccountField',bodyjson);
        if(resp.data.status==="successful"){
            this.phoneN=phoneN;
            alert("Nuovo numero di telefono: "+phoneN);
            this.forceUpdate();
        } else {
            alert("Qualcosa è andato storto. Controlla che il nuovo numero: "+phoneN+" sia scritto nel formato corretto.")
            window.location.pathname="/appsmanagement"
        }
    }

    async cancelSub(){
        const axios = require('axios').default;
        var token=await axios({
            method:"POST",
            url:'https://api.sandbox.paypal.com/v1/oauth2/token',
            auth:{
                username:'ARsQyOh4CzmSbX1dPH61I4fJ1FePhjoKrxlr890-qKletuKWRJS1HLEao8MdQeaLgebDcDITK5kSxC92',
                password:'EKuK8uW2tnHHJrBXBsryZGAuSToCSjeRkQnPfUr1ayLawr91UXernox4DVy3piDr5lB4_i241k1rZu_V'
            },
            data:qs.stringify({
                grant_type:"client_credentials"
            }),
            headers:{
                'Access-Control-Allow-Origin':'*',
                'Access-Control-Allow-Credentials':true,
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Headers':'Authorization,Content-Type,Accept',
                'Content-Type':'application/x-www-form-urlencoded'
            }
        })
        var reqHeader={
            headers:{
                'Content-Type':'application/json',
                Authorization:'Bearer '+ token.data.access_token
            }
        }
        var bodyjson={
            reason:"_"
        }
        const sub=axios.post('https://api.sandbox.paypal.com/v1/billing/subscriptions/'+this.payment+'/suspend',bodyjson,reqHeader)
            .then((resp)=>{
                alert("Il tuo abbonamento è stato sospeso.")
                window.location.pathname="/brokenheart"
            }).catch((error)=>{
                alert(error);
                this.forceUpdate();
                return;
            });
    }

    async activateSub(){
        const axios = require('axios').default;
        var token=await axios({
            method:"POST",
            url:'https://api.sandbox.paypal.com/v1/oauth2/token',
            auth:{
                username:'ARsQyOh4CzmSbX1dPH61I4fJ1FePhjoKrxlr890-qKletuKWRJS1HLEao8MdQeaLgebDcDITK5kSxC92',
                password:'EKuK8uW2tnHHJrBXBsryZGAuSToCSjeRkQnPfUr1ayLawr91UXernox4DVy3piDr5lB4_i241k1rZu_V'
            },
            data:qs.stringify({
                grant_type:"client_credentials"
            }),
            headers:{
                'Access-Control-Allow-Origin':'*',
                'Access-Control-Allow-Credentials':true,
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Headers':'Authorization,Content-Type,Accept',
                'Content-Type':'application/x-www-form-urlencoded'
            }
        })
        var reqHeader={
            headers:{
                'Content-Type':'application/json',
                Authorization:'Bearer '+ token.data.access_token
            }
        }
        var bodyjson={
            reason:"_"
        }
        axios.post('https://api.sandbox.paypal.com/v1/billing/subscriptions/'+this.payment+'/activate',bodyjson,reqHeader)
            .then((resp)=>{
                alert("Il tuo abbonamento è stato riattivato.")
                window.location.pathname="/appsmanagement"
            }).catch((error)=>{
                alert(error);
                this.forceUpdate();
                return;
            });
    }

    paypalShow(){
        this.setState({
            paypal: true
        })
    }

    render(){
        return (
            <div className="sfondoas">
                <NavbarAdminAS/>
                <Container className="tab-container">
                    <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example">
                        <Tab eventKey="profile" title="Account">
                            <Form className="settings-container">
                                <div className="margini">
                                    <Form.Row className="bottomspace">
                                        <Col>
                                            <Form.Label>Nome</Form.Label>
                                            <Form.Control type="text" value={this.name} disabled/>
                                        </Col>
                                        <Col>
                                            <Form.Label>Cognome</Form.Label>
                                            <Form.Control type="text" value={this.surname} disabled/>
                                        </Col>
                                    </Form.Row>
                                    <Form.Row className="bottomspace">
                                        <Col>
                                            <Form.Label>Codice Fiscale</Form.Label>
                                            <Form.Control type="text" value={this.cf} disabled/>
                                        </Col>
                                        <Col>
                                            <Form.Label>Data di Nascita</Form.Label>
                                            <Form.Control type="date" value={this.birthday} disabled/>
                                        </Col>
                                    </Form.Row>
                                    <Form.Label>Email</Form.Label>
                                    <Form.Row className="bottomspace">
                                        <Col>
                                            <Form.Control type="email" placeholder={this.email} ref={this.emailInput}/>
                                        </Col>
                                        <Col>
                                            <Button variant="outline-light" className="gradient-btn" onClick={this.editEmail.bind(this)}>Modifica</Button>
                                        </Col>
                                    </Form.Row>
                                    <Form.Label>Numero di Telefono</Form.Label>
                                    <Form.Row className="bottomspace">
                                        <Col>
                                            <Form.Control type="number" placeholder={this.phoneN} ref={this.phoneInput}/>
                                        </Col>
                                        <Col>
                                            <Button variant="outline-light" className="gradient-btn" onClick={this.editPhoneN.bind(this)}>Modifica</Button>
                                        </Col>
                                    </Form.Row>
                                </div>
                            </Form>
                        </Tab>
                        <Tab eventKey="payment" title="Gestisci Piano">
                            <Form>
                                <div className="margini">
                                    <Form.Label>Payment</Form.Label>
                                    <Row>
                                        <Col>
                                            <Form.Control type="text" value={this.payment} disabled/>
                                        </Col>
                                        <Col>
                                            {
                                                this.payment==="0" ?
                                                    <div>
                                                        <div>Non hai ancora un abbonamento, fallo subito!</div>
                                                        <div className="paypal-btn"><PayPalBtn type={"0"}/></div>
                                                    </div>
                                                    
                                                : this.state.isSusp ?
                                                    <Row>
                                                        <Button variant="success" onClick={()=>this.activateSub()} style={{marginRight:'2%', maxHeight:"38px"}}>Riattiva l'abbonamento</Button>
                                                    {
                                                        this.state.plan ==="0" ?
                                                        <div>
                                                            <Popup trigger={
                                                                <Button variant="outline-light" className="gradient-btn" onClick={()=>this.paypalShow()}>Upgrade!</Button>
                                                            }
                                                                on={['hover', 'focus']}
                                                                position='top center'
                                                            >
                                                                <span>Passa subito al piano di abbonamento "Business"!</span>
                                                            </Popup>
                                                            {
                                                                this.state.paypal ?
                                                                <div className="paypal-btn" style={{marginTop:"5%"}}><PayPalBtn type={"1"}/></div> :
                                                                <div/>
                                                            }
                                                        </div> 
                                                        :
                                                        <div>
                                                            <Popup trigger={
                                                                <Button variant="outline-light" className="gradient-btn" onClick={()=>this.paypalShow()}>Downgrade!</Button>
                                                                }
                                                                on={['hover', 'focus']}
                                                                position='top center'
                                                            >
                                                                <span>Conto troppo salato? Passa all'abbonamento "Base"</span>
                                                            </Popup>
                                                            {
                                                                this.state.paypal ?
                                                                <div className="paypal-btn" style={{marginTop:"5%"}}><PayPalBtn type={"0"}/></div> :
                                                                <div/>
                                                            }
                                                        </div>
                                                    }
                                                    </Row>
                                                : this.state.plan ==="0" ?
                                                    <div>
                                                        <Button variant="outline-danger" onClick={this.cancelSub.bind(this)} style={{marginRight:'2%'}}>Sospendi l'abbonamento</Button>
                                                    </div> 
                                                : 
                                                    <div>
                                                        <Button variant="outline-danger" onClick={this.cancelSub.bind(this)} style={{marginRight:'2%'}}>Sospendi l'abbonamento</Button>
                                                    </div> 
                                            }
                                        </Col>
                                    </Row>
                                </div>
                            </Form>
                        </Tab>
                    </Tabs>
                </Container>
            </div>
        )
    }
}
export default AccountSettings