import React, {Component} from "react";
import logo from "../images/Logo/Creappa logo.svg"
import googleIcon from "../images/Logo/google-icon@2x.svg"
import "../styles/nextpay.css"
import { Link } from "react-router-dom"
import Cookies from 'universal-cookie'
import {Col, Row, Form, Button, Card} from "react-bootstrap"

class Login extends Component {

  constructor() {
    super();
    this.userInput = React.createRef();
    this.passInput = React.createRef();
  }

  async sendLogin(){
      const cookie = new Cookies();
      const axios=require('axios').default;
      var un=this.userInput.current.value;
      var pwd=this.passInput.current.value;
      console.log(un)
      const resp = await axios.post(
          'https://api.creappa.com/login',
          { email: un, password: pwd },
          { headers: { 'Content-Type': 'application/json' } }
      )
      if(resp.data.status === "successful"){
          sessionStorage.setItem("pwd",pwd);
          cookie.set('account', {
              email: un
          }, { path : "/"})
          window.location.pathname="/appsmanagement"
      } else {
          alert("Login Errato: " + resp.data.status)
      }
  }

  render() {
    return (
      <div className="immagine-sfondo-na">
        <Link to="/" className="logo"><img className="logo" src={logo} alt="logo"/></Link>
        <Card className="blocco">
          <Col className="content-login">
            <p className="bloctitle"> ACCEDI </p>
            <br/>
            <Form className="login-form">
              <Form.Row>
                <Form.Label for="email-input" className="cardcont" style={{fontFamily:"Sofia Pro-SemiBold, Helvetica, Arial, serif", marginLeft:"-10px"}}>Indirizzo Email</Form.Label>
                <Form.Control className="email-input" type="email" placeholder="Tua email" style={{borderRadius:"10px"}} ref={this.userInput}></Form.Control>
              </Form.Row>
              <Form.Row>
                <Form.Label for="password-input" className="cardcont" style={{fontFamily:"Sofia Pro-SemiBold, Helvetica, Arial, serif", marginLeft:"-10px"}}>Password</Form.Label>
                <Form.Control className="password-input " type="password" placeholder="Tua password" style={{borderRadius:"10px"}} ref={this.passInput}></Form.Control>
              </Form.Row>
              <br/>
              <Form.Row>
                <Button onClick={this.sendLogin.bind(this)} className="accedi-btn gradient-btn"  variant="light" style={{color:"white"}}>
                  ACCEDI
                </Button>
              </Form.Row>        
            </Form>
            <div className="lost-psw">
              <Link to="/passwordrec">
                <div style={{fontSize:"12px", fontFamily:"Sofia Pro-SemiBold, Helvetica, Arial, serif"}} className="password-d-ticata-765 sofiapro-semi-bold-fuchsia-blue-13px border-class-1">
                PASSWORD DIMENTICATA?
                </div>
              </Link>
              <p className="cardcont" style={{ fontWeight:"bold", textAlign:"start"}}>
                Non hai l'account? 
                <Link to="/signup" className="scritta-ghey" style={{borderWidth:"0px",  fontSize:"13px", marginRight:"20%"}}>
                  REGISTRATI
                </Link>
              </p>
            </div>
          </Col>
        </Card>
    </div>
    )
  }
}

export default Login;
