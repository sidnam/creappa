import React, { Component } from "react"
import {Link} from "react-router-dom"
import '../styles/nextpay.css'
import logo from "../images/Logo/Creappa logo.svg"
import {Card, Button} from 'react-bootstrap'
import goodpayImg from "../images/Illustrations/thank you italian.svg"

class GoodPay extends Component{

    render(){
        return(
            <div>
                <div className="immagine-sfondo-na">
                    <img className="logo" src={logo} alt="logo"/>
                    <Card className="blocco">
                        <img className="cardimg" src={goodpayImg} alt="good pay"/>
                        <div className="bloctitle">Pagamento avvenuto con successo!</div>
                        <div className="cardcont">Grazie per il pagamento!<br/>Ora potrai usufruire del potenziale di Creappa al massimo!</div>
                        <center><Link to={"/appsmanagement"}><Button className="gradient-btn" variant="light"><div className="btnTitle">Vai alla dashboard!</div></Button></Link></center>
                    </Card>
                </div>
            </div>
        )
    }
}
export default GoodPay