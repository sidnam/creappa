import React, { Component } from "react"
import "../styles/homepage.css"
import {Button, Container, Row, Col} from "react-bootstrap"
import Newnavbar from "../components/Newnavbar";
import Footbar from "../components/Footbar";
import firstImg from "../images/Illustrations/Hero illustration.svg"
import secondImg from "../images/Illustrations/Mobile login.svg"
import thirdImg from "../images/Illustrations/Build app.svg"
import firstIcon from "../images/Svg_/Vector.svg"
import secondIcon from "../images/Svg_/Custom build.svg"
import thirdIcon from "../images/Svg_/custom features.svg"
import fourthIcon from "../images/Svg_/UX.svg"
import fifthIcon from "../images/Svg_/Builder.svg"
import sixthIcon from "../images/Svg_/preview.svg"
import threePhone from "../images/Illustrations/smartphone.png"
import {Link} from 'react-router-dom'
import templateIcon from "../images/Svg_/registrati-icon.svg";
import compIcon from "../images/Svg_/choose-logo-icon.svg";
import shareIcon from "../images/Svg_/share.svg";
import previewIcon from "../images/Svg_/template.svg";


class HomePage extends Component {
    render() {
        return (
            <div>
                <Newnavbar/>
                <Container fluid>
                    <Row sm={2} xs={1}>
                        <Col className="hero-text">
                            <div className="firsttitle" >La tua APP Semplice ed Economica!</div>
                            <div className="firstpar" style={{lineHeight: "1.6"}}>Aumenta le vendite con la tua APP.<br></br>Realizzala in 4 semplici passaggi a soli 9,90€ l'anno.</div>
                            <Link to={"/login"}>
                                <Button className="start-btn" variant="light">
                                    <div className="btnTitle" style={{fontWeight:"bold"}}>INIZIA SUBITO!</div>
                                </Button>
                            </Link>
                        </Col>
                        <Col>
                            <img className="first-img" src={firstImg} alt="hero illustration"></img>
                        </Col>
                        <Col>
                        <div className="days">
                                *Prova Creappa <bold>GRATIS per 30 giorni.</bold><br></br>
                                Carta di credito non richiesta.
                        </div>
                        </Col>
                    </Row>
                    <Row className="justify-content-sm-center help">
                        <div className="middletitle app title-home">L'APP che ti aiuta ad incrementare le vendite</div>
                    </Row>
                    <Row sm={2} xs={1} style={{marginBottom:"5%"}}>
                        <Col className="section2">
                            <Row className="informa">
                                <Col sm={2} xs={2}>
                                    <img src={fourthIcon} alt="fourth icon"></img>
                                </Col>
                                <Col sm={10} xs={10}>
                                    <div className="conttitle">Informa</div>
                                    <div className="contpar">Comunica novità, eventi o aperture straordinarie.</div>
                                </Col>
                            </Row>
                            <Row className="affascina">
                                <Col sm={2} xs={2}>
                                    <img src={fifthIcon} alt="fifth icon"></img>
                                </Col>
                                <Col sm={10} xs={10}>
                                    <div className="conttitle">Affascina</div>
                                    <div className="contpar">Crea una vetrina virtualecon immagini e prezi dei tuoi prodotti o servizi.</div>
                                </Col>
                            </Row>
                            <Row className="attiva-clienti">
                                <Col sm={2} xs={2}>
                                    <img src={sixthIcon} alt="sixth icon"></img>
                                </Col>
                                <Col sm={10} xs={10}>
                                    <div className="conttitle">Attiva i tuoi clienti</div>
                                    <div className="contpar">Seducili con promozioni ed occasioni last minute.</div>
                                </Col>
                            </Row>
                        </Col>
                        <Col>
                            <img className="first-img" src={thirdImg} alt="build app"></img>
                        </Col>
                    </Row>
                </Container>
                <div className="sfondo-vector">

                    <Row className="justify-content-sm-center flex-direction-column help">
                        <div className="middletitle create-app">Crea e personalizza la tua <br>
                        </br>APP a soli 9,90 all'anno!</div>
                        <img className="last-img" src={threePhone} alt="smartphone"></img>
                    </Row>
                
                </div>
                <Container fluid>
                    <Row className="greyarea justify-content-md-center steps">
                        <center style={{width: "70vw"}}>
                            <div className="end-title">
                                <>Bastano 4 passaggi semplici e veloci</>
                            </div>
                            <Row className="justify-content-md-center" sm={4} xs={2}>
                                <Col>
                                    <img className="icon" src={templateIcon} alt="vectors"/>
                                    <div className="details">
                                        Registrati
                                    </div>
                                </Col>
                                <Col>
                                    <img className="icon" src={compIcon} alt="vectors"/>
                                    <div className="details">
                                        Scegli nome e logo 
                                    </div>
                                </Col>
                                <Col>
                                    <img className="icon" src={previewIcon} alt="vectors"/>
                                    <div className="details">
                                        Scegli il template
                                    </div>
                                </Col>
                                <Col>
                                    <img className="icon" src={shareIcon} alt="vectors"/>
                                    <div className="details">
                                        Condivi coi tuoi clienti
                                    </div>
                                </Col>
                            </Row>
                            <Row className="justify-content-sm-center">
                                <Link to={"/signup"} style={{display:"block", marginLeft:"auto",marginRight:"auto", marginBottom:"5%"}}>
                                    <Button className="gradient-btn try" variant="light">
                                        <div className="btnTitle">PROVA SUBITO!</div>
                                    </Button>
                                </Link>
                            </Row>
                        </center>
                    </Row>
                </Container>
                <Footbar/>
            </div>
        )
    }
}

export default HomePage