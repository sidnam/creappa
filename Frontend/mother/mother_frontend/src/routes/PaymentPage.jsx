import React, { Component } from "react"
import PayPalBtn from "../components/PayPalBtn"
import {Link} from "react-router-dom"
import '../styles/pp.css'
import logo from "../images/Logo/Creappa logo.svg"
import {Card} from 'react-bootstrap'

class PaymentPage extends Component{

    render(){
        return(
            <div>
                <div className="immagine-sfondo-na">
                    <img className="logo" src={logo} alt="logo"/>
                    <Card className="card-paypal-btn">
                        <div className="cardtitle">METODO DI PAGAMENTO</div>
                        <PayPalBtn type={"0"}/>
                        <Link to="/appsmanagement">Salta il pagamento</Link><br/>
                        <div className="cardadv">(ATTENZIONE! Senza sottoscrivere un abbonamento non potrai iniziare a creare la tua prima app. Ricordati che sottoscrivendo un abbonamento di tipo "Base" avrai diritto a un mese di prova e che comunque potrai cancellare la tua sottoscrizione in qualsiasi momento.)</div>
                    </Card>
                </div>
            </div>
            
        )
    }
}
export default PaymentPage