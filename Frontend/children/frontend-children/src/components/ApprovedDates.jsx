import React, { Component } from "react"
import { Spring } from "react-spring/renderprops"
import Cookies from "universal-cookie"

class ApprovedDates extends Component {
    constructor(props) {
        super(props)
        this.state = {
            appuntamenti: []
        }
        this.aggiungiAppuntamenti = this.aggiungiAppuntamenti.bind(this)
        this.ritornaCarta = this.ritornaCarta.bind(this)
    }

    componentDidMount() {
        this.aggiungiAppuntamenti()
    }

    ritornaCarta(val){
        if(val == 0){
            return(<p style={{ color: "orange" }}>In sospeso</p>)
        }else if(val == 1){
            return(<p style={{ color: "green" }}>approvato</p>)
        }else if(val == -1){
            return(<p style={{ color: "red" }}>Non approvato</p>)
        }
    }

    async aggiungiAppuntamenti() {
        const appInfo = require('../files/info.json')
        var cookie = new Cookies()
        const axios = require('axios').default
        try {
            const response = await axios.post(
                'https://api.creappa.com/getDates',
                {
                    "email": cookie.get('account').email,
                    "password": cookie.get("account").pswd,
                    "appIndex": parseInt(appInfo.appIndex),
                },
                {
                    'Content-Type': 'Application/json'
                }
            )

            this.setState({ appuntamenti: response.data.dates })
        } catch (errore) {

        }        
    }

    render() {
        return (
            <div>
                <Spring from={{ opacity: 0 }} to={{ opacity: 1 }}>
                    {props => (
                        <div style={props}>
                            <div className="spaziatura-top-navbar"></div>
                            <div className="contenitore-immagine" style={{ backgroundImage: "url(" + this.props.background + ")" }}></div>
                            <h1 className={"titolo-about blurred spaziatura-about"}>Appuntamenti fatti</h1>

                        </div>
                    )}
                </Spring>

                { this.state.appuntamenti.map(appuntamento =>
                    <div>
                        <div class="card text-center" style={{ marginTop: "20px", width: "90%", maxWidth: "400px", marginLeft: "20px", marginRight: "5%" }}>
                            <div class="card-body">
                                <h5 class="card-title">{appuntamento.date}</h5>
                                <p class="card-text">{appuntamento.description}</p>
                                {
                                   this.ritornaCarta(appuntamento.approved)
                                }
                            </div>
                        </div>
                    </div>
                )}

            </div>)
    }
}

export default ApprovedDates;