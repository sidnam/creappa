import React, { Component } from "react"
import { Spring } from "react-spring/renderprops"

class Contacts extends Component {
    render() {
        return (
            <div>
                <div style={{marginTop: "100px"}}></div>
                <Spring from={{ opacity: 0 }} to={{ opacity: 1 }}>
                    {props => (
                        <div style={props}>
                            <h1 style={{color: this.props.coloreTesto, textAlign:"center",padding: "20px", backgroundColor:this.props.colore, marginLeft:"5%", marginRight:"5%"}}>{this.props.titolo}</h1>
                        </div>
                    )}
                </Spring>
                <Spring from={{ opacity: 0, marginLeft: -500}} to={{ opacity: 1, marginLeft: 0 }}>
                    {props => (
                        <div style={props}>
                            <ul style={{color: this.props.coloreTesto, textAlign:"center",padding: "20px", backgroundColor:this.props.colore, marginLeft:"5%", marginRight:"5%", color:"white"}}>
                                {this.props.contatti.map(contatto => (
                                    <li className="contatti-item" style={{color:this.props.coloreTesto}}> {contatto.nome} : <strong>{contatto.testo}</strong></li>
                                ))}
                            </ul>
                        </div>
                    )}
                </Spring>
            </div>
        )
    }
}

export default Contacts;