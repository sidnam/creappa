import React, { Component } from "react"
import { Spring } from "react-spring/renderprops"

class About extends Component {
    render() {
        return (
            <div>
                <Spring from={{ opacity: 0 }} to={{ opacity: 1 }}>
                    {props => (
                        <div style={props}>
                            <div style={{marginTop: "100px"}}></div>
                            <div style={{textAlign:"center",padding: "20px", backgroundColor:this.props.colore, marginLeft:"5%", marginRight:"5%"}}>
                                <h1 className={"titolo-about"} style={{color:this.props.coloreTesto}}>{this.props.titolo}</h1>
                                <p className={"testo-about"} style={{color:this.props.coloreTesto}}>{this.props.testo}</p>
                            </div>
                        </div>
                    )}
                </Spring>
                <Spring from={{ opacity: 0, marginLeft: -500 }} to={{ opacity: 1, marginLeft: 0}}>
                    {props => (
                        <div style={props}>
                            <img className={"immagine-about"}  src={"https://creappa.s3.amazonaws.com/resources/resources/background-template/sfondo.jpg"} />
                        </div>
                    )}
                </Spring>
            </div>

        )
    }
}

export default About;