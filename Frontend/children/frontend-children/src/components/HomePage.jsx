import React, { Component } from "react"
import { Spring } from "react-spring/renderprops"
import { Link } from "react-router-dom"

import ShoppingCartRoundedIcon from '@material-ui/icons/ShoppingCartRounded';
import ScheduleRoundedIcon from '@material-ui/icons/ScheduleRounded';
import CalendarTodayRoundedIcon from '@material-ui/icons/CalendarTodayRounded';
import MailRoundedIcon from '@material-ui/icons/MailRounded';
import LanguageRoundedIcon from '@material-ui/icons/LanguageRounded';
import ContactPhoneRoundedIcon from '@material-ui/icons/ContactPhoneRounded';
import StorefrontRoundedIcon from '@material-ui/icons/StorefrontRounded';
import AssessmentRoundedIcon from '@material-ui/icons/AssessmentRounded';
import PhotoLibraryIcon from '@material-ui/icons/PhotoLibrary';
import RoomIcon from '@material-ui/icons/Room';


import { } from "@material-ui/icons";

class HomePage extends Component {

    constructor(props) {
        super(props)
        this.controllo = this.controllo.bind(this)
        this.showSettings = this.showSettings.bind(this);
    }

    showSettings (event) {
        event.preventDefault();
    }

    render() {
        var colors = this.props.colors
        return (
            <div>
                
                <div className="spaziatura-top-navbar"></div>
                <div className="contenitore-immagine" style={{ backgroundImage: "url(" + this.props.background + ")" }}></div>
                <Spring from={{ opacity: 0, marginLeft: -500 }} to={{ opacity: 1, marginLeft: 0 }}>
                    {props => (
                        <div style={props}>
                            
                            <div className="container-bottoni">
                                {this.props.components.map(function (componente, i) {

                                        const hexRgb = require('hex-rgb');
                                        const colore = hexRgb(this.props.colore);
                                        var styleIcone = {
                                            backgroundColor:  "rgba("+colore.red+","+colore.green+","+colore.blue+",0.9)",
                                            display: "block",
                                            flex: "1 0 30%",
                                            margin:"5px",
                                            textAlign: "center",
                                            minWidth: "50px",
                                            marginTop: "50px",
                                            borderRadius: "2px",
                                    }

                                    if (componente.content.type === "About") {
                                        return (
                                            <div style={styleIcone}>
                                                <Link to={"/About" + this.controllo(this.props.components, componente, i)}>
                                                    <StorefrontRoundedIcon style={{ fontSize: 55, color: this.props.coloreTesto, marginTop:"20px"}}></StorefrontRoundedIcon>
                                                </Link>
                                                <p className="descrizione" style={{ color: this.props.coloreTesto}}>Chi siamo</p>
                                            </div>
                                        )
                                    } else if (componente.content.type === "Where") {
                                        return (
                                            <div style={styleIcone}>
                                                <Link to={"/Where" + this.controllo(this.props.components, componente, i)}>
                                                    <RoomIcon style={{ fontSize: 55, color: this.props.coloreTesto, marginTop:"20px" }}></RoomIcon>
                                                </Link>
                                                <p className="descrizione" style={{ color: this.props.coloreTesto }}>Dove</p>
                                            </div>
                                        )
                                    } else if (componente.content.type === "Shop") {
                                        return (
                                            <div style={styleIcone}>
                                                <Link to={"/Shop" + this.controllo(this.props.components, componente, i)}>
                                                    <ShoppingCartRoundedIcon style={{ fontSize: 55, color: this.props.coloreTesto, marginTop:"20px" }}></ShoppingCartRoundedIcon>
                                                </Link>
                                                <p className="descrizione" style={{ color: this.props.coloreTesto }}>Shop</p>
                                            </div>
                                        )
                                    } else if (componente.content.type === "Schedule") {
                                        return (
                                            <div style={styleIcone}>
                                                <Link to={"/Schedule" + this.controllo(this.props.components, componente, i)}>
                                                    <ScheduleRoundedIcon style={{ fontSize: 55, color: this.props.coloreTesto , marginTop:"20px"}}></ScheduleRoundedIcon>
                                                </Link>
                                                <p className="descrizione" style={{ color: this.props.coloreTesto }}>Orario</p>
                                            </div>
                                        )
                                    } else if (componente.content.type === "Date") {
                                        return (
                                            <div style={styleIcone}>
                                                <Link to={"/Date" + this.controllo(this.props.components, componente, i)}>
                                                    <CalendarTodayRoundedIcon style={{ fontSize: 55, color: this.props.coloreTesto, marginTop:"20px" }}></CalendarTodayRoundedIcon>
                                                </Link>
                                                <p className="descrizione" style={{ color: this.props.coloreTesto }}>Appunt.</p>
                                            </div>
                                        )
                                    } else if (componente.content.type === "Contacts") {
                                        return (
                                            <div style={styleIcone}>
                                                <Link to={"/Contacts" + this.controllo(this.props.components, componente, i)}>
                                                    <ContactPhoneRoundedIcon style={{ fontSize: 55, color: this.props.coloreTesto, marginTop:"20px" }}></ContactPhoneRoundedIcon>
                                                </Link>
                                                <p className="descrizione" style={{ color: this.props.coloreTesto }}>Contatti</p>
                                            </div>
                                        )
                                    } else if (componente.content.type === "Report") {
                                        return (
                                            <div style={styleIcone}>
                                                <Link to={"/Report" + this.controllo(this.props.components, componente, i)}>
                                                    <MailRoundedIcon style={{ fontSize: 55, color: this.props.coloreTesto, marginTop:"20px"}}></MailRoundedIcon>
                                                </Link>
                                                <p className="descrizione" style={{ color: this.props.coloreTesto }}>Report</p>
                                            </div>
                                        )
                                    } else if (componente.content.type === "WebPage") {
                                        return (
                                            <div style={styleIcone}>
                                                <Link to={"/WebPage" + this.controllo(this.props.components, componente, i)}>
                                                    <LanguageRoundedIcon style={{ fontSize: 55, color: this.props.coloreTesto,marginTop:"20px" }}></LanguageRoundedIcon>
                                                </Link>
                                                <p className="descrizione" style={{ color: this.props.coloreTesto }}>Sito Web</p>
                                            </div>
                                        )
                                    } else if (componente.content.type === "DataViewer") {
                                        return (
                                            <div style={styleIcone}>
                                                <Link to={"/DataViewer" + this.controllo(this.props.components, componente, i)}>
                                                    <AssessmentRoundedIcon style={{ fontSize: 55, color: this.props.coloreTesto, marginTop:"20px"}}></AssessmentRoundedIcon>
                                                </Link>
                                                <p className="descrizione" style={{ color: this.props.coloreTesto}}>DataViewer</p>
                                            </div>
                                        )
                                    } else if(componente.content.type === "Galleria"){
                                        return (
                                            <div style={styleIcone}>
                                                <Link to={"/galleria" + this.controllo(this.props.components, componente, i)}>
                                                    <PhotoLibraryIcon style={{ fontSize: 55, color: this.props.coloreTesto ,marginTop:"20px"}}></PhotoLibraryIcon>
                                                </Link>
                                                <p className="descrizione" style={{ color: this.props.coloreTesto}}>{componente.content.titolo}</p>
                                            </div>
                                        )
                                    }
                                }, this)}
                            </div>
                        </div>
                    )}
                </Spring>
            </div>
        )
    }

    controllo(componenti, componente, indice) {
        var somma = 0
        for (var i = 0; i < indice; i++) {
            if (componenti[i].content.type === componente.content.type)
                somma += 1
        }
        return somma
    }
}
/*
*/
export default HomePage