import React, { Component } from "react"
import { Carousel } from "react-bootstrap"
import { Spring } from "react-spring/renderprops"

class Galleria extends Component {
    render() {
        return (
            <div>
                <div style={{marginTop: "100px"}}></div>
                <Spring from={{ opacity: 0 }} to={{ opacity: 1 }}>
                    {props => (
                        <div style={props}>
                            <h1 style={{textAlign:"center", color:this.props.coloreTesto, padding: "20px", backgroundColor:this.props.colore, marginRight:"5%", marginLeft:"5%"}}>{this.props.titolo}</h1>
                            <Carousel style={{textAlign:"center", backgroundColor:this.props.colore, marginRight:"5%",  marginLeft:"5%", zIndex:"2"}}>
                                {this.props.galleria.map((slide, i) =>
                                    <Carousel.Item>
                                        <img
                                            className="d-block w-100"
                                            src={slide.immagine}
                                            alt={i + 1 + "slide"}
                                        />
                                        <Carousel.Caption>
                                            <p style={{color:this.props.coloreTesto}}>{slide.descrizione}</p>
                                        </Carousel.Caption>
                                    </Carousel.Item>
                                )}
                            </Carousel>

                        </div>
                    )}
                </Spring>
            </div>
        )
    }
}

export default Galleria;