import React, { Component } from "react"

class Componente extends Component {
    constructor(props) {
        super(props);

        this.letturaJSON = this.letturaJSON.bind(this);
    }

    render() {
        return (
            <div>
                <h1>{this.props.testo}</h1>
            </div>
        )
    }

    letturaJSON() {
        var nome = this.props.json;
        var n = JSON.parse(nome);

        return (
        <div>
            <h1>
                {n.nome}
            </h1>
            <h1>
                {n.componenti}
            </h1>       
        </div>)
    }
}

export default Componente;