import React, { Component } from "react"
import { Spring } from "react-spring/renderprops"

class Report extends Component {
    constructor(props) {
        super(props)
        this.mandaReport = this.mandaReport.bind(this)
        this.state = {
            inputNome: '',
            inputEmail: '',
            inputTesto: ''
        }

    }
    render() {
        return (
            <div>
                <div className="contenitore">
                    <Spring from={{ opacity: 0 }} to={{ opacity: 1 }}>
                        {props => (
                            <div style={props}>
                                <div style={{marginTop: "100px"}} ></div>
                                <h1 className="blurred titolo-report" style={{textAlign:"center",padding: "20px", backgroundColor:this.props.colore, marginRight:"5%", color: this.props.coloreTesto}}>Contattaci</h1>
                            </div>
                        )}
                    </Spring>

                    <Spring from={{ opacity: 0, marginLeft: -500 }} to={{ opacity: 1, marginLeft: 0 }}>
                        {props => (
                            <div style={props}>
                                <form className="text-center border border-light p-5 form-report" action="#!">
                                    <div style={{backgroundColor:this.props.colore, marginBottom:"10px", padding:"20px"}}>
                                    <p className="h4 mb-4 blurred" style={{textAlign:"center",padding: "20px", backgroundColor:this.props.colore, color:this.props.coloreTesto}}>Inserisci i tuoi dati</p>
                                        <input type="text" value={this.state.inputNome} onChange={evt => this.updateNome(evt)} className="form-control mb-4" placeholder="Nome" />
                                        <input type="email" value={this.state.inputEmail} onChange={evt => this.updateEmail(evt)} className="form-control mb-4" placeholder="E-mail" />
                                    </div>

                                    <div style={{backgroundColor:this.props.colore, marginBottom:"10px", padding:"20px"}}>
                                        <label style={{color:"white", textAlign:"center",padding: "20px", backgroundColor:this.props.colore, marginRight:"5%", color: this.props.coloreTesto}}>Motivo</label>
                                        <select className="browser-default custom-select mb-4">
                                            <option value="1" selected>Feedback</option>
                                            <option value="2">Report un bug</option>
                                            <option value="3">Richiesta Feature</option>
                                        </select>
                                        <div className="form-group">
                                            <textarea className="form-control rounded-0" value={this.state.inputTesto} onChange={evt => this.updateTesto(evt)} rows="3" placeholder="Messaggio"></textarea>
                                        </div>
                                    </div>
                               
                                    <button className="bottone-date" style={{color: this.props.coloreTesto, backgroundColor:this.props.colore, marginLeft:"0%"}} type="submit" onClick={this.mandaReport}>Manda</button>
                                </form>
                            </div>
                        )}
                    </Spring>
                </div>
            </div>
        )
    }

    mandaReport() {
        const richiesta = {
            "email": this.state.inputEmail,
            "nome": this.state.inputNome,
            "testo": this.state.inputTesto
        }

        console.log(richiesta)
    }

    updateTesto(evt) {
        this.setState(
            {
                inputTesto: evt.target.value
            }
        )
    }
    updateNome(evt) {
        this.setState(
            {
                inputNome: evt.target.value
            }
        )
    }
    updateEmail(evt) {
        this.setState(
            {
                inputEmail: evt.target.value
            }
        )
    }
}

export default Report;