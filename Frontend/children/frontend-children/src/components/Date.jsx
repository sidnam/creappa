import React, { Component } from "react"
import { Spring } from "react-spring/renderprops"
import Cookies from 'universal-cookie'
import {Form} from "react-bootstrap"
import Datetime from "react-datetime";
import "react-datetime/css/react-datetime.css";
import {Col, Row, Modal, Button } from "react-bootstrap"


class Date extends Component {
    constructor(props) {
        super(props)
        this.getDate = this.getDate.bind(this);
    }

    state = {
        date: "",
        dateDescription: "",
        dateEmail: "",
        showModal : false,
        stringa:""
    }

    render() {
        return (
            <div>
                <div className="contenitore">
                    <Spring from={{ opacity: 0 }} to={{ opacity: 1 }}>
                        {
                            props =>
                                <div style={props}>
                                    <div style={{marginTop: "100px"}}></div>
                                    <div style={{textAlign:"center",padding: "20px", backgroundColor:this.props.colore, marginRight:"5%", marginLeft:"5%"}}>
                                        <h1 style={{color:this.props.coloreTesto}}>{this.props.titolo}</h1>
                                        <p style={{color:this.props.coloreTesto}}>{this.props.testo}</p>
                                    </div>
                                    <div style={{backgroundColor:this.props.colore, marginRight:"5%", marginLeft:"5%", padding:"20px"}}>
                                        <p style={{color:this.props.coloreTesto}}>Inserisci la tua mail</p>
                                        <div className="form-group">
                                            <input as="email" placeholder="tua mail" value={this.state.dateEmail} onChange={e => this.setState({dateEmail: e.target.value})} placeholder="Tua mail" style={{width:"90%"}}/>
                                        </div>
                                        <div className="datepicker-date" style={{marginLeft:"5%"}}>
                                            <p style={{color:this.props.coloreTesto}}>Inserisci la data</p>
                                            <Datetime value={this.state.date} placeholder="Inserisci la data" onChange={this.onChange}/>
                                        </div>
                                        <div className="form-group">
                                            <p style={{color:this.props.coloreTesto}}>Inserisci il messaggio</p>
                                            <Form.Control as="textarea" rows="3" value={this.state.dateDescription} onChange={evt => this.updateTesto(evt)} placeholder="Messaggio" className="textarea-date"/>
                                        </div>
                                    </div>
                                    <button onClick={this.getDate} className="bottone-date" style={{color: this.props.coloreTesto, marginTop:"10px", backgroundColor:this.props.colore, marginRight:"5%", padding:"10px"}}>Prenota</button>
                                    <p style={{color:"red"}}><strong>{this.state.stringa}</strong></p>
                                </div>
                        }
                    </Spring>

                </div>
                <Modal show={this.state.showModal} onHide={e => this.setState({showModal:false})}>
                    <Modal.Header closeButton>
                        <Modal.Title>Richiesta effettuata</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Row>
                            <Col>
                                <Row className="justify-content-center">
                                    appuntamento per il {this.state.date.toString()} effettuato
                                </Row>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={e => this.setState({showModal:false})}>
                            Fatto
                                </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }

    onChange = date => this.setState({ date })
    updateTesto = evt => this.setState({ dateDescription: evt.target.value })

    async getDate() {
        //var date = this.state.date.toString()
        try {
            var date = this.state.date.toDate();
            var cookie = new Cookies()
            const appInfo = require('../files/info.json')
            const axios = require('axios').default
            const response = await axios.post(
                'https://api.creappa.com/askForDate',
                {
                    "email": cookie.get('account').email,
                    "password": cookie.get("account").pswd,
                    "appIndex": parseInt(appInfo.appIndex),
                    "dateDescription": this.state.dateDescription,
                    "date": {
                        "day": date.getDate(),
                        "month": date.getMonth() + 1,
                        "year": date.getYear() + 1900,
                        "hour": date.getHours(),
                        "minute": date.getMinutes()
                    }
                },
                {
                    'Content-Type': 'Application/json'
                }
            )

            if(response.data.status === "successful"){
                this.setState({stringa:""})
                this.setState({showModal:true})
            }
            else{
                this.setState({stringa:"Qualcosa è andato storto con il tuo appuntamento"})
            }
        } catch (error) {
            this.setState({stringa:"Qualcosa è andato storto con il tuo appuntamento"})
        }
    }

}
export default Date;