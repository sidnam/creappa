import React, { Component } from "react"
import { Spring } from "react-spring/renderprops"
import { Link } from "react-router-dom";

class WebPage extends Component {
    render() {
        return (
            <div>
            <div style={{marginTop: "100px"}}></div>
             <div className="contenitore">
                    <Spring from={{ opacity: 0 }} to={{ opacity: 1 }}>
                        {props => (
                            <div style={props}>
                                <div style={{textAlign:"center",padding: "20px", backgroundColor:this.props.colore, marginLeft:"5%", marginRight:"5%"}}>
                                <h1 style={{color:this.props.coloreTesto}}>{this.props.titolo}</h1>
                                <p style={{color:this.props.coloreTesto}}>{this.props.testo}</p>
                                <img src = {this.props.logo} style={{margin:"0 auto", maxWidth: "50%"}}></img><br/>
                                <Link  style={{color:"lightBlue"}} onClick={() => window.location.href = this.props.sito}className="titolo-webpage">{this.props.sito}</Link>
                                </div>
                            </div>
                        )}
                    </Spring>

                </div>
            </div>
        )
    }
}

export default WebPage;