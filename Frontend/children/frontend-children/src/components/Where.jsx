import React, { Component } from "react"
import { ReactBingmaps } from 'react-bingmaps';
import { Spring } from 'react-spring/renderprops'

class Where extends Component {
    constructor(props) {
        super(props)
        console.log([parseFloat(this.props.position.lat,10), parseFloat(this.props.position.lng,10)])
        this.state={
            lat:parseFloat(this.props.position.lat),
            lng:parseFloat(this.props.position.lng)
        }
    }
    render() {
        return (
            <div>
                <Spring from={{ opacity: 0 }} to={{ opacity: 1 }}>
                    {props => (
                        <div style={props}>
                            <div style={{marginTop: "100px"}}></div>
                            <div style={{textAlign:"center",padding: "20px", backgroundColor:this.props.colore, marginRight:"5%",  marginLeft: "5%"}}>
                                <h1 style={{color:this.props.coloreTesto}}>{this.props.titolo}</h1>
                                <p style={{color:this.props.coloreTesto}}>{this.props.testo}</p>
                            </div>
                            <div style={{ height: '80vh', Width: '90vh', marginLeft:"5%", marginRight:"5%" }}>
                            <ReactBingmaps
                                bingmapKey="AjZqm8thFeWufp6k0NsAOET6UzcISnQHH7EOjv62DZWtXGj20E3miG_nbyT8tqYG"
                                center={[this.state.lat, this.state.lng]}
                                zoom={500}
                                pushPins={
                                    [
                                        {
                                            "location": [this.state.lat, this.state.lng], "option": { color: 'red' }, "addHandler": { "type": "click", callback: this.callBackMethod }
                                        }
                                    ]
                                }
                            >
                            </ReactBingmaps>
                            </div>
                        </div>
                    )}
                </Spring>
            </div>
        )
    }
}

export default Where;