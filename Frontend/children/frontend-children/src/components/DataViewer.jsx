import React, { Component } from "react"
import {Spring} from "react-spring/renderprops"
import { Link } from "react-router-dom"

class DataViewer extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div>
                <div className="spaziatura-top-navbar"></div>
                <div className="contenitore-immagine" style={{backgroundImage : "url("+ this.props.background +")"}}></div>
                    {props => (
                        <div style={props}>
                            <h1 className={"spaziatura-shop blurred"}>{this.props.titolo}</h1>
                        </div>
                    )}
                <Spring from={{ opacity: 0, marginLeft: -500 }} to={{ opacity: 1, marginLeft: 0 }}>
                        {props => (
                            <div style={props}>
                                <div className="container-bottoni">
                                    {this.props.sections.map(function (sezione, i){
                                        switch(sezione.type){
                                            case "ranking":
                                                return(
                                                    <div className="imgTesto">
                                                        <Link to={"/DataViewer/" + sezione.title}>
                                                            <img src={sezione.img} className="logo"/>
                                                        </Link>
                                                        <p className="descrizione">{sezione.title}</p>
                                                    </div>
                                                )
                                            case "cake":
                                                return(
                                                    <div className="imgTesto">
                                                        <Link to={"/DataViewer/" + sezione.title}>
                                                            <img src={sezione.img} className="logo"/>
                                                        </Link>
                                                        <p className="descrizione">{sezione.title}</p>
                                                    </div>
                                                )
                                        }
                                    })}
                                </div>
                            </div>
                        )}
                </Spring>
            </div>
        )
    }
}

export default DataViewer;