import React, { Component } from "react"
import { Spring } from "react-spring/renderprops"

class Schedule extends Component {
    render() {
        return (
            <div>
                <div style={{marginTop: "100px"}}></div>
                <div className="contenitore">
                    <Spring from={{ opacity: 0 }} to={{ opacity: 1 }}>
                        {props => (
                            <div style={props}>
                                <h1 className="titolo-orario" style={{backgroundColor: this.props.colore, padding: "20px", color:this.props.coloreTesto}}>{this.props.titolo}</h1>
                            </div>
                        )}
                    </Spring>

                    <Spring from={{ opacity: 0, marginLeft: -500 }} to={{ opacity: 1, marginLeft: 0 }}>
                        {props => (
                            <div style={props}>
                                <table className="tabella-orario">
                                    <tr>
                                        <th>Giorno</th>
                                        <th>Orario</th>
                                    </tr>
                                    {this.props.orari.map(orario => (
                                        <tr>
                                            <td>{orario.giorno}</td>
                                            <td>{orario.orario}</td>
                                        </tr>
                                    ))}
                                </table>
                            </div>
                        )}
                    </Spring>
                </div>
            </div>
        )
    }
}

export default Schedule;