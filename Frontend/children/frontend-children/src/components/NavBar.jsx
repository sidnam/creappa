import React, { Component } from "react"
import { Link } from "react-router-dom";
import Cookies from "universal-cookie";
import ShareIcon from '@material-ui/icons/Share' ;
import {FormControl, Button, Container, Col, Row, Modal, InputGroup } from "react-bootstrap"
import {FileCopyOutlined} from "@material-ui/icons"
import { slide as Menu } from 'react-burger-menu'

import ShoppingCartRoundedIcon from '@material-ui/icons/ShoppingCartRounded';
import ScheduleRoundedIcon from '@material-ui/icons/ScheduleRounded';
import CalendarTodayRoundedIcon from '@material-ui/icons/CalendarTodayRounded';
import MailRoundedIcon from '@material-ui/icons/MailRounded';
import LanguageRoundedIcon from '@material-ui/icons/LanguageRounded';
import ContactPhoneRoundedIcon from '@material-ui/icons/ContactPhoneRounded';
import StorefrontRoundedIcon from '@material-ui/icons/StorefrontRounded';
import AssessmentRoundedIcon from '@material-ui/icons/AssessmentRounded';
import PhotoLibraryIcon from '@material-ui/icons/PhotoLibrary';
import RoomIcon from '@material-ui/icons/Room';

var QRCode = require('qrcode.react');

class NavBar extends Component {
    constructor(props) {
        super(props)
        this.logout = this.logout.bind(this)
        this.controllo = this.controllo.bind(this)
        this.state = {
            showModal:false
        }
    }
    logout() {
        var cookie = new Cookies()
        this.props.modificaLoggato(false)
        cookie.remove('account')
        window.location.pathname = "/"
    }

    controllo(componenti, componente, indice) {
        var somma = 0
        for (var i = 0; i < indice; i++) {
          if (componenti[i].content.type === componente.content.type)
            somma += 1
        }
        return somma
      }

    render() {
        var cookie = new Cookies()

        const appInfo = require('../files/info.json')
        const style = {
            "bmMenu" :
            {
                background: this.props.colore,
                padding: '40px',
                fontSize: '1.15em',
            },
            "bmBurgerBars" :
            {
                background: this.props.coloreTesto
            },
            
          };
        return (
            <div>
                <div style={{position:"absolute", zIndex: "4", top:"-1px"}}>
                    <Menu styles={style}>
                        <Link to="/"><img className="menu-item" style={{maxWidth:"100px", height:"auto", marginBottom: "40px"}} src={this.props.logo} /></Link>
                        {this.props.components.map(function (componente, i) {
                            switch (componente.content.type) {
                            case "About":
                                return <div><StorefrontRoundedIcon style={{ fontSize: 35, color: this.props.coloreTesto, marginRight:"40px", marginBottom:"15px"}}></StorefrontRoundedIcon><Link to={"/About" + this.controllo(this.props.components, componente, i)} className="menu-item" style={{color:this.props.coloreTesto}} href="/about">Chi siamo</Link></div>
                            case "Where":
                                return <div><RoomIcon style={{ fontSize: 35, color: this.props.coloreTesto, marginRight:"40px", marginBottom:"15px"}}></RoomIcon><Link to={"/Where" + this.controllo(this.props.components, componente, i)} className="menu-item" style={{color:this.props.coloreTesto}} >Dove trovarci</Link></div>
                            case "Shop":
                                return <div><ShoppingCartRoundedIcon style={{ fontSize: 35, color: this.props.coloreTesto, marginRight:"40px", marginBottom:"15px"}}></ShoppingCartRoundedIcon><Link to={"/Shop" + this.controllo(this.props.components, componente, i)} className="menu-item" style={{color:this.props.coloreTesto}} >Negozio</Link></div>
                            case "Schedule":
                                return <div><ScheduleRoundedIcon style={{ fontSize: 35, color: this.props.coloreTesto, marginRight:"40px", marginBottom:"15px"}}></ScheduleRoundedIcon><Link to={"/Schedule" + this.controllo(this.props.components, componente, i)} className="menu-item" style={{color:this.props.coloreTesto}} >Orario</Link></div>
                            case "Date":
                                return <div><CalendarTodayRoundedIcon style={{ fontSize: 35, color: this.props.coloreTesto, marginRight:"40px", marginBottom:"15px"}}></CalendarTodayRoundedIcon><Link to={"/Date" + this.controllo(this.props.components, componente, i)} className="menu-item" style={{color:this.props.coloreTesto}} >Appuntamento</Link></div>
                            case "Report":
                                return <div><MailRoundedIcon style={{ fontSize: 35, color: this.props.coloreTesto, marginRight:"40px", marginBottom:"15px"}}></MailRoundedIcon><Link to={"/Report" + this.controllo(this.props.components, componente, i)}  className="menu-item" style={{color:this.props.coloreTesto}} >Report</Link></div>
                            case "WebPage":
                                return <div><LanguageRoundedIcon style={{ fontSize: 35, color: this.props.coloreTesto, marginRight:"40px", marginBottom:"15px"}}></LanguageRoundedIcon><Link to={"/WebPage" + this.controllo(this.props.components, componente, i)} className="menu-item" style={{color:this.props.coloreTesto}} >Sito Web</Link></div>
                            case "Contacts":
                                return <div><ContactPhoneRoundedIcon style={{ fontSize: 35, color: this.props.coloreTesto, marginRight:"40px", marginBottom:"15px"}}></ContactPhoneRoundedIcon><Link to={"/Contacts" + this.controllo(this.props.components, componente, i)} className="menu-item" style={{color:this.props.coloreTesto}}>Contatti</Link></div>                            
                            case "Galleria":
                                return <div><PhotoLibraryIcon style={{ fontSize: 35, color: this.props.coloreTesto, marginRight:"40px", marginBottom:"15px"}}></PhotoLibraryIcon><Link to={"/Galleria" + this.controllo(this.props.components, componente, i)}  className="menu-item" style={{color:this.props.coloreTesto}} >Galleria</Link></div>
                                
                            }
                        }, this)
                        }
                        
                    </Menu>
                </div>

                <nav className="navbar justify-content-end fixed-top" style={{ minHeight: "80px", backgroundColor:this.props.colore, zIndex:"1"}}>
                    <ShareIcon style={{ fontSize: 40, color: this.props.coloreTesto, marginRight: "10px"}} className="navbar-right" onClick={e => this.setState({showModal:true})} alt="Condividi"></ShareIcon>
                </nav>
                <Modal show={this.state.showModal} onHide={e => this.setState({showModal:false})}>
                    <Modal.Header closeButton>
                        <Modal.Title>Condividi la tua applicazione!</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Row>
                            <Col md={4} className="justify-content-center">
                                <Row className="justify-content-center" >
                                    <QRCode value={'https://' + appInfo.name + '.creappa.com/'} />
                                </Row>
                            </Col>
                            <Col>
                                <Row className="justify-content-center">
                                    <h6>Condividi questo QRCode oppure il link qua sotto per diffondere la tua app!</h6>
                                </Row>
                                <Row>
                                    <InputGroup className="mb-3">
                                        <FormControl
                                            value={'https://' + appInfo.name + '.creappa.com/'}
                                        />
                                        <InputGroup.Append>
                                            <div style={{ verticalAlign: "center" }}>
                                                <FileCopyOutlined />
                                            </div>
                                        </InputGroup.Append>
                                    </InputGroup>
                                </Row>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={e => this.setState({showModal:false})}>
                            Chiudi
                                </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}

export default NavBar;

/*
<Dropdown>
    <Dropdown.Toggle variant="primary" id="dropdown-basic">
        <i className="fas fa-user fa-fw"></i>
    </Dropdown.Toggle>
    {
        (<Dropdown.Menu>
            {(this.props.loggato) ? (<div><Dropdown.Item><Link to="/approvedDates">{cookie.get('account').email}</Link></Dropdown.Item><Dropdown.Item onClick={this.logout}><Link>LogOut</Link></Dropdown.Item></div>) : (<Dropdown.Item><Link to="/login">{"Login"}</Link></Dropdown.Item>)}
        </Dropdown.Menu>)
    }
</Dropdown>

*/