import React, { Component } from "react"
import { Spring } from 'react-spring/renderprops'

class Shop extends Component {
    render() {
        return (
            <div>
                 <div style={{marginTop: "100px"}}></div>
                <Spring from={{ opacity: 0 }} to={{ opacity: 1 }}>
                    {props => (
                        <div style={props}>
                            <h1 style={{textAlign:"center", padding: "20px", backgroundColor:this.props.colore, color:this.props.coloreTesto, marginRight:"5%", marginLeft: "5%"}}>{this.props.titolo}</h1>
                            <p style={{textAlign:"center", padding: "20px", backgroundColor:this.props.colore, color:this.props.coloreTesto, marginRight:"5%", marginLeft: "5%"}}>{this.props.testo}</p>
                        </div>
                    )}
                </Spring>
                {this.props.prodotti.map((prodotto, i) => (
                    <Spring from={{ opacity: 0, marginLeft: -500 }} to={{ opacity: 1, marginLeft: 0 }}>
                        {props => (
                            <div style={props}>
                                <div class="card card-shop" style={{textAlign:"center", backgroundColor:this.props.colore, color:this.props.coloreTesto, marginRight:"5%", marginLeft: "5%"}}>
                                    <img alt="img" class="card-img-top immagine-shop " src={prodotto.img} alt="Card image cap" />
                                    <h5 class="card-header titolo-shop" style={{color:this.props.coloreTesto}}>{prodotto.titolo}</h5>
                                    <div className="card-body">
                                        <p class="card-text testo-shop" style={{color:this.props.coloreTesto}}>{prodotto.descrizione}</p>
                                        <footer className="blockquote-footer testo-shop" style={{color:this.props.coloreTesto}}><strong>Prezzo:</strong> {prodotto.prezzo}$ </footer>
                                    </div>
                                </div>
                            </div>
                        )}

                    </Spring>
                ))}

            </div>
        )
    }
}
export default Shop;