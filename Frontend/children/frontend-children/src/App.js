import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"

/* Import dei componenti statici */
import HomePage from "./components/HomePage"
import NavBar from "./components/NavBar"

/* Import dei componenti Dinamici */
import About from "./components/About"
import Where from "./components/Where"
import Shop from "./components/Shop"
import Galleria from "./components/Galleria"
import Schedule from "./components/Schedule"
import Date from "./components/Date"
import Report from "./components/Report"
import WebPage from "./components/WebPage"
import Contacts from "./components/Contacts"
import DataViewer from './components/DataViewer';

/* Altri Import */
import CircularProgress from '@material-ui/core/CircularProgress';

class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      coloreSfondo: "",
    }

    this.getBackgroundImagePath = this.getBackgroundImagePath.bind(this)
    this.controllo = this.controllo.bind(this)
  }
 
  controllo(componenti, componente, indice) {
    var somma = 0
    for (var i = 0; i < indice; i++) {
      if (componenti[i].content.type === componente.content.type)
        somma += 1
    }
    return somma
  }

  async componentDidMount() {
    const axios = require('axios').default
    const appInfo = require('./files/info.json')

    const response = await axios.post(
      'https://api.creappa.com/getConfigs',
      {
        'subdomain': appInfo.name,
        'appIndex': parseInt(appInfo.appIndex)
      },
      {
        'Content-Type': 'Application/json'
      }
    )
    
    document.title = response.data.subdomain;
    var logoDimension = {
      width: 0,
      height: 0
    }

    var imageSizes = {
      width: 0,
      height: 0
    }

    var image = new Image()

    image.addEventListener("load", (event) => {
      console.log(event)
      var dynamicManifest = {
        "name": appInfo.name + ", powered by creappa.com",
        "short_name": appInfo.name,
        "theme_color": response.data.colors.primary,
        "display": "fullscreen",
        "start_url": "https://" + appInfo.name + ".creappa.com/",
        "icons": [{
          "src": event.path[0].currentSrc,
          "sizes": event.path[0].naturalWidth.toString() + "x" + event.path[0].naturalHeight.toString(),
          "type": "image/png"
        }]
      }

      const stringManifest = JSON.stringify(dynamicManifest);
      const blob = new Blob([stringManifest], { type: 'application/json' })
      const manifestURL = URL.createObjectURL(blob)
      document.querySelector('#my-manifest').setAttribute('href', manifestURL)

      var fonts = []
      response.data.font === null ? fonts.push('Roboto') : fonts.push(response.data.font)

      /*WebFont.load({
        google: {
          families: fonts
        }
      })*/

      document.querySelector('#root').setAttribute('style', 'font-family:' + response.data.font)
    })
    image.src = this.getLogoPath(response.data.resources)

    this.setState(
      {
        loading: false,
        configs: response.data,
        logged: true,
        coloreSfondo: response.data.colors.principale,
        coloreTesto : response.data.colors.secondario
      }
    )
    document.body.style.backgroundImage = "url("+this.getBackgroundImagePath(this.state.configs.resources) +")"//"url('https://creappa.s3.eu-central-1.amazonaws.com/resources/background-template/sfondo1.jpg')";
    console.log("colori")
    console.log(this.state.coloreTesto)
  }

  getLogoPath(resources) {
    var logoIndex = 0;
    resources.map(function (risorsa, i) {
      if (risorsa.content.type === "logo")
        logoIndex = i
    }
    )
    const favicon = document.getElementById("favicon")
    favicon.href = resources[logoIndex].content.path
    return resources[logoIndex].content.path
  }

  getBackgroundImagePath(resources) {
    var backgroundImageIndex = 0;
    resources.map(function (risorsa, i) {
      if (risorsa.content.type === "background_image")
        backgroundImageIndex = i
    })

    return resources[backgroundImageIndex].content.path;
  }
  render() {
    return (
      <div>
        {
          this.state.loading ? <div className="loading"> <CircularProgress></CircularProgress></div> :
            <div>
              <Router>
                <NavBar modificaLoggato={this.modificaLoggato} coloreTesto={this.state.coloreTesto} colore={this.state.coloreSfondo} coloreTesto={this.state.coloreTesto} components={this.state.configs.components} colors={"FFFFFF"} loggato={this.state.loggato} titolo={this.state.configs.subdomain} logo={this.getLogoPath(this.state.configs.resources)} />
                {
                  this.state.configs.components.map(function (componente, i) {
                    switch (componente.content.type) {
                      case "About":
                        return (<Route exact path={"/" + componente.content.type + this.controllo(this.state.configs.components, componente, i)} render={() => <About titolo={componente.content.titolo} background={this.getBackgroundImagePath(this.state.configs.resources)} testo={componente.content.testo} immagine={componente.content.immagine} colore={this.state.coloreSfondo} coloreTesto={this.state.coloreTesto} />}></Route>)
                      case "Where":
                        return (<Route exact path={"/" + componente.content.type + this.controllo(this.state.configs.components, componente, i)} render={() => <Where titolo={componente.content.titolo} position={componente.content.posizione} background={this.getBackgroundImagePath(this.state.configs.resources)} testo={componente.content.testo} colore={this.state.coloreSfondo} coloreTesto={this.state.coloreTesto} posizione={componente.content.posizione} />}></Route>)
                      case "Shop":
                        return (<Route exact path={"/" + componente.content.type + this.controllo(this.state.configs.components, componente, i)} render={() => <Shop titolo={componente.content.titolo} background={this.getBackgroundImagePath(this.state.configs.resources)} testo={componente.content.testo} colore={this.state.coloreSfondo} coloreTesto={this.state.coloreTesto} prodotti={componente.content.prodotti} />}></Route>)
                      case "Schedule":
                        return (<Route exact path={"/" + componente.content.type + this.controllo(this.state.configs.components, componente, i)} render={() => <Schedule orari={componente.content.orari} background={this.getBackgroundImagePath(this.state.configs.resources)} titolo={componente.content.titolo} colore={this.state.coloreSfondo} coloreTesto={this.state.coloreTesto}/>}></Route>)
                      case "Date":
                        return (<Route exact path={"/" + componente.content.type + this.controllo(this.state.configs.components, componente, i)} render={() => <Date titolo={componente.content.titolo} background={this.getBackgroundImagePath(this.state.configs.resources)} testo={componente.content.testo} colore={this.state.coloreSfondo} coloreTesto={this.state.coloreTesto}/>}></Route>)
                      case "Report":
                        return (<Route exact path={"/" + componente.content.type + this.controllo(this.state.configs.components, componente, i)} render={() => <Report colore={this.state.coloreSfondo} coloreTesto={this.state.coloreTesto} background={this.getBackgroundImagePath(this.state.configs.resources)} />}></Route>)
                      case "WebPage":
                        return (<Route exact path={"/" + componente.content.type + this.controllo(this.state.configs.components, componente, i)} render={() => <WebPage colore={this.state.coloreSfondo} coloreTesto={this.state.coloreTesto} titolo={componente.content.titolo} logo={this.getLogoPath(this.state.configs.resources)} background={this.getBackgroundImagePath(this.state.configs.resources)} sito={componente.content.sito} testo={componente.content.testo} />}></Route>)
                      case "Contacts":
                        return (<Route exact path={"/" + componente.content.type + this.controllo(this.state.configs.components, componente, i)} render={() => <Contacts colore={this.state.coloreSfondo} coloreTesto={this.state.coloreTesto} titolo={componente.content.titolo} background={this.getBackgroundImagePath(this.state.configs.resources)} contatti={componente.content.contatti} />}></Route>)
                      case "DataViewer":
                        return (<Route exact path={"/" + componente.content.type + this.controllo(this.state.configs.components, componente, i)} render={() => <DataViewer colore={this.state.coloreSfondo} coloreTesto={this.state.coloreTesto} titolo={componente.content.titolo} background={this.getBackgroundImagePath(this.state.configs.resources)} sections={componente.content.sections} />}></Route>)
                      case "Galleria":
                        return (<Route exact path={"/" + componente.content.type + this.controllo(this.state.configs.components, componente, i)} render={() => <Galleria colore={this.state.coloreSfondo} coloreTesto={this.state.coloreTesto} galleria={componente.content.galleria} titolo={componente.content.titolo} background={this.getBackgroundImagePath(this.state.configs.resources)}></Galleria>}></Route>
                        )
                      }
                  }, this)
                }
                <Switch>
                  <Route exact path="/" render={() => <HomePage colore={this.state.coloreSfondo} coloreTesto={this.state.coloreTesto} logo={this.getLogoPath(this.state.configs.resources)} background={this.getBackgroundImagePath(this.state.configs.resources)} colors={this.state.configs.colors} components={this.state.configs.components} ></HomePage>}></Route>
                </Switch>
              </Router>
            </div>
        }
      </div>
    )
  }
}

export default App;
